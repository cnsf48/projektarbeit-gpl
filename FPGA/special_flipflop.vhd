library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Das Flipflop kann nach einem Reset erst wieder aktiviert werden, wenn Set kurzzeitig inaktiv war.
entity special_flipflop is
	Port (
		S	: in	std_logic;
		R	: in	std_logic;
		Q	: out	std_logic
	);
end special_flipflop;

architecture Behavioral of special_flipflop is
	signal Q2		: std_logic;
	signal notQ		: std_logic;
	signal notQ_R	: std_logic;
	signal R_int	: std_logic;
begin

notQ_R	<= R nor R_int;
R_int	<= (not R and not S) nor notQ_R;
Q		<= Q2;
Q2		<= R_int nor notQ;
notQ	<= S nor Q2;

end Behavioral;