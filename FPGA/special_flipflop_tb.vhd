library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity special_flipflop_tb is
end entity special_flipflop_tb;

architecture behavioral of special_flipflop_tb is

	component special_flipflop is
		Port (
--			clk	: in	STD_LOGIC;
			S	: in	STD_LOGIC;
			R	: in	STD_LOGIC;
			Q	: out	STD_LOGIC
		);
	end component;

	constant TIME_DELTA : time := 100 ns;
--	constant TIME_DELTA_CLK : time := 10 ns;

--	signal	clk			: std_logic := '0';
	signal	S			: std_logic := '0';
	signal	R			: std_logic := '0';

	signal	Q			: std_logic;	

begin


--	clk   <= not clk  after TIME_DELTA_CLK;

	simulation: process
	begin

	S <= '0';
	R <= '0';

	wait for TIME_DELTA;

	S <= '1';
	R <= '0';

	wait for TIME_DELTA;

	S <= '0';
	R <= '0';

	wait for TIME_DELTA;

	S <= '0';
	R <= '1';

	wait for TIME_DELTA;

	S <= '0';
	R <= '0';

	wait for TIME_DELTA;

	S <= '0';
	R <= '0';

	wait for TIME_DELTA;

	S <= '1';
	R <= '1';

	wait for TIME_DELTA;

	S <= '0';
	R <= '0';

	wait for TIME_DELTA;

	S <= '1';
	R <= '0';

	wait for TIME_DELTA;

	S <= '1';
	R <= '1';

	wait for TIME_DELTA;

	S <= '1';
	R <= '0';

	wait for TIME_DELTA;

	S <= '0';
	R <= '0';

	wait for TIME_DELTA;

	S <= '0';
	R <= '0';

	wait for TIME_DELTA;

	S <= '0';
	R <= '1';

	wait for TIME_DELTA;

	S <= '1';
	R <= '1';

	wait for TIME_DELTA;

	S <= '0';
	R <= '1';

	wait for TIME_DELTA;

	S <= '0';
	R <= '0';

	wait for TIME_DELTA;
	end process simulation;
	
	special_flipflop_inst: special_flipflop
	port map(
--		clk	=> clk,
		S	=> S,
		R	=> R,
		Q	=> Q
	);
end behavioral;

