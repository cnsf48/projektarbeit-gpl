library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--component drehimpulsgeber port(
--	a			: in	std_logic;
--	b			: in	std_logic;
--	reset		: in	std_logic;
--	\out\		: out	std_logic_vector(7 downto 0);
--	count_up	: out	std_logic;
--	count_down	: out	std_logic );
--end component;


entity drehimpulsgeber is
	port(
		a			: in	std_logic;
		b			: in	std_logic;
		reset		: in	std_logic;
		clk			: in	std_logic;
		counter		: out	std_logic_vector(7 downto 0);
		count_up	: out	std_logic;
		count_down	: out	std_logic
	);
end drehimpulsgeber;

architecture behavioral of drehimpulsgeber is

	type State_type is (idle, a1, a2, a3, b1, b2, b3);  -- Define the states

	signal	state			: State_type;
	signal	counter_int		: std_logic_vector (7 downto 0);
	signal	count_up_int	: std_logic;
	signal	count_down_int	: std_logic;	

begin
	process (clk, reset)
	begin
		if (reset = '1') then
			state <= idle;
			counter_int <= (others => '0');
		elsif rising_edge(clk) then
			case state is
				when idle =>
					count_up_int <= '0';
					count_down_int <= '0';
					if (a = '1') then
						state <= a1;
					elsif (b = '1') then
						state <= b1;
					end if;
				when a1 =>
					if (a = '0') then
						state <= idle;
					elsif (b = '1') then
						state <= a2;
					end if;
				when a2 =>
					if (a = '0') then
						state <= a3;
					elsif (b = '0') then
						state <= a1;
					end if;
				when a3 =>
					if (a = '1') then
						state <= a2;
					elsif (b = '0') then
						state <= idle;
						counter_int <= std_logic_vector(unsigned(counter_int) + 1);
						count_up_int <= '1';
					end if;
				when b1 =>
					if (b = '0') then
						state <= idle;
					elsif (a = '1') then
						state <= b2;
					end if;
				when b2 =>
					if (b = '0') then
						state <= b3;
					elsif (a = '0') then
						state <= b1;
					end if;
				when b3 =>
					if (b = '1') then
						state <= b2;
					elsif (a = '0') then
						state <= idle;
						counter_int <= std_logic_vector(unsigned(counter_int) - 1);
						count_down_int <= '1';
					end if;
				when others =>
					state <= idle;
			end case;
		end if;
	end process;
	counter <= counter_int;
	count_up <= count_up_int;
	count_down <= count_down_int;
end behavioral;