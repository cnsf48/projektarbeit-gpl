library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity serialize is
	port(
		clk			: in	std_logic;
		reset		: in	std_logic;
		data		: in	std_logic_vector(7 downto 0);
		tmp_out		: out	std_logic_vector(7 downto 0);
		load		: out	std_logic;
		serial_out	: out	std_logic );
end serialize;

architecture archi of serialize is

	type State_type is (l0, s1, s2, s3, s4, s5, s6, s7);  -- Define the states

	signal state		: State_type;
	signal loading		: std_logic;
	signal tmp			: std_logic_vector(7 downto 0);

begin

	process (clk, reset)
	begin
		if (reset = '1') then
			state <= l0;
			tmp <= (others => '0');
		elsif rising_edge(clk) then
			case state is
				when l0 =>
					loading <= '0';
					tmp <= data;
					state <= s1;
				when s1 =>
					tmp <= tmp(6 downto 0) & '0';
					loading <= '0';
					state <= s2;
				when s2 =>
					tmp <= tmp(6 downto 0) & '0';
					loading <= '0';
					state <= s3;
				when s3 =>
					tmp <= tmp(6 downto 0) & '0';
					loading <= '0';
					state <= s4;
				when s4 =>
					tmp <= tmp(6 downto 0) & '0';
					loading <= '0';
					state <= s5;
				when s5 =>
					tmp <= tmp(6 downto 0) & '0';
					loading <= '0';
					state <= s6;
				when s6 =>
					tmp <= tmp(6 downto 0) & '0';
					loading <= '0';
					state <= s7;
				when others =>
					tmp <= tmp(6 downto 0) & '0';
					loading <= '1';
					state <= l0;
			end case;
		end if;
	end process;

serial_out	<= tmp(7);
load		<= loading;
tmp_out		<= tmp;

end archi;