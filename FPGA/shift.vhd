library ieee; 
use ieee.std_logic_1164.all; 

entity shift is 
	port(
		C		: in std_logic;
		SI		: in std_logic;
		ALOAD	: in std_logic;
		D		: in std_logic_vector(7 downto 0);
		tmp_out	: out std_logic_vector(7 downto 0);
		SO		: out std_logic);
end shift; 

architecture archi of shift is 
	signal tmp: std_logic_vector(7 downto 0); 
begin  
	process (C, ALOAD, D) 
	begin 
		if (ALOAD='1') then 
			tmp <= D; 
		elsif (C'event and C='1') then 
			tmp <= tmp(6 downto 0) & SI; 
		end if; 
	end process; 
	SO <= tmp(7); 
	tmp_out <= tmp;
end archi; 