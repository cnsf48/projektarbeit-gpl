library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity LCD_tb is
end entity LCD_tb;

architecture behavioral of LCD_tb is

component LCD is
	port(
		clk		: in	std_logic;
		send_n	: in	std_logic;
		addr	: in	std_logic_vector(7 downto 0);
		data	: in	std_logic_vector(7 downto 0);
		LCD_Bus	: out	std_logic_vector(7 downto 0);
		E		: out	std_logic;
		RS		: out	std_logic
	);
end component;

	constant TIME_DELTA : time := 100 ns;
	constant TIME_DELTA_CLK : time := 1 ns;

	signal	clk			: std_logic := '0';
	signal	send_n		: std_logic := '0';	
	signal	addr		: std_logic_vector (7 downto 0);
	signal	data		: std_logic_vector (7 downto 0);

	signal	LCD_Bus		: std_logic_vector (7 downto 0);
	signal	E			: std_logic;
	signal	RS			: std_logic;

begin


	clk   <= not clk  after TIME_DELTA_CLK;

	simulation: process
	begin

	send_n <= '0';
	addr <= x"00";
	data <= x"00";

	wait for TIME_DELTA;

	send_n <= '0';
	addr <= x"00";
	data <= x"00";

	wait for TIME_DELTA;

	send_n <= '0';
	addr <= x"10";
	data <= x"FF";

	wait for TIME_DELTA;

	send_n <= '1';
	addr <= x"10";
	data <= x"FF";

	wait for TIME_DELTA;

	send_n <= '0';
	addr <= x"10";
	data <= x"FF";

	wait for TIME_DELTA;

	send_n <= '0';
	addr <= x"10";
	data <= x"FF";

	wait for TIME_DELTA;
	end process simulation;
	
	LCD_inst: LCD
	port map(
		clk			=> clk,
		send_n		=> send_n,
		addr		=> addr,
		data		=> data,
		LCD_Bus		=> LCD_Bus,
		E			=> E,
		RS			=> RS
	);
end behavioral;

