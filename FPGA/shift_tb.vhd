library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity shift_tb is
end entity shift_tb;

architecture behavioral of shift_tb is

	component shift is
		Port (
		C		: in	std_logic;
		SI		: in	std_logic;
		ALOAD	: in	std_logic;
		D		: in	std_logic_vector(7 downto 0);
		tmp_out	: out	std_logic_vector(7 downto 0);
		SO		: out	std_logic
		);
	end component;

	constant TIME_DELTA		: time := 100 ns;
	constant TIME_DELTA_CLK	: time := 10 ns;

	signal	C			: std_logic := '0';
	signal	SI			: std_logic := '0';
	signal	ALOAD		: std_logic := '0';
	signal	D			: std_logic_vector (7 downto 0);
	signal	tmp_out		: std_logic_vector (7 downto 0);

	signal	SO			: std_logic;	

begin


	C   <= not C  after TIME_DELTA_CLK;

	simulation: process
	begin

	SI		<= '0';
	ALOAD	<= '0';
	D		<= x"00";

	wait for TIME_DELTA_CLK / 2;

	SI		<= '0';
	ALOAD	<= '0';
	D		<= x"00";

	wait for TIME_DELTA;

	SI		<= '0';
	ALOAD	<= '0';
	D		<= x"AF";

	wait for TIME_DELTA;

	SI		<= '0';
	ALOAD	<= '1';
	D		<= x"AF";

	wait for TIME_DELTA;

	SI		<= '0';
	ALOAD	<= '0';
	D		<= x"00";

	wait for TIME_DELTA;

	SI		<= '0';
	ALOAD	<= '0';
	D		<= x"00";

	wait for TIME_DELTA;

	SI		<= '0';
	ALOAD	<= '0';
	D		<= x"00";

	wait for TIME_DELTA;

	SI		<= '0';
	ALOAD	<= '0';
	D		<= x"00";

	wait for TIME_DELTA;

	SI		<= '0';
	ALOAD	<= '0';
	D		<= x"00";

	wait for TIME_DELTA;

	SI		<= '0';
	ALOAD	<= '0';
	D		<= x"00";

	wait for TIME_DELTA;

	SI		<= '0';
	ALOAD	<= '0';
	D		<= x"00";

	wait for TIME_DELTA;

	SI		<= '0';
	ALOAD	<= '0';
	D		<= x"00";

	wait for TIME_DELTA;

	SI		<= '0';
	ALOAD	<= '0';
	D		<= x"00";

	wait for TIME_DELTA;

	SI		<= '0';
	ALOAD	<= '0';
	D		<= x"00";

	wait for TIME_DELTA;

	SI		<= '0';
	ALOAD	<= '0';
	D		<= x"00";

	wait for TIME_DELTA;

	SI		<= '0';
	ALOAD	<= '0';
	D		<= x"00";

	wait for TIME_DELTA;

	wait for TIME_DELTA;
	end process simulation;
	
	shift_inst: shift
	port map(
		C		=> C,
		SI		=> SI,
		ALOAD	=> ALOAD,
		D		=> D,
		tmp_out	=> tmp_out,
		SO		=> SO
	);
end behavioral;

