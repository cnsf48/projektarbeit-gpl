--
-- Code zur Projektarbeit:
-- Versuchsaufbau zur Messung von Bitfehlerraten über unterschiedliche Übertragungswege bei unterschiedlichen Leitungs- und Kanalkodierungen
-- Christoph Schäfer
--
-- Einige Funktionen sind noch nicht oder nicht vollständig implementiert
-- Dazu gehört z.B. der Daten Empfang mit Oversampling
-- Relativ gut Funktionieren die Encoderauswertung und die LCD Ansteuerung
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library machxo2;
use machxo2.all;
use machxo2.components.osch;

--library work;
--use work.all;
--use work.drehimpulsgeber;
--use work.drehimpulsgeber.all;


entity TinyFPGA_A2 is
	port(
		--pin_jtag_tms	: inout	std_logic;	--JTAG
		--pin_jtag_tck	: inout	std_logic;	--JTAG
		--pin_jtag_tdi	: inout	std_logic;	--JTAG
		--pin_jtag_tdo	: inout	std_logic;	--JTAG
	--	pin_sda0		: inout	std_logic;	--I2C0
	--	pin_scl0		: in	std_logic;	--I2C0
		pin_sda1		: inout	std_logic;	--I2C1
		pin_scl1		: in	std_logic;	--I2C1
		pin_gpio_08_cs	: in	std_logic;	--SPI0
		pin_spi_0_sclk	: in	std_logic;	--SPI0
		pin_spi_0_miso	: out	std_logic;	--SPI0
		pin_spi_0_mosi	: in	std_logic;	--SPI0
		pin_gpio_04		: in	std_logic;	--GPIO-PI	--CLK-IN
		--pin_gpio_18	: inout	std_logic;	--GPIO-PI
		--pin_gpio_17	: inout	std_logic;	--GPIO-PI
		--pin_gpio_27	: inout	std_logic;	--GPIO-PI
		--pin_gpio_23	: inout	std_logic;	--GPIO-PI
		--pin_gpio_22	: inout	std_logic;	--GPIO-PI
		--pin_gpio_24	: inout	std_logic;	--GPIO-PI
		--pin_gpio_25	: inout	std_logic;	--GPIO-PI
		--pin_gpio_07	: inout	std_logic;	--GPIO-PI
		pin_gpio_05		: in	std_logic;	--GPIO-PI	--FIFO Write Enable
		pin_gpio_06		: out	std_logic;	--GPIO-PI	--FIFO Almost Empty
		pin_gpio_26		: out	std_logic;	--GPIO-PI	--FIFO Almost Full
		pin_led_1		: out	std_logic;	--LED
		pin_led_2		: out	std_logic;	--LED
		pin_led_3		: out	std_logic;	--LED
		pin_led_4		: out	std_logic;	--LED
		pin_led_5		: out	std_logic;	--LED
		pin_led_6		: out	std_logic;	--LED
		pin_led_7		: out	std_logic;	--LED
		pin_led_8		: out	std_logic;	--LED
		pin_led_in_1	: out	std_logic;	--LED
		pin_led_in_2	: out	std_logic;	--LED
		pin_led_out_1	: out	std_logic;	--LED
		pin_led_out_2	: out	std_logic;	--LED
		pin_lcd_reset	: out	std_logic;	--Display
		pin_lcd_rs		: out	std_logic;	--Display
		pin_lcd_csb		: out	std_logic;	--Display
		pin_lcd_rw		: out	std_logic;	--Display
		pin_lcd_strb	: out	std_logic;	--Display
		pin_lcd_psb		: out	std_logic;	--Display
		pin_lcd_d_0		: out	std_logic;	--Display
		pin_lcd_d_1		: out	std_logic;	--Display
		pin_lcd_d_2		: out	std_logic;	--Display
		pin_lcd_d_3		: out	std_logic;	--Display
		pin_lcd_d_4		: out	std_logic;	--Display
		pin_lcd_d_5		: out	std_logic;	--Display
		pin_lcd_d_6		: out	std_logic;	--Display
		pin_lcd_d_7		: out	std_logic;	--Display
		pin_ta_1		: in	std_logic;	--Taster
		pin_ta_2		: in	std_logic;	--Taster
		pin_enc_a		: in	std_logic;	--Encoder
		pin_enc_b		: in	std_logic;	--Encoder
		pin_out_1		: out	std_logic;	--Output-BNC
		pin_out_2		: out	std_logic;	--Output-BNC
		--pin_out_3		: inout	std_logic;	--Output
		--pin_out_4		: inout	std_logic;	--Output
		--pin_out_5		: inout	std_logic;	--Output
		--pin_out_6		: inout	std_logic;	--Output
		--pin_out_7		: inout	std_logic;	--Output
		--pin_out_8		: inout	std_logic;	--Output
		pin_in_1		: in	std_logic;	--Input-BNC
		pin_in_2		: in	std_logic;	--Input-BNC
		--pin_in_3		: inout	std_logic;	--Input
		--pin_in_4		: inout	std_logic;	--Input
		--pin_in_5		: inout	std_logic;	--Input
		--pin_in_6		: inout	std_logic;	--Input
		pin_tp_1		: out	std_logic	--Test-Point
		--pin_tp_2		: inout	std_logic;	--Test-Point
		--pin_tp_3		: inout	std_logic;	--Test-Point
		--pin_tp_4		: inout	std_logic;	--Test-Point
		--pin_tp_5		: inout	std_logic;	--Test-Point
		--pin_tp_6		: inout	std_logic;	--Test-Point
		--pin_tp_7		: inout	std_logic;	--Test-Point
		--pin_tp_8		: inout	std_logic;	--Test-Point
		--pin_tp_9		: inout	std_logic;	--Test-Point
		--pin_tp_10		: inout	std_logic;	--Test-Point
		--pin_tp_11		: inout	std_logic;	--Test-Point
		--pin_tp_12		: inout	std_logic	--Test-Point
	);
end TinyFPGA_A2;




--	2.08 2.77 4.16 5.54  8.31 11.08 15.65  29.56
--	2.15 2.89 4.29 5.78  8.58 11.57 16.63  33.25
--	2.22 3.02 4.43 6.05  8.87 12.09 17.73  38.00
--	2.29 3.17 4.59 6.33  9.17 12.67 19.00  44.33
--	2.38 3.33 4.75 6.65  9.50 13.30 20.46  53.20
--	2.46 3.50 4.93 7.00  9.85 14.00 22.17  66.50
--	2.56 3.69 5.12 7.39 10.23 14.78 24.18  88.67
--	2.66 3.91 5.32 7.82 10.64 15.65 26.60 133.00

architecture behavioral of TinyFPGA_A2 is

component i2cSlave port(
	clk		: in	std_logic;
	rst		: in	std_logic;
	sda		: in	std_logic;
	scl		: in	std_logic;
	myReg0	: out	std_logic_vector(7 downto 0);
	myReg1	: out	std_logic_vector(7 downto 0);
	myReg2	: out	std_logic_vector(7 downto 0);
	myReg3	: out	std_logic_vector(7 downto 0);
	myReg4	: in	std_logic_vector(7 downto 0);
	myReg5	: in	std_logic_vector(7 downto 0);
	myReg6	: in	std_logic_vector(7 downto 0);
	myReg7	: in	std_logic_vector(7 downto 0);
	i2c_done: out	std_logic;
	addr	: out	std_logic_vector(7 downto 0);
	data	: out	std_logic_vector(7 downto 0) );
end component;

component drehimpulsgeber is
	port(
		a			: in	std_logic;
		b			: in	std_logic;
		reset		: in	std_logic;
		clk			: in	std_logic;
		counter		: out	std_logic_vector(7 downto 0);
		count_up	: out	std_logic;
		count_down	: out	std_logic
	);
end component;

--component spi_slave is
--	port(
--		resetbuffer		: in	std_logic;
--		transmittenable	: in	std_logic;
--		tdata			: in	std_logic_vector(7 downto 0);
--		msbfirst		: in	std_logic;
--		cs				: in	std_logic;
--		sck				: in	std_logic;
--		mosi			: in	std_logic;
--		miso			: out	std_logic;
--		done			: out	std_logic;
--		rdata			: out	std_logic_vector(7 downto 0)
--	);
--end component;

component LCD is
	port(
		clk		: in	std_logic;
		send_n	: in	std_logic;
		addr	: in	std_logic_vector(7 downto 0);
		data	: in	std_logic_vector(7 downto 0);
		LCD_Bus	: out	std_logic_vector(7 downto 0);
		E		: out	std_logic;
		RS		: out	std_logic
	);
end component;

component fifo
	port (
		Data		: in	std_logic_vector(0 downto 0);
		WrClock		: in	std_logic; 
		RdClock		: in	std_logic;
		WrEn		: in	std_logic;
		RdEn		: in	std_logic; 
		Reset		: in	std_logic;
		RPReset		: in	std_logic; 
		Q			: out	std_logic_vector(0 downto 0);
		Empty		: out	std_logic; 
		Full		: out	std_logic;
		AlmostEmpty	: out	std_logic; 
		AlmostFull	: out	std_logic
	);
end component;

component pll
	port (
		CLKI		: in	std_logic;
		CLKOP		: out	std_logic
	);
end component;

--component serialize is
--	Port (
--		clk			: in	std_logic;
--		reset		: in	std_logic;
--		data		: in	std_logic_vector(7 downto 0);
--		tmp_out		: out	std_logic_vector(7 downto 0);
--		load		: out	std_logic;
--		serial_out	: out	std_logic
--	);
--end component;

	signal	clk_mst				: std_logic;
	signal	clk_int				: std_logic;
	signal	led_timer			: std_logic_vector (31 downto 0);
	signal	led_output			: std_logic_vector (7 downto 0);

	signal	mode_reg1			: std_logic_vector (7 downto 0);
	signal	mode_reg2			: std_logic_vector (7 downto 0);
	signal	data_reg3			: std_logic_vector (7 downto 0);
	signal	data_reg4			: std_logic_vector (7 downto 0);
	signal	data_reg6			: std_logic_vector (7 downto 0);
	signal	enc_counter			: std_logic_vector (7 downto 0);
	signal	regAddr				: std_logic_vector (7 downto 0);
	signal	data				: std_logic_vector (7 downto 0);
	signal	i2c_done			: std_logic;

	signal	count_up			: std_logic;
	signal	count_down			: std_logic;
	
	signal	E					: std_logic;
	signal	RS					: std_logic;
	signal	LCD_Bus				: std_logic_vector (7 downto 0);

	signal	done				: std_logic;
	signal	rdata				: std_logic_vector (0 downto 0);

	signal  countdown			: std_logic_vector (15 downto 0);
	signal	toggle				: std_logic;
	signal	var_clk				: std_logic;
	signal	output_clk			: std_logic;
	signal	output_data			: std_logic_vector (0 downto 0);
	signal	output_load			: std_logic;
	signal	output_pin			: std_logic;
	signal	fifo_state			: std_logic_vector (3 downto 0);
	signal	fifo_write_enable	: std_logic;

begin

	
	--output_clk <= pin_gpio_04 and not mode_reg1(0) and '1';
	process (pin_gpio_04)
	begin
		if rising_edge(pin_gpio_04) then
			output_clk <= not output_clk;
		end if;
	end process;

	--output_data <= data_reg3;
	--pin_gpio_26 <= output_load;
	pin_gpio_26 <= fifo_state(2);
	pin_gpio_06 <= fifo_state(1);
	--rdata <= data;
	--done <= i2c_done;

	done <= pin_spi_0_sclk and '1';
	rdata(0) <= pin_spi_0_mosi and '1';
	output_pin <= output_data(0);
	output_load <= output_clk;
	fifo_write_enable <= pin_gpio_05 and '1';


	process (clk_int)
	begin
		if rising_edge(clk_int) then
			led_timer(31 downto 1) <= std_logic_vector(unsigned(led_timer(31 downto 1)) + 1);
		end if;
	end process;

	led_timer(0) <= clk_int;
	var_clk <= led_timer(to_integer(unsigned(data_reg3)));
	
	process (var_clk)
	begin
		if rising_edge(var_clk) then
			if(unsigned(countdown) = 0) then
				countdown(7 downto 0) <= data_reg4;
				countdown(15 downto 8) <= mode_reg2;
				toggle <= not toggle;
			else
				countdown <= std_logic_vector(unsigned(countdown) - 1);
			end if;
		end if;
	end process;

	pin_led_1	<= not led_output(0);
	pin_led_2	<= not led_output(1);
	pin_led_3	<= not led_output(2);
	pin_led_4	<= not led_output(3);
	pin_led_5	<= not led_output(4);
	pin_led_6	<= not led_output(5);
	pin_led_7	<= not led_output(6);
	pin_led_8	<= not led_output(7);

	
	pin_led_in_1 <= pin_in_1 and '1';
	pin_led_in_2 <= pin_in_2 and '1';
	pin_led_out_1 <= led_timer(26) and pin_ta_1;
	pin_led_out_2 <= led_timer(27) nand pin_ta_2;

	--pin_out_1 <= pin_spi_0_mosi and '1';
	--pin_out_2 <= pin_spi_0_sclk and '1';

	pin_out_1 <= output_clk;
	pin_out_2 <= output_pin; -- and not fifo_state(0);

	pin_tp_1 <= toggle;
	--pin_tp_1 <= '0';

	led_output(0) <= led_timer(29);
	led_output(1) <= led_timer(28);
	led_output(2) <= led_timer(27);
	led_output(3) <= led_timer(26);
	led_output(4) <= fifo_state(3);
	led_output(5) <= fifo_state(2);
	led_output(6) <= fifo_state(1);
	led_output(7) <= fifo_state(0);

	data_reg6(0)    <= pin_ta_1 and '1';
	data_reg6(1)    <= pin_ta_2 nand '1';
	data_reg6(2)    <= '0';
	data_reg6(3)    <= '0';
	data_reg6(4)    <= '0';
	data_reg6(5)    <= '0';
	data_reg6(6)    <= '0';
	data_reg6(7)    <= '0';
	
	pin_lcd_reset <= '1'; --1'b1;
	pin_lcd_csb   <= '0'; --1'b0;
	pin_lcd_psb   <= '1'; --1'b1;
	pin_lcd_strb  <= E; --E;
	pin_lcd_rw    <= '0'; --1'b0;
	pin_lcd_rs    <= RS; --RS;

	pin_lcd_d_0 <= LCD_Bus(0);
	pin_lcd_d_1 <= LCD_Bus(1);
	pin_lcd_d_2 <= LCD_Bus(2);
	pin_lcd_d_3 <= LCD_Bus(3);
	pin_lcd_d_4 <= LCD_Bus(4);
	pin_lcd_d_5 <= LCD_Bus(5);
	pin_lcd_d_6 <= LCD_Bus(6);
	pin_lcd_d_7 <= LCD_Bus(7);


internal_oscillator_inst: osch
   generic map (NOM_FREQ  => "133.00")
   port map (STDBY => '0', OSC => clk_mst, SEDSTDBY => open);


i2cSlave_inst: i2cSlave
	port map (
		clk			=> led_timer(3),
		rst			=> '0',
		sda			=> pin_sda1,
		scl			=> pin_scl1,
		myReg0		=> mode_reg1,
		myReg1		=> mode_reg2,
		myReg2		=> data_reg3,
		myReg3		=> data_reg4,
		myReg4		=> enc_counter,
		myReg5		=> data_reg6,
		myReg6		=> fifo_state(3 downto 0) & fifo_state(3 downto 0),
		myReg7		=> (others => '0'),
		i2c_done	=> i2c_done,
		addr		=> regAddr,
		data		=> data
);

drehimpulsgeber_inst: drehimpulsgeber
port map(
	a			=> not pin_enc_a,
	b			=> not pin_enc_b,
	reset		=> '0',
	counter		=> enc_counter,
	clk			=> led_timer(5),
	count_up	=> count_up,
	count_down	=> count_down
);

LCD_inst: LCD
port map(
	clk			=> led_timer(10),
	send_n		=> i2c_done,
	addr		=> regAddr,
	data		=> data,
	LCD_Bus		=> LCD_Bus,
	E			=> E,
	RS			=> RS
);

fifo_inst: fifo
port map (
	Data			=> rdata,
	WrClock			=> done,
	RdClock			=> output_load,
	WrEn			=> fifo_write_enable, ---Raspberry Pi write 
	RdEn			=> '1',
	Reset			=> data_reg6(0),
	RPReset			=> '0',
	Q				=> output_data,
	--Q				=> open,
	Empty			=> fifo_state(0), 
	Full			=> fifo_state(3),
	AlmostEmpty		=> fifo_state(1),
	AlmostFull		=> fifo_state(2)
);

--spi_slave_inst: spi_slave
--port map(
--	resetbuffer		=> not pin_gpio_08_cs,
--	transmittenable	=> '1',
--	tdata			=> enc_counter,
--	msbfirst		=> '1',
--	cs				=> pin_gpio_08_cs,
--	sck				=> pin_spi_0_sclk,
--	mosi			=> pin_spi_0_mosi,
--	miso			=> pin_spi_0_miso,
--	--done			=> done,
--	done			=> open,
--	--rdata			=> rdata
--	rdata			=> open
--);

pll_inst: pll
port map (
	CLKI	=> clk_mst,
	CLKOP	=> clk_int
);

--serialize_inst: serialize
--port map(
--	clk			=> output_clk,
--	reset		=> '0',
--	data		=> output_data,
--	tmp_out		=> open,
--	load		=> output_load,
--	serial_out	=> output_pin
--);


end behavioral;
