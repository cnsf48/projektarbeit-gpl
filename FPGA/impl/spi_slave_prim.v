// Verilog netlist produced by program LSE :  version Diamond (64-bit) 3.9.1.119
// Netlist written on Fri Nov 17 18:01:23 2017
//
// Verilog Description of module spi_slave
//

module spi_slave (resetbuffer, transmittenable, tdata, msbfirst, cs, 
            sck, mosi, miso, done, rdata) /* synthesis syn_module_defined=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(46[8:17])
    input resetbuffer;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(47[9:20])
    input transmittenable;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(47[33:48])
    input [7:0]tdata;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(48[15:20])
    input msbfirst;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(47[49:57])
    input cs;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(47[21:23])
    input sck;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(47[24:27])
    input mosi;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(47[28:32])
    output miso;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(49[10:14])
    output done;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(50[14:18])
    output [7:0]rdata;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(51[20:25])
    
    wire sck_c /* synthesis SET_AS_NETWORK=sck_c, is_clock=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(47[24:27])
    wire sck_N_19 /* synthesis is_inv_clock=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(53[13:17])
    
    wire VCC_net, GND_net, resetbuffer_c, cs_c, mosi_c, transmittenable_c, 
        msbfirst_c, tdata_c_7, tdata_c_6, tdata_c_5, tdata_c_4, tdata_c_3, 
        tdata_c_2, tdata_c_1, tdata_c_0, done_c, rdata_c_7, rdata_c_6, 
        rdata_c_5, rdata_c_4, rdata_c_3, rdata_c_2, rdata_c_1, rdata_c_0;
    wire [7:0]treg;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(53[13:17])
    wire [7:0]rreg;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(53[18:22])
    wire [3:0]nb;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(54[13:15])
    
    wire sout, miso_N_62, cs_N_41;
    wire [7:0]rreg_7__N_32;
    
    wire sck_c_enable_8, n20, n19, n18, n128, n129, n130, n131, 
        n132, n133;
    wire [7:0]treg_7__N_54;
    
    wire n7, n312;
    
    VLO i166 (.Z(GND_net));
    LUT4 i2_2_lut_rep_1_3_lut (.A(nb[1]), .B(nb[0]), .C(nb[2]), .Z(n312)) /* synthesis lut_function=(A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(72[7:11])
    defparam i2_2_lut_rep_1_3_lut.init = 16'h8080;
    FD1P3AX rreg_i0_i0 (.D(rreg_7__N_32[0]), .SP(cs_N_41), .CK(sck_c), 
            .Q(rreg[0]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(66[7] 75[6])
    defparam rreg_i0_i0.GSR = "ENABLED";
    FD1P3AX done_41 (.D(n312), .SP(cs_N_41), .CK(sck_c), .Q(done_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(66[7] 75[6])
    defparam done_41.GSR = "ENABLED";
    FD1P3AX rreg_i0_i3 (.D(rreg_7__N_32[3]), .SP(cs_N_41), .CK(sck_c), 
            .Q(rreg[3]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(66[7] 75[6])
    defparam rreg_i0_i3.GSR = "ENABLED";
    FD1P3AX rdata_i0_i1 (.D(rreg_7__N_32[0]), .SP(sck_c_enable_8), .CK(sck_c), 
            .Q(rdata_c_0));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(66[7] 75[6])
    defparam rdata_i0_i1.GSR = "ENABLED";
    FD1P3AY treg_i0_i0 (.D(treg_7__N_54[0]), .SP(cs_N_41), .CK(sck_N_19), 
            .Q(treg[0]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(83[7] 93[6])
    defparam treg_i0_i0.GSR = "ENABLED";
    LUT4 mux_14_i2_3_lut (.A(rreg[2]), .B(rreg[0]), .C(msbfirst_c), .Z(rreg_7__N_32[1])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(70[5:38])
    defparam mux_14_i2_3_lut.init = 16'hcaca;
    FD1P3AX rreg_i0_i2 (.D(rreg_7__N_32[2]), .SP(cs_N_41), .CK(sck_c), 
            .Q(rreg[2]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(66[7] 75[6])
    defparam rreg_i0_i2.GSR = "ENABLED";
    LUT4 mux_35_i1_4_lut (.A(tdata_c_0), .B(treg[1]), .C(n7), .D(msbfirst_c), 
         .Z(treg_7__N_54[0])) /* synthesis lut_function=(A (B+((D)+!C))+!A (B (C)+!B (C (D)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(86[9] 91[7])
    defparam mux_35_i1_4_lut.init = 16'hfaca;
    FD1P3AX rreg_i0_i1 (.D(rreg_7__N_32[1]), .SP(cs_N_41), .CK(sck_c), 
            .Q(rreg[1]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(66[7] 75[6])
    defparam rreg_i0_i1.GSR = "ENABLED";
    INV i169 (.A(sck_c), .Z(sck_N_19));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(47[24:27])
    VHI i167 (.Z(VCC_net));
    OBZ miso_pad (.I(sout), .T(miso_N_62), .O(miso));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(58[10:14])
    TSALL TSALL_INST (.TSALL(GND_net));
    LUT4 i165_2_lut_3_lut_4_lut (.A(nb[1]), .B(nb[0]), .C(cs_c), .D(nb[2]), 
         .Z(sck_c_enable_8)) /* synthesis lut_function=(!(((C+!(D))+!B)+!A)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(72[7:11])
    defparam i165_2_lut_3_lut_4_lut.init = 16'h0800;
    LUT4 i2_3_lut (.A(nb[2]), .B(nb[1]), .C(nb[0]), .Z(n7)) /* synthesis lut_function=(A+(B+(C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(85[7:12])
    defparam i2_3_lut.init = 16'hfefe;
    FD1P3AX rreg_i0_i5 (.D(rreg_7__N_32[5]), .SP(cs_N_41), .CK(sck_c), 
            .Q(rreg[5]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(66[7] 75[6])
    defparam rreg_i0_i5.GSR = "ENABLED";
    LUT4 treg_0__I_0_3_lut (.A(treg[0]), .B(treg[7]), .C(msbfirst_c), 
         .Z(sout)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(57[15:39])
    defparam treg_0__I_0_3_lut.init = 16'hcaca;
    LUT4 mux_14_i7_3_lut (.A(rreg[7]), .B(rreg[5]), .C(msbfirst_c), .Z(rreg_7__N_32[6])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(70[5:38])
    defparam mux_14_i7_3_lut.init = 16'hcaca;
    LUT4 mux_14_i3_3_lut (.A(rreg[3]), .B(rreg[1]), .C(msbfirst_c), .Z(rreg_7__N_32[2])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(70[5:38])
    defparam mux_14_i3_3_lut.init = 16'hcaca;
    LUT4 mux_14_i8_3_lut (.A(mosi_c), .B(rreg[6]), .C(msbfirst_c), .Z(rreg_7__N_32[7])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(70[5:38])
    defparam mux_14_i8_3_lut.init = 16'hcaca;
    LUT4 mux_14_i5_3_lut (.A(rreg[5]), .B(rreg[3]), .C(msbfirst_c), .Z(rreg_7__N_32[4])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(70[5:38])
    defparam mux_14_i5_3_lut.init = 16'hcaca;
    LUT4 mux_35_i2_3_lut (.A(tdata_c_1), .B(n133), .C(n7), .Z(treg_7__N_54[1])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(86[9] 91[7])
    defparam mux_35_i2_3_lut.init = 16'hcaca;
    LUT4 mux_34_i2_3_lut (.A(treg[2]), .B(treg[0]), .C(msbfirst_c), .Z(n133)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(90[6:40])
    defparam mux_34_i2_3_lut.init = 16'hcaca;
    OB done_pad (.I(done_c), .O(done));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(50[14:18])
    OB rdata_pad_7 (.I(rdata_c_7), .O(rdata[7]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(51[20:25])
    OB rdata_pad_6 (.I(rdata_c_6), .O(rdata[6]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(51[20:25])
    OB rdata_pad_5 (.I(rdata_c_5), .O(rdata[5]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(51[20:25])
    OB rdata_pad_4 (.I(rdata_c_4), .O(rdata[4]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(51[20:25])
    OB rdata_pad_3 (.I(rdata_c_3), .O(rdata[3]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(51[20:25])
    OB rdata_pad_2 (.I(rdata_c_2), .O(rdata[2]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(51[20:25])
    OB rdata_pad_1 (.I(rdata_c_1), .O(rdata[1]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(51[20:25])
    OB rdata_pad_0 (.I(rdata_c_0), .O(rdata[0]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(51[20:25])
    IB resetbuffer_pad (.I(resetbuffer), .O(resetbuffer_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(47[9:20])
    IB transmittenable_pad (.I(transmittenable), .O(transmittenable_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(47[33:48])
    IB tdata_pad_7 (.I(tdata[7]), .O(tdata_c_7));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(48[15:20])
    IB tdata_pad_6 (.I(tdata[6]), .O(tdata_c_6));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(48[15:20])
    IB tdata_pad_5 (.I(tdata[5]), .O(tdata_c_5));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(48[15:20])
    IB tdata_pad_4 (.I(tdata[4]), .O(tdata_c_4));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(48[15:20])
    IB tdata_pad_3 (.I(tdata[3]), .O(tdata_c_3));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(48[15:20])
    IB tdata_pad_2 (.I(tdata[2]), .O(tdata_c_2));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(48[15:20])
    IB tdata_pad_1 (.I(tdata[1]), .O(tdata_c_1));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(48[15:20])
    IB tdata_pad_0 (.I(tdata[0]), .O(tdata_c_0));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(48[15:20])
    IB msbfirst_pad (.I(msbfirst), .O(msbfirst_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(47[49:57])
    IB cs_pad (.I(cs), .O(cs_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(47[21:23])
    IB sck_pad (.I(sck), .O(sck_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(47[24:27])
    IB mosi_pad (.I(mosi), .O(mosi_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(47[28:32])
    FD1P3AX rreg_i0_i6 (.D(rreg_7__N_32[6]), .SP(cs_N_41), .CK(sck_c), 
            .Q(rreg[6]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(66[7] 75[6])
    defparam rreg_i0_i6.GSR = "ENABLED";
    FD1P3AX rreg_i0_i7 (.D(rreg_7__N_32[7]), .SP(cs_N_41), .CK(sck_c), 
            .Q(rreg[7]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(66[7] 75[6])
    defparam rreg_i0_i7.GSR = "ENABLED";
    FD1P3AX rdata_i0_i2 (.D(rreg_7__N_32[1]), .SP(sck_c_enable_8), .CK(sck_c), 
            .Q(rdata_c_1));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(66[7] 75[6])
    defparam rdata_i0_i2.GSR = "ENABLED";
    FD1P3AX rdata_i0_i3 (.D(rreg_7__N_32[2]), .SP(sck_c_enable_8), .CK(sck_c), 
            .Q(rdata_c_2));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(66[7] 75[6])
    defparam rdata_i0_i3.GSR = "ENABLED";
    FD1P3AX rdata_i0_i4 (.D(rreg_7__N_32[3]), .SP(sck_c_enable_8), .CK(sck_c), 
            .Q(rdata_c_3));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(66[7] 75[6])
    defparam rdata_i0_i4.GSR = "ENABLED";
    FD1P3AX rdata_i0_i5 (.D(rreg_7__N_32[4]), .SP(sck_c_enable_8), .CK(sck_c), 
            .Q(rdata_c_4));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(66[7] 75[6])
    defparam rdata_i0_i5.GSR = "ENABLED";
    FD1P3AX rdata_i0_i6 (.D(rreg_7__N_32[5]), .SP(sck_c_enable_8), .CK(sck_c), 
            .Q(rdata_c_5));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(66[7] 75[6])
    defparam rdata_i0_i6.GSR = "ENABLED";
    FD1P3AX rdata_i0_i7 (.D(rreg_7__N_32[6]), .SP(sck_c_enable_8), .CK(sck_c), 
            .Q(rdata_c_6));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(66[7] 75[6])
    defparam rdata_i0_i7.GSR = "ENABLED";
    FD1P3AX rdata_i0_i8 (.D(rreg_7__N_32[7]), .SP(sck_c_enable_8), .CK(sck_c), 
            .Q(rdata_c_7));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(66[7] 75[6])
    defparam rdata_i0_i8.GSR = "ENABLED";
    FD1P3AY treg_i0_i1 (.D(treg_7__N_54[1]), .SP(cs_N_41), .CK(sck_N_19), 
            .Q(treg[1]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(83[7] 93[6])
    defparam treg_i0_i1.GSR = "ENABLED";
    FD1P3AY treg_i0_i2 (.D(treg_7__N_54[2]), .SP(cs_N_41), .CK(sck_N_19), 
            .Q(treg[2]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(83[7] 93[6])
    defparam treg_i0_i2.GSR = "ENABLED";
    FD1P3AY treg_i0_i3 (.D(treg_7__N_54[3]), .SP(cs_N_41), .CK(sck_N_19), 
            .Q(treg[3]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(83[7] 93[6])
    defparam treg_i0_i3.GSR = "ENABLED";
    FD1P3AY treg_i0_i4 (.D(treg_7__N_54[4]), .SP(cs_N_41), .CK(sck_N_19), 
            .Q(treg[4]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(83[7] 93[6])
    defparam treg_i0_i4.GSR = "ENABLED";
    FD1P3AY treg_i0_i5 (.D(treg_7__N_54[5]), .SP(cs_N_41), .CK(sck_N_19), 
            .Q(treg[5]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(83[7] 93[6])
    defparam treg_i0_i5.GSR = "ENABLED";
    FD1P3AY treg_i0_i6 (.D(treg_7__N_54[6]), .SP(cs_N_41), .CK(sck_N_19), 
            .Q(treg[6]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(83[7] 93[6])
    defparam treg_i0_i6.GSR = "ENABLED";
    FD1P3AY treg_i0_i7 (.D(treg_7__N_54[7]), .SP(cs_N_41), .CK(sck_N_19), 
            .Q(treg[7]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(83[7] 93[6])
    defparam treg_i0_i7.GSR = "ENABLED";
    LUT4 mux_35_i3_3_lut (.A(tdata_c_2), .B(n132), .C(n7), .Z(treg_7__N_54[2])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(86[9] 91[7])
    defparam mux_35_i3_3_lut.init = 16'hcaca;
    LUT4 mux_34_i3_3_lut (.A(treg[3]), .B(treg[1]), .C(msbfirst_c), .Z(n132)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(90[6:40])
    defparam mux_34_i3_3_lut.init = 16'hcaca;
    LUT4 mux_35_i4_3_lut (.A(tdata_c_3), .B(n131), .C(n7), .Z(treg_7__N_54[3])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(86[9] 91[7])
    defparam mux_35_i4_3_lut.init = 16'hcaca;
    LUT4 i150_2_lut_3_lut (.A(nb[1]), .B(nb[0]), .C(nb[2]), .Z(n18)) /* synthesis lut_function=(!(A (B (C)+!B !(C))+!A !(C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(72[7:11])
    defparam i150_2_lut_3_lut.init = 16'h7878;
    LUT4 mux_35_i5_3_lut (.A(tdata_c_4), .B(n130), .C(n7), .Z(treg_7__N_54[4])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(86[9] 91[7])
    defparam mux_35_i5_3_lut.init = 16'hcaca;
    LUT4 mux_34_i5_3_lut (.A(treg[5]), .B(treg[3]), .C(msbfirst_c), .Z(n130)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(90[6:40])
    defparam mux_34_i5_3_lut.init = 16'hcaca;
    LUT4 mux_35_i6_3_lut (.A(tdata_c_5), .B(n129), .C(n7), .Z(treg_7__N_54[5])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(86[9] 91[7])
    defparam mux_35_i6_3_lut.init = 16'hcaca;
    LUT4 mux_34_i6_3_lut (.A(treg[6]), .B(treg[4]), .C(msbfirst_c), .Z(n129)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(90[6:40])
    defparam mux_34_i6_3_lut.init = 16'hcaca;
    LUT4 mux_35_i7_3_lut (.A(tdata_c_6), .B(n128), .C(n7), .Z(treg_7__N_54[6])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(86[9] 91[7])
    defparam mux_35_i7_3_lut.init = 16'hcaca;
    LUT4 mux_34_i7_3_lut (.A(treg[7]), .B(treg[5]), .C(msbfirst_c), .Z(n128)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(90[6:40])
    defparam mux_34_i7_3_lut.init = 16'hcaca;
    LUT4 mux_35_i8_4_lut (.A(tdata_c_7), .B(treg[6]), .C(n7), .D(msbfirst_c), 
         .Z(treg_7__N_54[7])) /* synthesis lut_function=(A (B+!(C (D)))+!A (B (C)+!B !((D)+!C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(86[9] 91[7])
    defparam mux_35_i8_4_lut.init = 16'hcafa;
    LUT4 i141_1_lut (.A(nb[0]), .Z(n20)) /* synthesis lut_function=(!(A)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(72[7:11])
    defparam i141_1_lut.init = 16'h5555;
    LUT4 mux_14_i6_3_lut (.A(rreg[6]), .B(rreg[4]), .C(msbfirst_c), .Z(rreg_7__N_32[5])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(70[5:38])
    defparam mux_14_i6_3_lut.init = 16'hcaca;
    LUT4 mux_34_i4_3_lut (.A(treg[4]), .B(treg[2]), .C(msbfirst_c), .Z(n131)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(90[6:40])
    defparam mux_34_i4_3_lut.init = 16'hcaca;
    LUT4 i163_2_lut (.A(cs_c), .B(transmittenable_c), .Z(miso_N_62)) /* synthesis lut_function=(A+!(B)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(58[10:14])
    defparam i163_2_lut.init = 16'hbbbb;
    LUT4 i143_2_lut (.A(nb[1]), .B(nb[0]), .Z(n19)) /* synthesis lut_function=(!(A (B)+!A !(B))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(72[7:11])
    defparam i143_2_lut.init = 16'h6666;
    PUR PUR_INST (.PUR(VCC_net));
    defparam PUR_INST.RST_PULSE = 1;
    LUT4 mux_14_i1_3_lut (.A(rreg[1]), .B(mosi_c), .C(msbfirst_c), .Z(rreg_7__N_32[0])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(70[5:38])
    defparam mux_14_i1_3_lut.init = 16'hcaca;
    LUT4 cs_I_0_1_lut (.A(cs_c), .Z(cs_N_41)) /* synthesis lut_function=(!(A)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(58[17:22])
    defparam cs_I_0_1_lut.init = 16'h5555;
    LUT4 mux_14_i4_3_lut (.A(rreg[4]), .B(rreg[2]), .C(msbfirst_c), .Z(rreg_7__N_32[3])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(70[5:38])
    defparam mux_14_i4_3_lut.init = 16'hcaca;
    GSR GSR_INST (.GSR(resetbuffer_c));
    FD1P3AX rreg_i0_i4 (.D(rreg_7__N_32[4]), .SP(cs_N_41), .CK(sck_c), 
            .Q(rreg[4]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(66[7] 75[6])
    defparam rreg_i0_i4.GSR = "ENABLED";
    FD1P3IX nb_74_75__i1 (.D(n20), .SP(cs_N_41), .CD(sck_c_enable_8), 
            .CK(sck_c), .Q(nb[0]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(72[7:11])
    defparam nb_74_75__i1.GSR = "ENABLED";
    FD1P3IX nb_74_75__i2 (.D(n19), .SP(cs_N_41), .CD(sck_c_enable_8), 
            .CK(sck_c), .Q(nb[1]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(72[7:11])
    defparam nb_74_75__i2.GSR = "ENABLED";
    FD1P3IX nb_74_75__i3 (.D(n18), .SP(cs_N_41), .CD(sck_c_enable_8), 
            .CK(sck_c), .Q(nb[2]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_2/impl/source/spi_slave.v(72[7:11])
    defparam nb_74_75__i3.GSR = "ENABLED";
    
endmodule
//
// Verilog Description of module TSALL
// module not written out since it is a black-box. 
//

//
// Verilog Description of module PUR
// module not written out since it is a black-box. 
//

