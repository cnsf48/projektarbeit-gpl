// Verilog netlist produced by program LSE :  version Diamond (64-bit) 3.9.1.119
// Netlist written on Wed Dec 20 11:15:08 2017
//
// Verilog Description of module LCD
//

module LCD (clk, send, addr, data, LCD_Bus, E, RS, out) /* synthesis syn_module_defined=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(1[8:11])
    input clk;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(12[8:11])
    input send;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(13[8:12])
    input [7:0]addr;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(14[14:18])
    input [7:0]data;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(15[14:18])
    output [7:0]LCD_Bus;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(16[14:21])
    output E;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(18[8:9])
    output RS;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(20[8:10])
    output [7:0]out;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(23[14:17])
    
    wire clk_c /* synthesis SET_AS_NETWORK=clk_c, is_clock=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(12[8:11])
    wire clk_low /* synthesis is_clock=1, SET_AS_NETWORK=clk_low */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(28[6:13])
    wire out_7__N_9 /* synthesis is_clock=1, SET_AS_NETWORK=out_7__N_9 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(23[14:17])
    wire E_N_37 /* synthesis is_clock=1, SET_AS_NETWORK=E_N_37 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(18[8:9])
    
    wire GND_net, VCC_net, send_c, addr_c_7, addr_c_6, addr_c_5, 
        addr_c_4, addr_c_3, addr_c_2, addr_c_1, addr_c_0, LCD_Bus_c_7_c, 
        LCD_Bus_c_6_c, LCD_Bus_c_5_c, LCD_Bus_c_4_c, LCD_Bus_c_3_c, 
        LCD_Bus_c_2_c, LCD_Bus_c_1_c, LCD_Bus_c_0_c, E_c, RS_c, out_c_7, 
        out_c_6, out_c_5, out_c_4, out_c_2, out_c_1, out_c_0;
    wire [2:0]state;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(26[11:16])
    wire [2:0]next_state;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(27[11:21])
    
    wire n3, n2, n336, n255, RS_N_39, E_N_36, next_state_2__N_13, 
        n20, n19, n18, n257, n14, n353, n9, n268, state_0_derived_1;
    
    VHI i2 (.Z(VCC_net));
    CCU2D clk_div_129_130_add_4_3 (.A0(n2), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(clk_low), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n336), .S0(n19), .S1(n18));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(87[16:27])
    defparam clk_div_129_130_add_4_3.INIT0 = 16'hfaaa;
    defparam clk_div_129_130_add_4_3.INIT1 = 16'hfaaa;
    defparam clk_div_129_130_add_4_3.INJECT1_0 = "NO";
    defparam clk_div_129_130_add_4_3.INJECT1_1 = "NO";
    FD1S1I out_7__I_0_i5 (.D(n255), .CK(out_7__N_9), .CD(state[0]), .Q(out_c_4));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(41[1] 82[4])
    defparam out_7__I_0_i5.GSR = "ENABLED";
    FD1S1I next_state_2__I_0_i1 (.D(n353), .CK(next_state_2__N_13), .CD(state[2]), 
           .Q(next_state[0]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(41[1] 82[4])
    defparam next_state_2__I_0_i1.GSR = "ENABLED";
    FD1S1A E_I_0 (.D(E_N_36), .CK(E_N_37), .Q(E_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(41[1] 82[4])
    defparam E_I_0.GSR = "ENABLED";
    FD1S1I out_7__I_0_i3 (.D(E_N_37), .CK(out_7__N_9), .CD(n353), .Q(out_c_2));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(41[1] 82[4])
    defparam out_7__I_0_i3.GSR = "ENABLED";
    FD1S1A out_7__I_0_i1 (.D(state_0_derived_1), .CK(out_7__N_9), .Q(out_c_0));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(41[1] 82[4])
    defparam out_7__I_0_i1.GSR = "ENABLED";
    LUT4 i201_2_lut_rep_4_3_lut (.A(state[1]), .B(state[2]), .C(state[0]), 
         .Z(state_0_derived_1)) /* synthesis lut_function=(!(A+(B+!(C)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(42[2] 80[9])
    defparam i201_2_lut_rep_4_3_lut.init = 16'h1010;
    OB LCD_Bus_pad_6 (.I(LCD_Bus_c_6_c), .O(LCD_Bus[6]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(16[14:21])
    OB LCD_Bus_pad_7 (.I(LCD_Bus_c_7_c), .O(LCD_Bus[7]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(16[14:21])
    FD1S1I next_state_2__I_0_i3 (.D(E_N_37), .CK(next_state_2__N_13), .CD(n353), 
           .Q(next_state[2]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(41[1] 82[4])
    defparam next_state_2__I_0_i3.GSR = "ENABLED";
    FD1S1A out_7__I_0_i2 (.D(E_N_36), .CK(out_7__N_9), .Q(out_c_1));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(41[1] 82[4])
    defparam out_7__I_0_i2.GSR = "ENABLED";
    FD1S3AX state_i0 (.D(next_state[0]), .CK(clk_low), .Q(state[0]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(92[8] 96[4])
    defparam state_i0.GSR = "ENABLED";
    LUT4 i200_2_lut (.A(state[1]), .B(state[2]), .Z(E_N_37)) /* synthesis lut_function=(!((B)+!A)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(42[2] 80[9])
    defparam i200_2_lut.init = 16'h2222;
    LUT4 i203_4_lut (.A(send_c), .B(state[2]), .C(state[1]), .D(state[0]), 
         .Z(next_state_2__N_13)) /* synthesis lut_function=(!(A (B+!(C+!(D)))+!A (B+!(C)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(42[2] 80[9])
    defparam i203_4_lut.init = 16'h3032;
    FD1S1I out_7__I_0_i6 (.D(n255), .CK(out_7__N_9), .CD(n353), .Q(out_c_5));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(41[1] 82[4])
    defparam out_7__I_0_i6.GSR = "ENABLED";
    FD1S1I out_7__I_0_i7 (.D(n257), .CK(out_7__N_9), .CD(state[0]), .Q(out_c_6));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(41[1] 82[4])
    defparam out_7__I_0_i7.GSR = "ENABLED";
    FD1S1I out_7__I_0_i8 (.D(n257), .CK(out_7__N_9), .CD(n353), .Q(out_c_7));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(41[1] 82[4])
    defparam out_7__I_0_i8.GSR = "ENABLED";
    FD1S3AX clk_div_129_130__i0 (.D(n20), .CK(clk_c), .Q(n3)) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(87[16:27])
    defparam clk_div_129_130__i0.GSR = "ENABLED";
    FD1S1I RS_I_0 (.D(RS_N_39), .CK(state_0_derived_1), .CD(n268), .Q(RS_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(41[1] 82[4])
    defparam RS_I_0.GSR = "ENABLED";
    LUT4 i155_2_lut (.A(state[1]), .B(state[2]), .Z(n257)) /* synthesis lut_function=(A (B)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(42[2] 80[9])
    defparam i155_2_lut.init = 16'h8888;
    LUT4 i242_4_lut (.A(n9), .B(n14), .C(addr_c_2), .D(addr_c_4), .Z(RS_N_39)) /* synthesis lut_function=(!(A+(B+(C+!(D))))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(48[8:21])
    defparam i242_4_lut.init = 16'h0100;
    FD1S1I next_state_2__I_0_i2 (.D(E_N_37), .CK(next_state_2__N_13), .CD(state[0]), 
           .Q(next_state[1]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(41[1] 82[4])
    defparam next_state_2__I_0_i2.GSR = "ENABLED";
    LUT4 i1_2_lut (.A(addr_c_0), .B(addr_c_6), .Z(n9)) /* synthesis lut_function=(A+(B)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(48[8:21])
    defparam i1_2_lut.init = 16'heeee;
    FD1S3AX clk_div_129_130__i2 (.D(n18), .CK(clk_c), .Q(clk_low)) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(87[16:27])
    defparam clk_div_129_130__i2.GSR = "ENABLED";
    LUT4 i1_1_lut_rep_5 (.A(state[0]), .Z(n353)) /* synthesis lut_function=(!(A)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(42[2] 80[9])
    defparam i1_1_lut_rep_5.init = 16'h5555;
    LUT4 i6_4_lut (.A(addr_c_3), .B(addr_c_1), .C(addr_c_5), .D(addr_c_7), 
         .Z(n14)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(48[8:21])
    defparam i6_4_lut.init = 16'hfffe;
    LUT4 i2_3_lut_3_lut (.A(state[0]), .B(state[1]), .C(state[2]), .Z(E_N_36)) /* synthesis lut_function=(!(A+((C)+!B))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(42[2] 80[9])
    defparam i2_3_lut_3_lut.init = 16'h0404;
    GSR GSR_INST (.GSR(VCC_net));
    FD1S3AX clk_div_129_130__i1 (.D(n19), .CK(clk_c), .Q(n2)) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(87[16:27])
    defparam clk_div_129_130__i1.GSR = "ENABLED";
    FD1S3AX state_i2 (.D(next_state[2]), .CK(clk_low), .Q(state[2]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(92[8] 96[4])
    defparam state_i2.GSR = "ENABLED";
    LUT4 i202_2_lut_3_lut (.A(state[1]), .B(state[2]), .C(state[0]), .Z(out_7__N_9)) /* synthesis lut_function=(A+(B+(C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(42[2] 80[9])
    defparam i202_2_lut_3_lut.init = 16'hfefe;
    TSALL TSALL_INST (.TSALL(GND_net));
    PUR PUR_INST (.PUR(VCC_net));
    defparam PUR_INST.RST_PULSE = 1;
    LUT4 i166_1_lut_2_lut_3_lut (.A(state[1]), .B(state[2]), .C(state[0]), 
         .Z(n268)) /* synthesis lut_function=(A+(B+!(C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(42[2] 80[9])
    defparam i166_1_lut_2_lut_3_lut.init = 16'hefef;
    CCU2D clk_div_129_130_add_4_1 (.A0(GND_net), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(n3), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .COUT(n336), .S1(n20));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(87[16:27])
    defparam clk_div_129_130_add_4_1.INIT0 = 16'hF000;
    defparam clk_div_129_130_add_4_1.INIT1 = 16'h0555;
    defparam clk_div_129_130_add_4_1.INJECT1_0 = "NO";
    defparam clk_div_129_130_add_4_1.INJECT1_1 = "NO";
    VLO i1 (.Z(GND_net));
    FD1S3AX state_i1 (.D(next_state[1]), .CK(clk_low), .Q(state[1]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(92[8] 96[4])
    defparam state_i1.GSR = "ENABLED";
    LUT4 i239_2_lut (.A(state[1]), .B(state[2]), .Z(n255)) /* synthesis lut_function=(!(A+!(B))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(42[2] 80[9])
    defparam i239_2_lut.init = 16'h4444;
    OB LCD_Bus_pad_5 (.I(LCD_Bus_c_5_c), .O(LCD_Bus[5]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(16[14:21])
    OB LCD_Bus_pad_4 (.I(LCD_Bus_c_4_c), .O(LCD_Bus[4]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(16[14:21])
    OB LCD_Bus_pad_3 (.I(LCD_Bus_c_3_c), .O(LCD_Bus[3]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(16[14:21])
    OB LCD_Bus_pad_2 (.I(LCD_Bus_c_2_c), .O(LCD_Bus[2]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(16[14:21])
    OB LCD_Bus_pad_1 (.I(LCD_Bus_c_1_c), .O(LCD_Bus[1]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(16[14:21])
    OB LCD_Bus_pad_0 (.I(LCD_Bus_c_0_c), .O(LCD_Bus[0]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(16[14:21])
    OB E_pad (.I(E_c), .O(E));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(18[8:9])
    OB RS_pad (.I(RS_c), .O(RS));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(20[8:10])
    OB out_pad_7 (.I(out_c_7), .O(out[7]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(23[14:17])
    OB out_pad_6 (.I(out_c_6), .O(out[6]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(23[14:17])
    OB out_pad_5 (.I(out_c_5), .O(out[5]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(23[14:17])
    OB out_pad_4 (.I(out_c_4), .O(out[4]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(23[14:17])
    OB out_pad_3 (.I(GND_net), .O(out[3]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(23[14:17])
    OB out_pad_2 (.I(out_c_2), .O(out[2]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(23[14:17])
    OB out_pad_1 (.I(out_c_1), .O(out[1]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(23[14:17])
    OB out_pad_0 (.I(out_c_0), .O(out[0]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(23[14:17])
    IB clk_pad (.I(clk), .O(clk_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(12[8:11])
    IB send_pad (.I(send), .O(send_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(13[8:12])
    IB addr_pad_7 (.I(addr[7]), .O(addr_c_7));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(14[14:18])
    IB addr_pad_6 (.I(addr[6]), .O(addr_c_6));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(14[14:18])
    IB addr_pad_5 (.I(addr[5]), .O(addr_c_5));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(14[14:18])
    IB addr_pad_4 (.I(addr[4]), .O(addr_c_4));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(14[14:18])
    IB addr_pad_3 (.I(addr[3]), .O(addr_c_3));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(14[14:18])
    IB addr_pad_2 (.I(addr[2]), .O(addr_c_2));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(14[14:18])
    IB addr_pad_1 (.I(addr[1]), .O(addr_c_1));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(14[14:18])
    IB addr_pad_0 (.I(addr[0]), .O(addr_c_0));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(14[14:18])
    IB LCD_Bus_c_7_pad (.I(data[7]), .O(LCD_Bus_c_7_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(15[14:18])
    IB LCD_Bus_c_6_pad (.I(data[6]), .O(LCD_Bus_c_6_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(15[14:18])
    IB LCD_Bus_c_5_pad (.I(data[5]), .O(LCD_Bus_c_5_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(15[14:18])
    IB LCD_Bus_c_4_pad (.I(data[4]), .O(LCD_Bus_c_4_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(15[14:18])
    IB LCD_Bus_c_3_pad (.I(data[3]), .O(LCD_Bus_c_3_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(15[14:18])
    IB LCD_Bus_c_2_pad (.I(data[2]), .O(LCD_Bus_c_2_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(15[14:18])
    IB LCD_Bus_c_1_pad (.I(data[1]), .O(LCD_Bus_c_1_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(15[14:18])
    IB LCD_Bus_c_0_pad (.I(data[0]), .O(LCD_Bus_c_0_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/lcd.v(15[14:18])
    
endmodule
//
// Verilog Description of module TSALL
// module not written out since it is a black-box. 
//

//
// Verilog Description of module PUR
// module not written out since it is a black-box. 
//

