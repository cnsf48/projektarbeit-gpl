// Verilog netlist produced by program LSE :  version Diamond (64-bit) 3.9.1.119
// Netlist written on Tue Jan 30 16:11:46 2018
//
// Verilog Description of module fifo
//

module fifo (Data, WrClock, RdClock, WrEn, RdEn, Reset, RPReset, 
            Q, Empty, Full, AlmostEmpty, AlmostFull) /* synthesis NGD_DRC_MASK=1 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(14[8:12])
    input [7:0]Data;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(16[9:13])
    input WrClock;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(17[9:16])
    input RdClock;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(18[9:16])
    input WrEn;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(19[9:13])
    input RdEn;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(20[9:13])
    input Reset;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(21[9:14])
    input RPReset;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(22[9:16])
    output [7:0]Q;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(23[9:10])
    output Empty;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(24[9:14])
    output Full;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(25[9:13])
    output AlmostEmpty;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(26[9:20])
    output AlmostFull;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(27[9:19])
    
    wire WrClock_c /* synthesis is_clock=1 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(17[9:16])
    
    wire Data_c_7, Data_c_6, Data_c_5, Data_c_4, Data_c_3, Data_c_2, 
        Data_c_1, Data_c_0, RdClock_c, WrEn_c, RdEn_c, Reset_c, 
        RPReset_c, Q_c_7, Q_c_6, Q_c_5, Q_c_4, Q_c_3, Q_c_2, Q_c_1, 
        Q_c_0, Empty_c, Full_c, AlmostEmpty_c, AlmostFull_c, scuba_vhi, 
        scuba_vlo;
    
    FIFO8KB fifo_1_2 (.DI0(scuba_vlo), .DI1(scuba_vlo), .DI2(Data_c_2), 
            .DI3(scuba_vlo), .DI4(scuba_vlo), .DI5(Data_c_3), .DI6(scuba_vlo), 
            .DI7(scuba_vlo), .DI8(scuba_vlo), .DI9(scuba_vlo), .DI10(scuba_vlo), 
            .DI11(Data_c_2), .DI12(scuba_vlo), .DI13(scuba_vlo), .DI14(scuba_vlo), 
            .DI15(scuba_vlo), .DI16(scuba_vlo), .DI17(scuba_vlo), .FULLI(Full_c), 
            .EMPTYI(Empty_c), .CSW1(scuba_vhi), .CSW0(scuba_vhi), .CSR1(scuba_vhi), 
            .CSR0(scuba_vhi), .WE(WrEn_c), .RE(RdEn_c), .ORE(RdEn_c), 
            .CLKW(WrClock_c), .CLKR(RdClock_c), .RST(Reset_c), .RPRST(RPReset_c), 
            .DO0(Q_c_2), .DO1(Q_c_3)) /* synthesis syn_instantiated=1 */ ;
    defparam fifo_1_2.DATA_WIDTH_W = 2;
    defparam fifo_1_2.DATA_WIDTH_R = 2;
    defparam fifo_1_2.REGMODE = "NOREG";
    defparam fifo_1_2.RESETMODE = "ASYNC";
    defparam fifo_1_2.ASYNC_RESET_RELEASE = "SYNC";
    defparam fifo_1_2.CSDECODE_W = "0b11";
    defparam fifo_1_2.CSDECODE_R = "0b11";
    defparam fifo_1_2.AEPOINTER = "0b11111111111110";
    defparam fifo_1_2.AEPOINTER1 = "0b00000000000000";
    defparam fifo_1_2.AFPOINTER = "0b11111111111110";
    defparam fifo_1_2.AFPOINTER1 = "0b00000000000000";
    defparam fifo_1_2.FULLPOINTER = "0b11111111111110";
    defparam fifo_1_2.FULLPOINTER1 = "0b00000000000000";
    defparam fifo_1_2.GSR = "DISABLED";
    FIFO8KB fifo_2_1 (.DI0(scuba_vlo), .DI1(scuba_vlo), .DI2(Data_c_4), 
            .DI3(scuba_vlo), .DI4(scuba_vlo), .DI5(Data_c_5), .DI6(scuba_vlo), 
            .DI7(scuba_vlo), .DI8(scuba_vlo), .DI9(scuba_vlo), .DI10(scuba_vlo), 
            .DI11(Data_c_4), .DI12(scuba_vlo), .DI13(scuba_vlo), .DI14(scuba_vlo), 
            .DI15(scuba_vlo), .DI16(scuba_vlo), .DI17(scuba_vlo), .FULLI(Full_c), 
            .EMPTYI(Empty_c), .CSW1(scuba_vhi), .CSW0(scuba_vhi), .CSR1(scuba_vhi), 
            .CSR0(scuba_vhi), .WE(WrEn_c), .RE(RdEn_c), .ORE(RdEn_c), 
            .CLKW(WrClock_c), .CLKR(RdClock_c), .RST(Reset_c), .RPRST(RPReset_c), 
            .DO0(Q_c_4), .DO1(Q_c_5)) /* synthesis syn_instantiated=1 */ ;
    defparam fifo_2_1.DATA_WIDTH_W = 2;
    defparam fifo_2_1.DATA_WIDTH_R = 2;
    defparam fifo_2_1.REGMODE = "NOREG";
    defparam fifo_2_1.RESETMODE = "ASYNC";
    defparam fifo_2_1.ASYNC_RESET_RELEASE = "SYNC";
    defparam fifo_2_1.CSDECODE_W = "0b11";
    defparam fifo_2_1.CSDECODE_R = "0b11";
    defparam fifo_2_1.AEPOINTER = "0b11111111111110";
    defparam fifo_2_1.AEPOINTER1 = "0b00000000000000";
    defparam fifo_2_1.AFPOINTER = "0b11111111111110";
    defparam fifo_2_1.AFPOINTER1 = "0b00000000000000";
    defparam fifo_2_1.FULLPOINTER = "0b11111111111110";
    defparam fifo_2_1.FULLPOINTER1 = "0b00000000000000";
    defparam fifo_2_1.GSR = "DISABLED";
    VHI scuba_vhi_inst (.Z(scuba_vhi));
    VLO scuba_vlo_inst (.Z(scuba_vlo));
    FIFO8KB fifo_3_0 (.DI0(scuba_vlo), .DI1(scuba_vlo), .DI2(Data_c_6), 
            .DI3(scuba_vlo), .DI4(scuba_vlo), .DI5(Data_c_7), .DI6(scuba_vlo), 
            .DI7(scuba_vlo), .DI8(scuba_vlo), .DI9(scuba_vlo), .DI10(scuba_vlo), 
            .DI11(Data_c_6), .DI12(scuba_vlo), .DI13(scuba_vlo), .DI14(scuba_vlo), 
            .DI15(scuba_vlo), .DI16(scuba_vlo), .DI17(scuba_vlo), .FULLI(Full_c), 
            .EMPTYI(Empty_c), .CSW1(scuba_vhi), .CSW0(scuba_vhi), .CSR1(scuba_vhi), 
            .CSR0(scuba_vhi), .WE(WrEn_c), .RE(RdEn_c), .ORE(RdEn_c), 
            .CLKW(WrClock_c), .CLKR(RdClock_c), .RST(Reset_c), .RPRST(RPReset_c), 
            .DO0(Q_c_6), .DO1(Q_c_7)) /* synthesis syn_instantiated=1 */ ;
    defparam fifo_3_0.DATA_WIDTH_W = 2;
    defparam fifo_3_0.DATA_WIDTH_R = 2;
    defparam fifo_3_0.REGMODE = "NOREG";
    defparam fifo_3_0.RESETMODE = "ASYNC";
    defparam fifo_3_0.ASYNC_RESET_RELEASE = "SYNC";
    defparam fifo_3_0.CSDECODE_W = "0b11";
    defparam fifo_3_0.CSDECODE_R = "0b11";
    defparam fifo_3_0.AEPOINTER = "0b11111111111110";
    defparam fifo_3_0.AEPOINTER1 = "0b00000000000000";
    defparam fifo_3_0.AFPOINTER = "0b11111111111110";
    defparam fifo_3_0.AFPOINTER1 = "0b00000000000000";
    defparam fifo_3_0.FULLPOINTER = "0b11111111111110";
    defparam fifo_3_0.FULLPOINTER1 = "0b00000000000000";
    defparam fifo_3_0.GSR = "DISABLED";
    OB Q_pad_0 (.I(Q_c_0), .O(Q[0]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(23[9:10])
    OB Q_pad_1 (.I(Q_c_1), .O(Q[1]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(23[9:10])
    OB Q_pad_2 (.I(Q_c_2), .O(Q[2]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(23[9:10])
    FIFO8KB fifo_0_3 (.DI0(scuba_vlo), .DI1(scuba_vlo), .DI2(Data_c_0), 
            .DI3(scuba_vlo), .DI4(scuba_vlo), .DI5(Data_c_1), .DI6(scuba_vlo), 
            .DI7(scuba_vlo), .DI8(scuba_vlo), .DI9(scuba_vlo), .DI10(scuba_vlo), 
            .DI11(Data_c_0), .DI12(scuba_vlo), .DI13(scuba_vlo), .DI14(scuba_vlo), 
            .DI15(scuba_vlo), .DI16(scuba_vlo), .DI17(scuba_vlo), .FULLI(Full_c), 
            .EMPTYI(Empty_c), .CSW1(scuba_vhi), .CSW0(scuba_vhi), .CSR1(scuba_vhi), 
            .CSR0(scuba_vhi), .WE(WrEn_c), .RE(RdEn_c), .ORE(RdEn_c), 
            .CLKW(WrClock_c), .CLKR(RdClock_c), .RST(Reset_c), .RPRST(RPReset_c), 
            .DO0(Q_c_0), .DO1(Q_c_1), .EF(Empty_c), .AEF(AlmostEmpty_c), 
            .AFF(AlmostFull_c), .FF(Full_c)) /* synthesis syn_instantiated=1 */ ;
    defparam fifo_0_3.DATA_WIDTH_W = 2;
    defparam fifo_0_3.DATA_WIDTH_R = 2;
    defparam fifo_0_3.REGMODE = "NOREG";
    defparam fifo_0_3.RESETMODE = "ASYNC";
    defparam fifo_0_3.ASYNC_RESET_RELEASE = "SYNC";
    defparam fifo_0_3.CSDECODE_W = "0b11";
    defparam fifo_0_3.CSDECODE_R = "0b11";
    defparam fifo_0_3.AEPOINTER = "0b00001000000000";
    defparam fifo_0_3.AEPOINTER1 = "0b00001000000010";
    defparam fifo_0_3.AFPOINTER = "0b01111000000000";
    defparam fifo_0_3.AFPOINTER1 = "0b01110111111110";
    defparam fifo_0_3.FULLPOINTER = "0b10000000000000";
    defparam fifo_0_3.FULLPOINTER1 = "0b01111111111110";
    defparam fifo_0_3.GSR = "DISABLED";
    IB Data_pad_6 (.I(Data[6]), .O(Data_c_6));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(16[9:13])
    IB Data_pad_7 (.I(Data[7]), .O(Data_c_7));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(16[9:13])
    OB Q_pad_3 (.I(Q_c_3), .O(Q[3]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(23[9:10])
    OB AlmostFull_pad (.I(AlmostFull_c), .O(AlmostFull));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(27[9:19])
    OB Q_pad_4 (.I(Q_c_4), .O(Q[4]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(23[9:10])
    OB AlmostEmpty_pad (.I(AlmostEmpty_c), .O(AlmostEmpty));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(26[9:20])
    OB Q_pad_5 (.I(Q_c_5), .O(Q[5]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(23[9:10])
    OB Full_pad (.I(Full_c), .O(Full));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(25[9:13])
    OB Q_pad_6 (.I(Q_c_6), .O(Q[6]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(23[9:10])
    OB Empty_pad (.I(Empty_c), .O(Empty));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(24[9:14])
    OB Q_pad_7 (.I(Q_c_7), .O(Q[7]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(23[9:10])
    IB Data_pad_5 (.I(Data[5]), .O(Data_c_5));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(16[9:13])
    IB Data_pad_4 (.I(Data[4]), .O(Data_c_4));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(16[9:13])
    IB Data_pad_3 (.I(Data[3]), .O(Data_c_3));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(16[9:13])
    IB Data_pad_2 (.I(Data[2]), .O(Data_c_2));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(16[9:13])
    IB Data_pad_1 (.I(Data[1]), .O(Data_c_1));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(16[9:13])
    IB Data_pad_0 (.I(Data[0]), .O(Data_c_0));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(16[9:13])
    IB WrClock_pad (.I(WrClock), .O(WrClock_c));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(17[9:16])
    IB RdClock_pad (.I(RdClock), .O(RdClock_c));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(18[9:16])
    IB WrEn_pad (.I(WrEn), .O(WrEn_c));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(19[9:13])
    IB RdEn_pad (.I(RdEn), .O(RdEn_c));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(20[9:13])
    IB Reset_pad (.I(Reset), .O(Reset_c));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(21[9:14])
    IB RPReset_pad (.I(RPReset), .O(RPReset_c));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2/fifo.vhd(22[9:16])
    GSR GSR_INST (.GSR(scuba_vhi));
    TSALL TSALL_INST (.TSALL(scuba_vlo));
    PUR PUR_INST (.PUR(scuba_vhi));
    defparam PUR_INST.RST_PULSE = 1;
    
endmodule
//
// Verilog Description of module TSALL
// module not written out since it is a black-box. 
//

//
// Verilog Description of module PUR
// module not written out since it is a black-box. 
//

