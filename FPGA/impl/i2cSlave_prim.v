// Verilog netlist produced by program LSE :  version Diamond (64-bit) 3.9.1.119
// Netlist written on Sat Dec 16 21:09:13 2017
//
// Verilog Description of module i2cSlave
//

module i2cSlave (clk, rst, sda, scl, myReg0, myReg1, myReg2, myReg3, 
            myReg4, myReg5, myReg6, myReg7) /* synthesis syn_module_defined=1 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(48[8:16])
    input clk;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(63[7:10])
    input rst;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(64[7:10])
    inout sda;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(65[7:10])
    input scl;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(66[7:10])
    output [7:0]myReg0;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(67[14:20])
    output [7:0]myReg1;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(68[14:20])
    output [7:0]myReg2;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(69[14:20])
    output [7:0]myReg3;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(70[14:20])
    input [7:0]myReg4;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(71[13:19])
    input [7:0]myReg5;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(72[13:19])
    input [7:0]myReg6;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(73[13:19])
    input [7:0]myReg7;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(74[13:19])
    
    wire clk_c /* synthesis SET_AS_NETWORK=clk_c, is_clock=1 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(63[7:10])
    
    wire GND_net, VCC_net, n10, scl_c, myReg0_c_7, myReg0_c_6, myReg0_c_5, 
        myReg0_c_4, myReg0_c_3, myReg0_c_2, myReg0_c_1, myReg0_c_0, 
        myReg1_c_7, myReg1_c_6, myReg1_c_5, myReg1_c_4, myReg1_c_3, 
        myReg1_c_2, myReg1_c_1, myReg1_c_0, myReg2_c_7, myReg2_c_6, 
        myReg2_c_5, myReg2_c_4, myReg2_c_3, myReg2_c_2, myReg2_c_1, 
        myReg2_c_0, myReg3_c_7, myReg3_c_6, myReg3_c_5, myReg3_c_4, 
        myReg3_c_3, myReg3_c_2, myReg3_c_1, myReg3_c_0, myReg4_c_7, 
        myReg4_c_6, myReg4_c_5, myReg4_c_4, myReg4_c_3, myReg4_c_2, 
        myReg4_c_1, myReg4_c_0, myReg5_c_7, myReg5_c_6, myReg5_c_5, 
        myReg5_c_4, myReg5_c_3, myReg5_c_2, myReg5_c_1, myReg5_c_0, 
        myReg6_c_7, myReg6_c_6, myReg6_c_5, myReg6_c_4, myReg6_c_3, 
        myReg6_c_2, myReg6_c_1, myReg6_c_0, myReg7_c_7, myReg7_c_6, 
        myReg7_c_5, myReg7_c_4, myReg7_c_3, myReg7_c_2, myReg7_c_1, 
        myReg7_c_0, sdaDeb, sclDeb;
    wire [6:0]sdaPipe;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(80[24:31])
    wire [6:0]sclPipe;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(81[24:31])
    wire [6:0]sclDelayed;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(83[24:34])
    wire [2:0]sdaDelayed;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(84[24:34])
    wire [1:0]startStopDetState;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(85[11:28])
    
    wire clearStartStopDet, sdaOut;
    wire [7:0]regAddr;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(89[12:19])
    wire [7:0]dataToRegIF;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(90[12:23])
    
    wire writeEn;
    wire [7:0]dataFromRegIF;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(92[12:25])
    wire [1:0]rstPipe;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(93[11:18])
    
    wire startEdgeDet;
    wire [1:0]rstPipe_1__N_28;
    
    wire sclDeb_N_43, sclDeb_N_42, startEdgeDet_N_45, n98, n3396, 
        n20, n3213, next_writeEn, n3637;
    wire [7:0]txData;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(87[11:17])
    
    wire n7, n3185, n3634, n1323, clk_c_enable_64, n3354, clk_c_enable_13, 
        n3392, n3183, n1373, sda_out, n1325, n6, clk_c_enable_50, 
        n3493, n279, n280, n281, n282, n283, n286, n287, n288, 
        n289, n290, n291, n292, n293, n15, n338, n16, n15_adj_407, 
        clk_c_enable_9, n3655, n1465, n1459, clk_c_enable_67, n4, 
        n56, n36, n15_adj_408, n14, n13, n3650, n3207, n4_adj_409, 
        n3648, clk_c_enable_65, n3208, n3647, n10_adj_410, n3645, 
        n7_adj_411, clk_c_enable_57, n3420, n3, n1, n3642, n8, 
        n8_adj_412, n7_adj_413, n3654;
    
    VHI i2 (.Z(VCC_net));
    registerInterface u_registerInterface (.myReg1_c_0(myReg1_c_0), .clk_c(clk_c), 
            .dataToRegIF({dataToRegIF}), .myReg2_c_0(myReg2_c_0), .myReg3_c_0(myReg3_c_0), 
            .myReg0_c_0(myReg0_c_0), .regAddr({regAddr}), .dataFromRegIF({dataFromRegIF}), 
            .myReg2_c_4(myReg2_c_4), .myReg3_c_4(myReg3_c_4), .myReg0_c_4(myReg0_c_4), 
            .myReg1_c_4(myReg1_c_4), .myReg6_c_6(myReg6_c_6), .myReg7_c_6(myReg7_c_6), 
            .myReg4_c_6(myReg4_c_6), .myReg5_c_6(myReg5_c_6), .myReg2_c_6(myReg2_c_6), 
            .myReg3_c_6(myReg3_c_6), .myReg0_c_6(myReg0_c_6), .myReg1_c_6(myReg1_c_6), 
            .writeEn(writeEn), .myReg6_c_7(myReg6_c_7), .myReg7_c_7(myReg7_c_7), 
            .myReg1_c_1(myReg1_c_1), .myReg1_c_2(myReg1_c_2), .myReg1_c_3(myReg1_c_3), 
            .myReg1_c_5(myReg1_c_5), .myReg1_c_7(myReg1_c_7), .myReg2_c_1(myReg2_c_1), 
            .myReg2_c_2(myReg2_c_2), .myReg2_c_3(myReg2_c_3), .myReg2_c_5(myReg2_c_5), 
            .myReg2_c_7(myReg2_c_7), .myReg3_c_1(myReg3_c_1), .myReg3_c_2(myReg3_c_2), 
            .myReg3_c_3(myReg3_c_3), .myReg3_c_5(myReg3_c_5), .myReg3_c_7(myReg3_c_7), 
            .myReg0_c_1(myReg0_c_1), .myReg0_c_2(myReg0_c_2), .myReg0_c_3(myReg0_c_3), 
            .myReg0_c_5(myReg0_c_5), .myReg0_c_7(myReg0_c_7), .myReg4_c_7(myReg4_c_7), 
            .myReg5_c_7(myReg5_c_7), .myReg6_c_3(myReg6_c_3), .myReg7_c_3(myReg7_c_3), 
            .myReg6_c_1(myReg6_c_1), .myReg7_c_1(myReg7_c_1), .myReg4_c_1(myReg4_c_1), 
            .myReg5_c_1(myReg5_c_1), .myReg6_c_5(myReg6_c_5), .myReg7_c_5(myReg7_c_5), 
            .myReg4_c_5(myReg4_c_5), .myReg5_c_5(myReg5_c_5), .myReg4_c_3(myReg4_c_3), 
            .myReg5_c_3(myReg5_c_3), .myReg6_c_2(myReg6_c_2), .myReg7_c_2(myReg7_c_2), 
            .myReg4_c_2(myReg4_c_2), .myReg5_c_2(myReg5_c_2), .myReg6_c_0(myReg6_c_0), 
            .myReg7_c_0(myReg7_c_0), .myReg4_c_0(myReg4_c_0), .myReg5_c_0(myReg5_c_0), 
            .myReg6_c_4(myReg6_c_4), .myReg7_c_4(myReg7_c_4), .myReg4_c_4(myReg4_c_4), 
            .myReg5_c_4(myReg5_c_4)) /* synthesis syn_module_defined=1 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(173[19] 187[2])
    FD1S3JX sdaPipe_i0 (.D(sda_out), .CK(clk_c), .PD(rstPipe[1]), .Q(sdaPipe[0]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(111[8] 130[4])
    defparam sdaPipe_i0.GSR = "ENABLED";
    FD1S3JX sclPipe_i0 (.D(scl_c), .CK(clk_c), .PD(rstPipe[1]), .Q(sclPipe[0]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(111[8] 130[4])
    defparam sclPipe_i0.GSR = "ENABLED";
    FD1S3AX sdaDeb_63 (.D(n3634), .CK(clk_c), .Q(sdaDeb));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(111[8] 130[4])
    defparam sdaDeb_63.GSR = "ENABLED";
    FD1S3JX sclDelayed_i0 (.D(sclDeb), .CK(clk_c), .PD(rstPipe[1]), .Q(sclDelayed[0]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(139[8] 148[4])
    defparam sclDelayed_i0.GSR = "ENABLED";
    FD1S3AX sclDeb_65 (.D(n3354), .CK(clk_c), .Q(sclDeb));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(111[8] 130[4])
    defparam sclDeb_65.GSR = "ENABLED";
    FD1S3JX sdaDelayed_i0 (.D(sdaDeb), .CK(clk_c), .PD(rstPipe[1]), .Q(sdaDelayed[0]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(139[8] 148[4])
    defparam sdaDelayed_i0.GSR = "ENABLED";
    FD1S3AX rstPipe_i0 (.D(rstPipe_1__N_28[0]), .CK(clk_c), .Q(rstPipe[0]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(101[8] 106[4])
    defparam rstPipe_i0.GSR = "ENABLED";
    OB myReg1_pad_3 (.I(myReg1_c_3), .O(myReg1[3]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(68[14:20])
    BB sda_pad (.I(GND_net), .T(sdaOut), .B(sda), .O(sda_out));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(97[8:11])
    OB myReg1_pad_4 (.I(myReg1_c_4), .O(myReg1[4]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(68[14:20])
    OB myReg1_pad_5 (.I(myReg1_c_5), .O(myReg1[5]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(68[14:20])
    OB myReg1_pad_6 (.I(myReg1_c_6), .O(myReg1[6]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(68[14:20])
    OB myReg1_pad_7 (.I(myReg1_c_7), .O(myReg1[7]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(68[14:20])
    OB myReg0_pad_3 (.I(myReg0_c_3), .O(myReg0[3]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(67[14:20])
    OB myReg0_pad_4 (.I(myReg0_c_4), .O(myReg0[4]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(67[14:20])
    OB myReg0_pad_5 (.I(myReg0_c_5), .O(myReg0[5]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(67[14:20])
    LUT4 i1920_3_lut_4_lut (.A(rstPipe[1]), .B(startEdgeDet), .C(n279), 
         .D(n286), .Z(n1325)) /* synthesis lut_function=(!(A+(B+!(C+(D))))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(191[8:35])
    defparam i1920_3_lut_4_lut.init = 16'h1110;
    LUT4 i2_3_lut_4_lut (.A(rstPipe[1]), .B(startEdgeDet), .C(n15_adj_407), 
         .D(n293), .Z(clk_c_enable_67)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(191[8:35])
    defparam i2_3_lut_4_lut.init = 16'hfffe;
    LUT4 i2780_2_lut (.A(clearStartStopDet), .B(rstPipe[1]), .Z(n3392)) /* synthesis lut_function=(A+(B)) */ ;
    defparam i2780_2_lut.init = 16'heeee;
    FD1S3JX sdaPipe_i3 (.D(sdaPipe[2]), .CK(clk_c), .PD(rstPipe[1]), .Q(sdaPipe[3]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(111[8] 130[4])
    defparam sdaPipe_i3.GSR = "ENABLED";
    OB myReg3_pad_5 (.I(myReg3_c_5), .O(myReg3[5]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(70[14:20])
    FD1S3JX sdaPipe_i2 (.D(sdaPipe[1]), .CK(clk_c), .PD(rstPipe[1]), .Q(sdaPipe[2]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(111[8] 130[4])
    defparam sdaPipe_i2.GSR = "ENABLED";
    LUT4 i1951_2_lut_3_lut (.A(rstPipe[1]), .B(startEdgeDet), .C(next_writeEn), 
         .Z(n1323)) /* synthesis lut_function=(!(A+(B+!(C)))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(191[8:35])
    defparam i1951_2_lut_3_lut.init = 16'h1010;
    OB myReg0_pad_0 (.I(myReg0_c_0), .O(myReg0[0]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(67[14:20])
    LUT4 i2910_2_lut_3_lut (.A(rstPipe[1]), .B(startEdgeDet), .C(n3493), 
         .Z(clk_c_enable_50)) /* synthesis lut_function=(A+(B+(C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(191[8:35])
    defparam i2910_2_lut_3_lut.init = 16'hfefe;
    LUT4 i1_2_lut_3_lut (.A(rstPipe[1]), .B(startEdgeDet), .C(n293), .Z(n4)) /* synthesis lut_function=(A+(B+(C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(191[8:35])
    defparam i1_2_lut_3_lut.init = 16'hfefe;
    LUT4 i1_3_lut (.A(sclDelayed[6]), .B(n280), .C(n281), .Z(n1459)) /* synthesis lut_function=(A (B+(C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(139[8] 148[4])
    defparam i1_3_lut.init = 16'ha8a8;
    LUT4 i5_3_lut (.A(sclPipe[4]), .B(n10_adj_410), .C(sclPipe[5]), .Z(sclDeb_N_42)) /* synthesis lut_function=(A+(B+(C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(123[14:40])
    defparam i5_3_lut.init = 16'hfefe;
    FD1P3IX startStopDetState__i0 (.D(n3642), .SP(clk_c_enable_65), .CD(n3392), 
            .CK(clk_c), .Q(startStopDetState[0]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(151[8] 170[4])
    defparam startStopDetState__i0.GSR = "ENABLED";
    LUT4 i1_3_lut_rep_43 (.A(n286), .B(n3420), .C(n3207), .Z(n3637)) /* synthesis lut_function=(!(A (B+!(C))+!A (B))) */ ;
    defparam i1_3_lut_rep_43.init = 16'h3131;
    OB myReg3_pad_6 (.I(myReg3_c_6), .O(myReg3[6]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(70[14:20])
    LUT4 i5_3_lut_adj_47 (.A(sclPipe[4]), .B(n10), .C(sclPipe[5]), .Z(sclDeb_N_43)) /* synthesis lut_function=(A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(121[9:35])
    defparam i5_3_lut_adj_47.init = 16'h8080;
    OB myReg3_pad_7 (.I(myReg3_c_7), .O(myReg3[7]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(70[14:20])
    LUT4 i2_3_lut_4_lut_adj_48 (.A(rstPipe[1]), .B(startEdgeDet), .C(n292), 
         .D(dataFromRegIF[0]), .Z(n3183)) /* synthesis lut_function=(!(A+(B+!(C (D))))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(191[8:35])
    defparam i2_3_lut_4_lut_adj_48.init = 16'h1000;
    OB myReg2_pad_0 (.I(myReg2_c_0), .O(myReg2[0]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(69[14:20])
    LUT4 i1_3_lut_adj_49 (.A(sclDelayed[6]), .B(n279), .C(n280), .Z(n1373)) /* synthesis lut_function=(A (B)+!A (B+(C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(108[26:40])
    defparam i1_3_lut_adj_49.init = 16'hdcdc;
    LUT4 i1_2_lut_3_lut_then_4_lut (.A(rstPipe[1]), .B(startEdgeDet), .C(n3650), 
         .D(n3647), .Z(n3655)) /* synthesis lut_function=(A+(B+(C (D)))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(191[8:35])
    defparam i1_2_lut_3_lut_then_4_lut.init = 16'hfeee;
    LUT4 i1_2_lut_3_lut_else_4_lut (.A(rstPipe[1]), .B(startEdgeDet), .C(n293), 
         .D(n279), .Z(n3654)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(191[8:35])
    defparam i1_2_lut_3_lut_else_4_lut.init = 16'hfffe;
    LUT4 i2905_3_lut_4_lut (.A(rstPipe[1]), .B(startEdgeDet), .C(n16), 
         .D(n279), .Z(clk_c_enable_57)) /* synthesis lut_function=(A+(B+((D)+!C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(191[8:35])
    defparam i2905_3_lut_4_lut.init = 16'hffef;
    LUT4 i2543_2_lut_3_lut_3_lut_4_lut (.A(rstPipe[1]), .B(startEdgeDet), 
         .C(n3637), .D(n6), .Z(n20)) /* synthesis lut_function=(A (D)+!A (B (D)+!B (C (D)+!C !(D)))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(191[8:35])
    defparam i2543_2_lut_3_lut_3_lut_4_lut.init = 16'hfe01;
    OB myReg2_pad_1 (.I(myReg2_c_1), .O(myReg2[1]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(69[14:20])
    LUT4 n3185_bdd_4_lut (.A(n3185), .B(n3208), .C(sdaDeb), .D(rstPipe[1]), 
         .Z(n3634)) /* synthesis lut_function=(A (B+((D)+!C))+!A (B (C+(D))+!B (D))) */ ;
    defparam n3185_bdd_4_lut.init = 16'hffca;
    LUT4 i7_4_lut (.A(sclDelayed[6]), .B(n3650), .C(next_writeEn), .D(n36), 
         .Z(n15_adj_407)) /* synthesis lut_function=(A (B (C))+!A (B (C+(D))+!B !(C+!(D)))) */ ;
    defparam i7_4_lut.init = 16'hc5c0;
    LUT4 i683_2_lut_3_lut (.A(rstPipe[1]), .B(startEdgeDet), .C(n15), 
         .Z(clk_c_enable_13)) /* synthesis lut_function=(A+(B+!(C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(191[8:35])
    defparam i683_2_lut_3_lut.init = 16'hefef;
    FD1S3JX sdaPipe_i4 (.D(sdaPipe[3]), .CK(clk_c), .PD(rstPipe[1]), .Q(sdaPipe[4]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(111[8] 130[4])
    defparam sdaPipe_i4.GSR = "ENABLED";
    LUT4 i1_4_lut (.A(n15_adj_408), .B(n291), .C(n13), .D(n14), .Z(n36)) /* synthesis lut_function=(A (B)+!A (B+!(C+(D)))) */ ;
    defparam i1_4_lut.init = 16'hcccd;
    FD1S3IX startEdgeDet_69 (.D(startEdgeDet_N_45), .CK(clk_c), .CD(rstPipe[1]), 
            .Q(startEdgeDet));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(151[8] 170[4])
    defparam startEdgeDet_69.GSR = "ENABLED";
    FD1S3JX sdaPipe_i1 (.D(sdaPipe[0]), .CK(clk_c), .PD(rstPipe[1]), .Q(sdaPipe[1]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(111[8] 130[4])
    defparam sdaPipe_i1.GSR = "ENABLED";
    OB myReg0_pad_6 (.I(myReg0_c_6), .O(myReg0[6]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(67[14:20])
    OB myReg2_pad_5 (.I(myReg2_c_5), .O(myReg2[5]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(69[14:20])
    OB myReg2_pad_6 (.I(myReg2_c_6), .O(myReg2[6]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(69[14:20])
    LUT4 i6_4_lut (.A(n290), .B(n282), .C(n287), .D(n286), .Z(n15_adj_408)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;
    defparam i6_4_lut.init = 16'hfffe;
    LUT4 i4_2_lut (.A(n279), .B(n280), .Z(n13)) /* synthesis lut_function=(A+(B)) */ ;
    defparam i4_2_lut.init = 16'heeee;
    OB myReg2_pad_2 (.I(myReg2_c_2), .O(myReg2[2]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(69[14:20])
    FD1S3JX sdaPipe_i6 (.D(sdaPipe[5]), .CK(clk_c), .PD(rstPipe[1]), .Q(sdaPipe[6]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(111[8] 130[4])
    defparam sdaPipe_i6.GSR = "ENABLED";
    OB myReg2_pad_7 (.I(myReg2_c_7), .O(myReg2[7]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(69[14:20])
    OB myReg0_pad_1 (.I(myReg0_c_1), .O(myReg0[1]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(67[14:20])
    OB myReg3_pad_4 (.I(myReg3_c_4), .O(myReg3[4]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(70[14:20])
    OB myReg1_pad_0 (.I(myReg1_c_0), .O(myReg1[0]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(68[14:20])
    OB myReg2_pad_3 (.I(myReg2_c_3), .O(myReg2[3]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(69[14:20])
    OB myReg0_pad_7 (.I(myReg0_c_7), .O(myReg0[7]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(67[14:20])
    FD1S3JX sdaPipe_i5 (.D(sdaPipe[4]), .CK(clk_c), .PD(rstPipe[1]), .Q(sdaPipe[5]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(111[8] 130[4])
    defparam sdaPipe_i5.GSR = "ENABLED";
    FD1S3JX sclPipe_i1 (.D(sclPipe[0]), .CK(clk_c), .PD(rstPipe[1]), .Q(sclPipe[1]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(111[8] 130[4])
    defparam sclPipe_i1.GSR = "ENABLED";
    OB myReg0_pad_2 (.I(myReg0_c_2), .O(myReg0[2]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(67[14:20])
    OB myReg1_pad_1 (.I(myReg1_c_1), .O(myReg1[1]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(68[14:20])
    OB myReg1_pad_2 (.I(myReg1_c_2), .O(myReg1[2]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(68[14:20])
    OB myReg2_pad_4 (.I(myReg2_c_4), .O(myReg2[4]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(69[14:20])
    OB myReg3_pad_3 (.I(myReg3_c_3), .O(myReg3[3]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(70[14:20])
    OB myReg3_pad_2 (.I(myReg3_c_2), .O(myReg3[2]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(70[14:20])
    OB myReg3_pad_1 (.I(myReg3_c_1), .O(myReg3[1]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(70[14:20])
    OB myReg3_pad_0 (.I(myReg3_c_0), .O(myReg3[0]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(70[14:20])
    IB clk_pad (.I(clk), .O(clk_c));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(63[7:10])
    IB rst_pad (.I(rst), .O(rstPipe_1__N_28[0]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(64[7:10])
    IB scl_pad (.I(scl), .O(scl_c));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(66[7:10])
    IB myReg4_pad_7 (.I(myReg4[7]), .O(myReg4_c_7));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(71[13:19])
    IB myReg4_pad_6 (.I(myReg4[6]), .O(myReg4_c_6));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(71[13:19])
    IB myReg4_pad_5 (.I(myReg4[5]), .O(myReg4_c_5));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(71[13:19])
    IB myReg4_pad_4 (.I(myReg4[4]), .O(myReg4_c_4));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(71[13:19])
    IB myReg4_pad_3 (.I(myReg4[3]), .O(myReg4_c_3));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(71[13:19])
    IB myReg4_pad_2 (.I(myReg4[2]), .O(myReg4_c_2));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(71[13:19])
    IB myReg4_pad_1 (.I(myReg4[1]), .O(myReg4_c_1));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(71[13:19])
    IB myReg4_pad_0 (.I(myReg4[0]), .O(myReg4_c_0));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(71[13:19])
    IB myReg5_pad_7 (.I(myReg5[7]), .O(myReg5_c_7));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(72[13:19])
    IB myReg5_pad_6 (.I(myReg5[6]), .O(myReg5_c_6));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(72[13:19])
    IB myReg5_pad_5 (.I(myReg5[5]), .O(myReg5_c_5));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(72[13:19])
    IB myReg5_pad_4 (.I(myReg5[4]), .O(myReg5_c_4));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(72[13:19])
    IB myReg5_pad_3 (.I(myReg5[3]), .O(myReg5_c_3));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(72[13:19])
    IB myReg5_pad_2 (.I(myReg5[2]), .O(myReg5_c_2));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(72[13:19])
    IB myReg5_pad_1 (.I(myReg5[1]), .O(myReg5_c_1));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(72[13:19])
    IB myReg5_pad_0 (.I(myReg5[0]), .O(myReg5_c_0));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(72[13:19])
    IB myReg6_pad_7 (.I(myReg6[7]), .O(myReg6_c_7));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(73[13:19])
    IB myReg6_pad_6 (.I(myReg6[6]), .O(myReg6_c_6));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(73[13:19])
    IB myReg6_pad_5 (.I(myReg6[5]), .O(myReg6_c_5));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(73[13:19])
    IB myReg6_pad_4 (.I(myReg6[4]), .O(myReg6_c_4));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(73[13:19])
    IB myReg6_pad_3 (.I(myReg6[3]), .O(myReg6_c_3));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(73[13:19])
    IB myReg6_pad_2 (.I(myReg6[2]), .O(myReg6_c_2));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(73[13:19])
    IB myReg6_pad_1 (.I(myReg6[1]), .O(myReg6_c_1));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(73[13:19])
    IB myReg6_pad_0 (.I(myReg6[0]), .O(myReg6_c_0));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(73[13:19])
    IB myReg7_pad_7 (.I(myReg7[7]), .O(myReg7_c_7));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(74[13:19])
    IB myReg7_pad_6 (.I(myReg7[6]), .O(myReg7_c_6));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(74[13:19])
    IB myReg7_pad_5 (.I(myReg7[5]), .O(myReg7_c_5));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(74[13:19])
    IB myReg7_pad_4 (.I(myReg7[4]), .O(myReg7_c_4));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(74[13:19])
    IB myReg7_pad_3 (.I(myReg7[3]), .O(myReg7_c_3));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(74[13:19])
    IB myReg7_pad_2 (.I(myReg7[2]), .O(myReg7_c_2));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(74[13:19])
    IB myReg7_pad_1 (.I(myReg7[1]), .O(myReg7_c_1));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(74[13:19])
    IB myReg7_pad_0 (.I(myReg7[0]), .O(myReg7_c_0));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(74[13:19])
    FD1S3JX sclPipe_i2 (.D(sclPipe[1]), .CK(clk_c), .PD(rstPipe[1]), .Q(sclPipe[2]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(111[8] 130[4])
    defparam sclPipe_i2.GSR = "ENABLED";
    FD1S3JX sclPipe_i3 (.D(sclPipe[2]), .CK(clk_c), .PD(rstPipe[1]), .Q(sclPipe[3]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(111[8] 130[4])
    defparam sclPipe_i3.GSR = "ENABLED";
    FD1S3JX sclPipe_i4 (.D(sclPipe[3]), .CK(clk_c), .PD(rstPipe[1]), .Q(sclPipe[4]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(111[8] 130[4])
    defparam sclPipe_i4.GSR = "ENABLED";
    FD1S3JX sclPipe_i5 (.D(sclPipe[4]), .CK(clk_c), .PD(rstPipe[1]), .Q(sclPipe[5]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(111[8] 130[4])
    defparam sclPipe_i5.GSR = "ENABLED";
    FD1S3JX sclPipe_i6 (.D(sclPipe[5]), .CK(clk_c), .PD(rstPipe[1]), .Q(sclPipe[6]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(111[8] 130[4])
    defparam sclPipe_i6.GSR = "ENABLED";
    FD1S3JX sclDelayed_i1 (.D(sclDelayed[0]), .CK(clk_c), .PD(rstPipe[1]), 
            .Q(sclDelayed[1]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(139[8] 148[4])
    defparam sclDelayed_i1.GSR = "ENABLED";
    FD1S3JX sclDelayed_i2 (.D(sclDelayed[1]), .CK(clk_c), .PD(rstPipe[1]), 
            .Q(sclDelayed[2]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(139[8] 148[4])
    defparam sclDelayed_i2.GSR = "ENABLED";
    FD1S3JX sclDelayed_i3 (.D(sclDelayed[2]), .CK(clk_c), .PD(rstPipe[1]), 
            .Q(sclDelayed[3]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(139[8] 148[4])
    defparam sclDelayed_i3.GSR = "ENABLED";
    FD1S3JX sclDelayed_i4 (.D(sclDelayed[3]), .CK(clk_c), .PD(rstPipe[1]), 
            .Q(sclDelayed[4]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(139[8] 148[4])
    defparam sclDelayed_i4.GSR = "ENABLED";
    FD1S3JX sclDelayed_i5 (.D(sclDelayed[4]), .CK(clk_c), .PD(rstPipe[1]), 
            .Q(sclDelayed[5]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(139[8] 148[4])
    defparam sclDelayed_i5.GSR = "ENABLED";
    FD1S3JX sclDelayed_i6 (.D(sclDelayed[5]), .CK(clk_c), .PD(rstPipe[1]), 
            .Q(sclDelayed[6]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(139[8] 148[4])
    defparam sclDelayed_i6.GSR = "ENABLED";
    FD1S3JX sdaDelayed_i1 (.D(sdaDelayed[0]), .CK(clk_c), .PD(rstPipe[1]), 
            .Q(sdaDelayed[1]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(139[8] 148[4])
    defparam sdaDelayed_i1.GSR = "ENABLED";
    FD1S3JX sdaDelayed_i2 (.D(sdaDelayed[1]), .CK(clk_c), .PD(rstPipe[1]), 
            .Q(sdaDelayed[2]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(139[8] 148[4])
    defparam sdaDelayed_i2.GSR = "ENABLED";
    FD1S3JX rstPipe_i1 (.D(rstPipe_1__N_28[0]), .CK(clk_c), .PD(rstPipe[0]), 
            .Q(rstPipe[1]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(101[8] 106[4])
    defparam rstPipe_i1.GSR = "ENABLED";
    LUT4 i5_4_lut (.A(sdaPipe[3]), .B(n7), .C(sdaPipe[4]), .D(n8), .Z(n3208)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(111[8] 130[4])
    defparam i5_4_lut.init = 16'hfffe;
    LUT4 i4_4_lut (.A(sclPipe[3]), .B(sclPipe[6]), .C(sclPipe[1]), .D(sclPipe[2]), 
         .Z(n10_adj_410)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(123[14:40])
    defparam i4_4_lut.init = 16'hfffe;
    FD1P3IX startStopDetState__i1 (.D(n98), .SP(clk_c_enable_65), .CD(n3392), 
            .CK(clk_c), .Q(startStopDetState[1]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(151[8] 170[4])
    defparam startStopDetState__i1.GSR = "ENABLED";
    LUT4 i5_4_lut_adj_50 (.A(sdaPipe[3]), .B(n7_adj_413), .C(sdaPipe[4]), 
         .D(n8_adj_412), .Z(n3185)) /* synthesis lut_function=(A (B (C (D)))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(111[8] 130[4])
    defparam i5_4_lut_adj_50.init = 16'h8000;
    LUT4 i1_2_lut (.A(sdaPipe[5]), .B(sdaPipe[2]), .Z(n7_adj_413)) /* synthesis lut_function=(A (B)) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(111[8] 130[4])
    defparam i1_2_lut.init = 16'h8888;
    LUT4 i2_2_lut (.A(sdaPipe[6]), .B(sdaPipe[1]), .Z(n8_adj_412)) /* synthesis lut_function=(A (B)) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(111[8] 130[4])
    defparam i2_2_lut.init = 16'h8888;
    LUT4 i1_2_lut_adj_51 (.A(sdaPipe[5]), .B(sdaPipe[2]), .Z(n7)) /* synthesis lut_function=(A+(B)) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(111[8] 130[4])
    defparam i1_2_lut_adj_51.init = 16'heeee;
    LUT4 i2_2_lut_adj_52 (.A(sdaPipe[6]), .B(sdaPipe[1]), .Z(n8)) /* synthesis lut_function=(A+(B)) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(111[8] 130[4])
    defparam i2_2_lut_adj_52.init = 16'heeee;
    LUT4 i1_2_lut_rep_48 (.A(sdaDelayed[2]), .B(sdaDelayed[1]), .Z(n3642)) /* synthesis lut_function=(!((B)+!A)) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(166[12] 167[41])
    defparam i1_2_lut_rep_48.init = 16'h2222;
    LUT4 i2_3_lut (.A(startStopDetState[0]), .B(sclDelayed[6]), .C(startStopDetState[1]), 
         .Z(n3207)) /* synthesis lut_function=(A+(B+(C))) */ ;
    defparam i2_3_lut.init = 16'hfefe;
    LUT4 i22_4_lut (.A(next_writeEn), .B(n3645), .C(n289), .D(n3650), 
         .Z(n3420)) /* synthesis lut_function=(A (C+!(D))+!A (B (C))) */ ;
    defparam i22_4_lut.init = 16'he0ea;
    GSR GSR_INST (.GSR(VCC_net));
    LUT4 i2784_3_lut (.A(n293), .B(n291), .C(sclDelayed[6]), .Z(n3396)) /* synthesis lut_function=(A+!((C)+!B)) */ ;
    defparam i2784_3_lut.init = 16'haeae;
    LUT4 i685_4_lut (.A(n56), .B(n3648), .C(n3213), .D(n4_adj_409), 
         .Z(clk_c_enable_64)) /* synthesis lut_function=(A (B+(C (D)))+!A (B)) */ ;
    defparam i685_4_lut.init = 16'heccc;
    LUT4 i1_2_lut_3_lut_adj_53 (.A(sdaDelayed[2]), .B(sdaDelayed[1]), .C(sclDeb), 
         .Z(startEdgeDet_N_45)) /* synthesis lut_function=(!((B+!(C))+!A)) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(166[12] 167[41])
    defparam i1_2_lut_3_lut_adj_53.init = 16'h2020;
    PUR PUR_INST (.PUR(VCC_net));
    defparam PUR_INST.RST_PULSE = 1;
    LUT4 i1_4_lut_adj_54 (.A(rstPipe[1]), .B(sclDeb_N_42), .C(sclDeb_N_43), 
         .D(sclDeb), .Z(n3354)) /* synthesis lut_function=(A+(B (C+(D))+!B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(111[8] 130[4])
    defparam i1_4_lut_adj_54.init = 16'hfefa;
    LUT4 i48_2_lut (.A(sdaDelayed[1]), .B(sdaDelayed[2]), .Z(n98)) /* synthesis lut_function=(!((B)+!A)) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(164[11:83])
    defparam i48_2_lut.init = 16'h2222;
    LUT4 i4_4_lut_adj_55 (.A(sclPipe[3]), .B(sclPipe[6]), .C(sclPipe[1]), 
         .D(sclPipe[2]), .Z(n10)) /* synthesis lut_function=(A (B (C (D)))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(121[9:35])
    defparam i4_4_lut_adj_55.init = 16'h8000;
    LUT4 i1_3_lut_adj_56 (.A(sclDelayed[6]), .B(n287), .C(n288), .Z(n1465)) /* synthesis lut_function=(!(A+!(B+(C)))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(205[11:24])
    defparam i1_3_lut_adj_56.init = 16'h5454;
    LUT4 i1_2_lut_2_lut (.A(n293), .B(txData[5]), .Z(n3)) /* synthesis lut_function=(!(A+!(B))) */ ;
    defparam i1_2_lut_2_lut.init = 16'h4444;
    LUT4 i2901_4_lut (.A(sclDeb), .B(n3392), .C(sdaDelayed[1]), .D(sdaDelayed[2]), 
         .Z(clk_c_enable_65)) /* synthesis lut_function=(A (B+!(C (D)+!C !(D)))+!A (B)) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(156[8] 169[6])
    defparam i2901_4_lut.init = 16'hceec;
    LUT4 i1_2_lut_adj_57 (.A(sclDelayed[6]), .B(sdaDeb), .Z(n7_adj_411)) /* synthesis lut_function=(A (B)) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(83[24:34])
    defparam i1_2_lut_adj_57.init = 16'h8888;
    LUT4 i1_2_lut_2_lut_adj_58 (.A(n293), .B(txData[6]), .Z(n1)) /* synthesis lut_function=(!(A+!(B))) */ ;
    defparam i1_2_lut_2_lut_adj_58.init = 16'h4444;
    LUT4 i1_2_lut_adj_59 (.A(sclDelayed[6]), .B(n283), .Z(n338)) /* synthesis lut_function=(!(A+!(B))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(205[11:24])
    defparam i1_2_lut_adj_59.init = 16'h4444;
    PFUMX i2979 (.BLUT(n3654), .ALUT(n3655), .C0(next_writeEn), .Z(clk_c_enable_9));
    LUT4 rstPipe_1__I_0_2_lut_rep_54 (.A(rstPipe[1]), .B(startEdgeDet), 
         .Z(n3648)) /* synthesis lut_function=(A+(B)) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(191[8:35])
    defparam rstPipe_1__I_0_2_lut_rep_54.init = 16'heeee;
    TSALL TSALL_INST (.TSALL(GND_net));
    serialInterface regAddr_7__I_0 (.regAddr({regAddr}), .clk_c(clk_c), 
            .\sclDelayed[6] (sclDelayed[6]), .n282(n282), .clk_c_enable_50(clk_c_enable_50), 
            .n20(n20), .n286(n286), .clk_c_enable_64(clk_c_enable_64), 
            .n3648(n3648), .n293(n293), .n291(n291), .GND_net(GND_net), 
            .writeEn(writeEn), .clk_c_enable_9(clk_c_enable_9), .n1323(n1323), 
            .n3647(n3647), .dataToRegIF({dataToRegIF}), .n279(n279), .n292(n292), 
            .n3650(n3650), .next_writeEn(next_writeEn), .clk_c_enable_13(clk_c_enable_13), 
            .n6(n6), .n3396(n3396), .n3645(n3645), .n289(n289), .\dataFromRegIF[5] (dataFromRegIF[5]), 
            .n288(n288), .\dataFromRegIF[4] (dataFromRegIF[4]), .n3637(n3637), 
            .n56(n56), .\dataFromRegIF[3] (dataFromRegIF[3]), .\txData[6] (txData[6]), 
            .\txData[5] (txData[5]), .clearStartStopDet(clearStartStopDet), 
            .clk_c_enable_57(clk_c_enable_57), .n1325(n1325), .\dataFromRegIF[6] (dataFromRegIF[6]), 
            .n290(n290), .n287(n287), .n1465(n1465), .n338(n338), .n283(n283), 
            .n281(n281), .n1459(n1459), .n280(n280), .n1373(n1373), 
            .n3493(n3493), .n3207(n3207), .sdaOut(sdaOut), .clk_c_enable_67(clk_c_enable_67), 
            .n4(n4), .n3183(n3183), .\dataFromRegIF[2] (dataFromRegIF[2]), 
            .\dataFromRegIF[7] (dataFromRegIF[7]), .\dataFromRegIF[1] (dataFromRegIF[1]), 
            .n15(n15), .startStopDetState({startStopDetState}), .n16(n16), 
            .n3213(n3213), .n4_adj_1(n4_adj_409), .n3420(n3420), .sdaDeb(sdaDeb), 
            .n7(n7_adj_411), .n14(n14), .n1(n1), .n3(n3)) /* synthesis syn_module_defined=1 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(189[17] 201[2])
    VLO i1 (.Z(GND_net));
    
endmodule
//
// Verilog Description of module registerInterface
//

module registerInterface (myReg1_c_0, clk_c, dataToRegIF, myReg2_c_0, 
            myReg3_c_0, myReg0_c_0, regAddr, dataFromRegIF, myReg2_c_4, 
            myReg3_c_4, myReg0_c_4, myReg1_c_4, myReg6_c_6, myReg7_c_6, 
            myReg4_c_6, myReg5_c_6, myReg2_c_6, myReg3_c_6, myReg0_c_6, 
            myReg1_c_6, writeEn, myReg6_c_7, myReg7_c_7, myReg1_c_1, 
            myReg1_c_2, myReg1_c_3, myReg1_c_5, myReg1_c_7, myReg2_c_1, 
            myReg2_c_2, myReg2_c_3, myReg2_c_5, myReg2_c_7, myReg3_c_1, 
            myReg3_c_2, myReg3_c_3, myReg3_c_5, myReg3_c_7, myReg0_c_1, 
            myReg0_c_2, myReg0_c_3, myReg0_c_5, myReg0_c_7, myReg4_c_7, 
            myReg5_c_7, myReg6_c_3, myReg7_c_3, myReg6_c_1, myReg7_c_1, 
            myReg4_c_1, myReg5_c_1, myReg6_c_5, myReg7_c_5, myReg4_c_5, 
            myReg5_c_5, myReg4_c_3, myReg5_c_3, myReg6_c_2, myReg7_c_2, 
            myReg4_c_2, myReg5_c_2, myReg6_c_0, myReg7_c_0, myReg4_c_0, 
            myReg5_c_0, myReg6_c_4, myReg7_c_4, myReg4_c_4, myReg5_c_4) /* synthesis syn_module_defined=1 */ ;
    output myReg1_c_0;
    input clk_c;
    input [7:0]dataToRegIF;
    output myReg2_c_0;
    output myReg3_c_0;
    output myReg0_c_0;
    input [7:0]regAddr;
    output [7:0]dataFromRegIF;
    output myReg2_c_4;
    output myReg3_c_4;
    output myReg0_c_4;
    output myReg1_c_4;
    input myReg6_c_6;
    input myReg7_c_6;
    input myReg4_c_6;
    input myReg5_c_6;
    output myReg2_c_6;
    output myReg3_c_6;
    output myReg0_c_6;
    output myReg1_c_6;
    input writeEn;
    input myReg6_c_7;
    input myReg7_c_7;
    output myReg1_c_1;
    output myReg1_c_2;
    output myReg1_c_3;
    output myReg1_c_5;
    output myReg1_c_7;
    output myReg2_c_1;
    output myReg2_c_2;
    output myReg2_c_3;
    output myReg2_c_5;
    output myReg2_c_7;
    output myReg3_c_1;
    output myReg3_c_2;
    output myReg3_c_3;
    output myReg3_c_5;
    output myReg3_c_7;
    output myReg0_c_1;
    output myReg0_c_2;
    output myReg0_c_3;
    output myReg0_c_5;
    output myReg0_c_7;
    input myReg4_c_7;
    input myReg5_c_7;
    input myReg6_c_3;
    input myReg7_c_3;
    input myReg6_c_1;
    input myReg7_c_1;
    input myReg4_c_1;
    input myReg5_c_1;
    input myReg6_c_5;
    input myReg7_c_5;
    input myReg4_c_5;
    input myReg5_c_5;
    input myReg4_c_3;
    input myReg5_c_3;
    input myReg6_c_2;
    input myReg7_c_2;
    input myReg4_c_2;
    input myReg5_c_2;
    input myReg6_c_0;
    input myReg7_c_0;
    input myReg4_c_0;
    input myReg5_c_0;
    input myReg6_c_4;
    input myReg7_c_4;
    input myReg4_c_4;
    input myReg5_c_4;
    
    wire clk_c /* synthesis SET_AS_NETWORK=clk_c, is_clock=1 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(63[7:10])
    
    wire clk_c_enable_20, clk_c_enable_27, clk_c_enable_34, clk_c_enable_41, 
        n3437, n3438, n3439, n3444, n3445, n3446, n3451, n3452, 
        n3453, n3426, n3427, n3430, n1561, n3428, n3429, n3431, 
        n9, n3432, n3454, n3455, n3458, n3456, n3457, n3459, 
        n3441, n3461, n3462, n3465, n3440, n3449, n3450, n3478, 
        n3477, n3463, n3464, n3466, n3476, n3475, n3418, n3468, 
        n3469, n3472, n3471, n3470, n3473, n3467, n3460, n3481, 
        n3474, n3479, n3480, n3442, n3443, n3447, n3448, n3433, 
        n3434, n3435, n3436, n6;
    
    FD1P3AX myReg1_i0_i1 (.D(dataToRegIF[0]), .SP(clk_c_enable_20), .CK(clk_c), 
            .Q(myReg1_c_0)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg1_i0_i1.GSR = "ENABLED";
    FD1P3AX myReg2_i0_i1 (.D(dataToRegIF[0]), .SP(clk_c_enable_27), .CK(clk_c), 
            .Q(myReg2_c_0)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg2_i0_i1.GSR = "ENABLED";
    FD1P3AX myReg3_i0_i1 (.D(dataToRegIF[0]), .SP(clk_c_enable_34), .CK(clk_c), 
            .Q(myReg3_c_0)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg3_i0_i1.GSR = "ENABLED";
    FD1P3AX myReg0_i0_i1 (.D(dataToRegIF[0]), .SP(clk_c_enable_41), .CK(clk_c), 
            .Q(myReg0_c_0)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg0_i0_i1.GSR = "ENABLED";
    L6MUX21 i2825 (.D0(n3437), .D1(n3438), .SD(regAddr[2]), .Z(n3439));
    L6MUX21 i2832 (.D0(n3444), .D1(n3445), .SD(regAddr[2]), .Z(n3446));
    L6MUX21 i2839 (.D0(n3451), .D1(n3452), .SD(regAddr[2]), .Z(n3453));
    PFUMX i2816 (.BLUT(n3426), .ALUT(n3427), .C0(regAddr[1]), .Z(n3430));
    LUT4 i1_2_lut_3_lut (.A(n1561), .B(regAddr[1]), .C(regAddr[0]), .Z(clk_c_enable_20)) /* synthesis lut_function=(!((B+!(C))+!A)) */ ;
    defparam i1_2_lut_3_lut.init = 16'h2020;
    PFUMX i2817 (.BLUT(n3428), .ALUT(n3429), .C0(regAddr[1]), .Z(n3431));
    FD1S3IX dataOut__i0 (.D(n3432), .CK(clk_c), .CD(n9), .Q(dataFromRegIF[0])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(87[8] 99[4])
    defparam dataOut__i0.GSR = "ENABLED";
    LUT4 i1_2_lut_3_lut_adj_44 (.A(n1561), .B(regAddr[1]), .C(regAddr[0]), 
         .Z(clk_c_enable_41)) /* synthesis lut_function=(!((B+(C))+!A)) */ ;
    defparam i1_2_lut_3_lut_adj_44.init = 16'h0202;
    PFUMX i2844 (.BLUT(n3454), .ALUT(n3455), .C0(regAddr[1]), .Z(n3458));
    PFUMX i2845 (.BLUT(n3456), .ALUT(n3457), .C0(regAddr[1]), .Z(n3459));
    LUT4 i2827_3_lut (.A(myReg2_c_4), .B(myReg3_c_4), .C(regAddr[0]), 
         .Z(n3441)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2827_3_lut.init = 16'hcaca;
    PFUMX i2851 (.BLUT(n3461), .ALUT(n3462), .C0(regAddr[1]), .Z(n3465));
    LUT4 i2826_3_lut (.A(myReg0_c_4), .B(myReg1_c_4), .C(regAddr[0]), 
         .Z(n3440)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2826_3_lut.init = 16'hcaca;
    PFUMX i2838 (.BLUT(n3449), .ALUT(n3450), .C0(regAddr[1]), .Z(n3452));
    LUT4 i2864_3_lut (.A(myReg6_c_6), .B(myReg7_c_6), .C(regAddr[0]), 
         .Z(n3478)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2864_3_lut.init = 16'hcaca;
    LUT4 i2863_3_lut (.A(myReg4_c_6), .B(myReg5_c_6), .C(regAddr[0]), 
         .Z(n3477)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2863_3_lut.init = 16'hcaca;
    PFUMX i2852 (.BLUT(n3463), .ALUT(n3464), .C0(regAddr[1]), .Z(n3466));
    LUT4 i2862_3_lut (.A(myReg2_c_6), .B(myReg3_c_6), .C(regAddr[0]), 
         .Z(n3476)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2862_3_lut.init = 16'hcaca;
    LUT4 i2861_3_lut (.A(myReg0_c_6), .B(myReg1_c_6), .C(regAddr[0]), 
         .Z(n3475)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2861_3_lut.init = 16'hcaca;
    LUT4 i1_4_lut (.A(writeEn), .B(regAddr[4]), .C(n3418), .D(regAddr[3]), 
         .Z(n1561)) /* synthesis lut_function=(!((B+(C+(D)))+!A)) */ ;
    defparam i1_4_lut.init = 16'h0002;
    PFUMX i2858 (.BLUT(n3468), .ALUT(n3469), .C0(regAddr[1]), .Z(n3472));
    LUT4 i2857_3_lut (.A(myReg6_c_7), .B(myReg7_c_7), .C(regAddr[0]), 
         .Z(n3471)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2857_3_lut.init = 16'hcaca;
    LUT4 i1_2_lut_3_lut_adj_45 (.A(regAddr[1]), .B(n1561), .C(regAddr[0]), 
         .Z(clk_c_enable_27)) /* synthesis lut_function=(!(((C)+!B)+!A)) */ ;
    defparam i1_2_lut_3_lut_adj_45.init = 16'h0808;
    PFUMX i2859 (.BLUT(n3470), .ALUT(n3471), .C0(regAddr[1]), .Z(n3473));
    FD1P3AX myReg1_i0_i2 (.D(dataToRegIF[1]), .SP(clk_c_enable_20), .CK(clk_c), 
            .Q(myReg1_c_1)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg1_i0_i2.GSR = "ENABLED";
    FD1P3AX myReg1_i0_i3 (.D(dataToRegIF[2]), .SP(clk_c_enable_20), .CK(clk_c), 
            .Q(myReg1_c_2)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg1_i0_i3.GSR = "ENABLED";
    FD1P3AX myReg1_i0_i4 (.D(dataToRegIF[3]), .SP(clk_c_enable_20), .CK(clk_c), 
            .Q(myReg1_c_3)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg1_i0_i4.GSR = "ENABLED";
    FD1P3AX myReg1_i0_i5 (.D(dataToRegIF[4]), .SP(clk_c_enable_20), .CK(clk_c), 
            .Q(myReg1_c_4)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg1_i0_i5.GSR = "ENABLED";
    FD1P3AX myReg1_i0_i6 (.D(dataToRegIF[5]), .SP(clk_c_enable_20), .CK(clk_c), 
            .Q(myReg1_c_5)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg1_i0_i6.GSR = "ENABLED";
    FD1P3AX myReg1_i0_i7 (.D(dataToRegIF[6]), .SP(clk_c_enable_20), .CK(clk_c), 
            .Q(myReg1_c_6)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg1_i0_i7.GSR = "ENABLED";
    FD1P3AX myReg1_i0_i8 (.D(dataToRegIF[7]), .SP(clk_c_enable_20), .CK(clk_c), 
            .Q(myReg1_c_7)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg1_i0_i8.GSR = "ENABLED";
    FD1P3AX myReg2_i0_i2 (.D(dataToRegIF[1]), .SP(clk_c_enable_27), .CK(clk_c), 
            .Q(myReg2_c_1)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg2_i0_i2.GSR = "ENABLED";
    FD1P3AX myReg2_i0_i3 (.D(dataToRegIF[2]), .SP(clk_c_enable_27), .CK(clk_c), 
            .Q(myReg2_c_2)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg2_i0_i3.GSR = "ENABLED";
    FD1P3AX myReg2_i0_i4 (.D(dataToRegIF[3]), .SP(clk_c_enable_27), .CK(clk_c), 
            .Q(myReg2_c_3)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg2_i0_i4.GSR = "ENABLED";
    FD1P3AX myReg2_i0_i5 (.D(dataToRegIF[4]), .SP(clk_c_enable_27), .CK(clk_c), 
            .Q(myReg2_c_4)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg2_i0_i5.GSR = "ENABLED";
    FD1P3AX myReg2_i0_i6 (.D(dataToRegIF[5]), .SP(clk_c_enable_27), .CK(clk_c), 
            .Q(myReg2_c_5)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg2_i0_i6.GSR = "ENABLED";
    FD1P3AX myReg2_i0_i7 (.D(dataToRegIF[6]), .SP(clk_c_enable_27), .CK(clk_c), 
            .Q(myReg2_c_6)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg2_i0_i7.GSR = "ENABLED";
    FD1P3AX myReg2_i0_i8 (.D(dataToRegIF[7]), .SP(clk_c_enable_27), .CK(clk_c), 
            .Q(myReg2_c_7)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg2_i0_i8.GSR = "ENABLED";
    FD1P3AX myReg3_i0_i2 (.D(dataToRegIF[1]), .SP(clk_c_enable_34), .CK(clk_c), 
            .Q(myReg3_c_1)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg3_i0_i2.GSR = "ENABLED";
    FD1P3AX myReg3_i0_i3 (.D(dataToRegIF[2]), .SP(clk_c_enable_34), .CK(clk_c), 
            .Q(myReg3_c_2)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg3_i0_i3.GSR = "ENABLED";
    FD1P3AX myReg3_i0_i4 (.D(dataToRegIF[3]), .SP(clk_c_enable_34), .CK(clk_c), 
            .Q(myReg3_c_3)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg3_i0_i4.GSR = "ENABLED";
    FD1P3AX myReg3_i0_i5 (.D(dataToRegIF[4]), .SP(clk_c_enable_34), .CK(clk_c), 
            .Q(myReg3_c_4)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg3_i0_i5.GSR = "ENABLED";
    FD1P3AX myReg3_i0_i6 (.D(dataToRegIF[5]), .SP(clk_c_enable_34), .CK(clk_c), 
            .Q(myReg3_c_5)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg3_i0_i6.GSR = "ENABLED";
    FD1P3AX myReg3_i0_i7 (.D(dataToRegIF[6]), .SP(clk_c_enable_34), .CK(clk_c), 
            .Q(myReg3_c_6)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg3_i0_i7.GSR = "ENABLED";
    FD1P3AX myReg3_i0_i8 (.D(dataToRegIF[7]), .SP(clk_c_enable_34), .CK(clk_c), 
            .Q(myReg3_c_7)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg3_i0_i8.GSR = "ENABLED";
    FD1P3AX myReg0_i0_i2 (.D(dataToRegIF[1]), .SP(clk_c_enable_41), .CK(clk_c), 
            .Q(myReg0_c_1)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg0_i0_i2.GSR = "ENABLED";
    FD1P3AX myReg0_i0_i3 (.D(dataToRegIF[2]), .SP(clk_c_enable_41), .CK(clk_c), 
            .Q(myReg0_c_2)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg0_i0_i3.GSR = "ENABLED";
    FD1P3AX myReg0_i0_i4 (.D(dataToRegIF[3]), .SP(clk_c_enable_41), .CK(clk_c), 
            .Q(myReg0_c_3)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg0_i0_i4.GSR = "ENABLED";
    FD1P3AX myReg0_i0_i5 (.D(dataToRegIF[4]), .SP(clk_c_enable_41), .CK(clk_c), 
            .Q(myReg0_c_4)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg0_i0_i5.GSR = "ENABLED";
    FD1P3AX myReg0_i0_i6 (.D(dataToRegIF[5]), .SP(clk_c_enable_41), .CK(clk_c), 
            .Q(myReg0_c_5)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg0_i0_i6.GSR = "ENABLED";
    FD1P3AX myReg0_i0_i7 (.D(dataToRegIF[6]), .SP(clk_c_enable_41), .CK(clk_c), 
            .Q(myReg0_c_6)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg0_i0_i7.GSR = "ENABLED";
    FD1P3AX myReg0_i0_i8 (.D(dataToRegIF[7]), .SP(clk_c_enable_41), .CK(clk_c), 
            .Q(myReg0_c_7)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(102[8] 111[4])
    defparam myReg0_i0_i8.GSR = "ENABLED";
    FD1S3IX dataOut__i1 (.D(n3467), .CK(clk_c), .CD(n9), .Q(dataFromRegIF[1])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(87[8] 99[4])
    defparam dataOut__i1.GSR = "ENABLED";
    FD1S3IX dataOut__i2 (.D(n3460), .CK(clk_c), .CD(n9), .Q(dataFromRegIF[2])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(87[8] 99[4])
    defparam dataOut__i2.GSR = "ENABLED";
    FD1S3IX dataOut__i3 (.D(n3453), .CK(clk_c), .CD(n9), .Q(dataFromRegIF[3])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(87[8] 99[4])
    defparam dataOut__i3.GSR = "ENABLED";
    FD1S3IX dataOut__i4 (.D(n3446), .CK(clk_c), .CD(n9), .Q(dataFromRegIF[4])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(87[8] 99[4])
    defparam dataOut__i4.GSR = "ENABLED";
    FD1S3IX dataOut__i5 (.D(n3439), .CK(clk_c), .CD(n9), .Q(dataFromRegIF[5])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(87[8] 99[4])
    defparam dataOut__i5.GSR = "ENABLED";
    FD1S3IX dataOut__i6 (.D(n3481), .CK(clk_c), .CD(n9), .Q(dataFromRegIF[6])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(87[8] 99[4])
    defparam dataOut__i6.GSR = "ENABLED";
    FD1S3IX dataOut__i7 (.D(n3474), .CK(clk_c), .CD(n9), .Q(dataFromRegIF[7])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=173, LSE_RLINE=187 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/registerinterface.v(87[8] 99[4])
    defparam dataOut__i7.GSR = "ENABLED";
    LUT4 i2856_3_lut (.A(myReg4_c_7), .B(myReg5_c_7), .C(regAddr[0]), 
         .Z(n3470)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2856_3_lut.init = 16'hcaca;
    LUT4 i1_2_lut_3_lut_adj_46 (.A(regAddr[1]), .B(n1561), .C(regAddr[0]), 
         .Z(clk_c_enable_34)) /* synthesis lut_function=(A (B (C))) */ ;
    defparam i1_2_lut_3_lut_adj_46.init = 16'h8080;
    PFUMX i2865 (.BLUT(n3475), .ALUT(n3476), .C0(regAddr[1]), .Z(n3479));
    LUT4 i2855_3_lut (.A(myReg2_c_7), .B(myReg3_c_7), .C(regAddr[0]), 
         .Z(n3469)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2855_3_lut.init = 16'hcaca;
    PFUMX i2866 (.BLUT(n3477), .ALUT(n3478), .C0(regAddr[1]), .Z(n3480));
    PFUMX i2831 (.BLUT(n3442), .ALUT(n3443), .C0(regAddr[1]), .Z(n3445));
    PFUMX i2837 (.BLUT(n3447), .ALUT(n3448), .C0(regAddr[1]), .Z(n3451));
    PFUMX i2823 (.BLUT(n3433), .ALUT(n3434), .C0(regAddr[1]), .Z(n3437));
    LUT4 i2854_3_lut (.A(myReg0_c_7), .B(myReg1_c_7), .C(regAddr[0]), 
         .Z(n3468)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2854_3_lut.init = 16'hcaca;
    LUT4 i2836_3_lut (.A(myReg6_c_3), .B(myReg7_c_3), .C(regAddr[0]), 
         .Z(n3450)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2836_3_lut.init = 16'hcaca;
    PFUMX i2824 (.BLUT(n3435), .ALUT(n3436), .C0(regAddr[1]), .Z(n3438));
    LUT4 i4_4_lut (.A(regAddr[6]), .B(regAddr[5]), .C(regAddr[3]), .D(n6), 
         .Z(n9)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;
    defparam i4_4_lut.init = 16'hfffe;
    LUT4 i1_2_lut (.A(regAddr[4]), .B(regAddr[7]), .Z(n6)) /* synthesis lut_function=(A+(B)) */ ;
    defparam i1_2_lut.init = 16'heeee;
    PFUMX i2830 (.BLUT(n3440), .ALUT(n3441), .C0(regAddr[1]), .Z(n3444));
    LUT4 i2805_4_lut (.A(regAddr[6]), .B(regAddr[2]), .C(regAddr[7]), 
         .D(regAddr[5]), .Z(n3418)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;
    defparam i2805_4_lut.init = 16'hfffe;
    LUT4 i2850_3_lut (.A(myReg6_c_1), .B(myReg7_c_1), .C(regAddr[0]), 
         .Z(n3464)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2850_3_lut.init = 16'hcaca;
    LUT4 i2849_3_lut (.A(myReg4_c_1), .B(myReg5_c_1), .C(regAddr[0]), 
         .Z(n3463)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2849_3_lut.init = 16'hcaca;
    LUT4 i2822_3_lut (.A(myReg6_c_5), .B(myReg7_c_5), .C(regAddr[0]), 
         .Z(n3436)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2822_3_lut.init = 16'hcaca;
    LUT4 i2821_3_lut (.A(myReg4_c_5), .B(myReg5_c_5), .C(regAddr[0]), 
         .Z(n3435)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2821_3_lut.init = 16'hcaca;
    LUT4 i2820_3_lut (.A(myReg2_c_5), .B(myReg3_c_5), .C(regAddr[0]), 
         .Z(n3434)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2820_3_lut.init = 16'hcaca;
    LUT4 i2835_3_lut (.A(myReg4_c_3), .B(myReg5_c_3), .C(regAddr[0]), 
         .Z(n3449)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2835_3_lut.init = 16'hcaca;
    LUT4 i2848_3_lut (.A(myReg2_c_1), .B(myReg3_c_1), .C(regAddr[0]), 
         .Z(n3462)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2848_3_lut.init = 16'hcaca;
    LUT4 i2847_3_lut (.A(myReg0_c_1), .B(myReg1_c_1), .C(regAddr[0]), 
         .Z(n3461)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2847_3_lut.init = 16'hcaca;
    LUT4 i2843_3_lut (.A(myReg6_c_2), .B(myReg7_c_2), .C(regAddr[0]), 
         .Z(n3457)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2843_3_lut.init = 16'hcaca;
    LUT4 i2842_3_lut (.A(myReg4_c_2), .B(myReg5_c_2), .C(regAddr[0]), 
         .Z(n3456)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2842_3_lut.init = 16'hcaca;
    LUT4 i2841_3_lut (.A(myReg2_c_2), .B(myReg3_c_2), .C(regAddr[0]), 
         .Z(n3455)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2841_3_lut.init = 16'hcaca;
    LUT4 i2840_3_lut (.A(myReg0_c_2), .B(myReg1_c_2), .C(regAddr[0]), 
         .Z(n3454)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2840_3_lut.init = 16'hcaca;
    LUT4 i2815_3_lut (.A(myReg6_c_0), .B(myReg7_c_0), .C(regAddr[0]), 
         .Z(n3429)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2815_3_lut.init = 16'hcaca;
    LUT4 i2819_3_lut (.A(myReg0_c_5), .B(myReg1_c_5), .C(regAddr[0]), 
         .Z(n3433)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2819_3_lut.init = 16'hcaca;
    LUT4 i2834_3_lut (.A(myReg2_c_3), .B(myReg3_c_3), .C(regAddr[0]), 
         .Z(n3448)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2834_3_lut.init = 16'hcaca;
    LUT4 i2814_3_lut (.A(myReg4_c_0), .B(myReg5_c_0), .C(regAddr[0]), 
         .Z(n3428)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2814_3_lut.init = 16'hcaca;
    LUT4 i2813_3_lut (.A(myReg2_c_0), .B(myReg3_c_0), .C(regAddr[0]), 
         .Z(n3427)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2813_3_lut.init = 16'hcaca;
    LUT4 i2833_3_lut (.A(myReg0_c_3), .B(myReg1_c_3), .C(regAddr[0]), 
         .Z(n3447)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2833_3_lut.init = 16'hcaca;
    LUT4 i2829_3_lut (.A(myReg6_c_4), .B(myReg7_c_4), .C(regAddr[0]), 
         .Z(n3443)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2829_3_lut.init = 16'hcaca;
    LUT4 i2828_3_lut (.A(myReg4_c_4), .B(myReg5_c_4), .C(regAddr[0]), 
         .Z(n3442)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2828_3_lut.init = 16'hcaca;
    LUT4 i2812_3_lut (.A(myReg0_c_0), .B(myReg1_c_0), .C(regAddr[0]), 
         .Z(n3426)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i2812_3_lut.init = 16'hcaca;
    L6MUX21 i2846 (.D0(n3458), .D1(n3459), .SD(regAddr[2]), .Z(n3460));
    L6MUX21 i2818 (.D0(n3430), .D1(n3431), .SD(regAddr[2]), .Z(n3432));
    L6MUX21 i2853 (.D0(n3465), .D1(n3466), .SD(regAddr[2]), .Z(n3467));
    L6MUX21 i2860 (.D0(n3472), .D1(n3473), .SD(regAddr[2]), .Z(n3474));
    L6MUX21 i2867 (.D0(n3479), .D1(n3480), .SD(regAddr[2]), .Z(n3481));
    
endmodule
//
// Verilog Description of module PUR
// module not written out since it is a black-box. 
//

//
// Verilog Description of module TSALL
// module not written out since it is a black-box. 
//

//
// Verilog Description of module serialInterface
//

module serialInterface (regAddr, clk_c, \sclDelayed[6] , n282, clk_c_enable_50, 
            n20, n286, clk_c_enable_64, n3648, n293, n291, GND_net, 
            writeEn, clk_c_enable_9, n1323, n3647, dataToRegIF, n279, 
            n292, n3650, next_writeEn, clk_c_enable_13, n6, n3396, 
            n3645, n289, \dataFromRegIF[5] , n288, \dataFromRegIF[4] , 
            n3637, n56, \dataFromRegIF[3] , \txData[6] , \txData[5] , 
            clearStartStopDet, clk_c_enable_57, n1325, \dataFromRegIF[6] , 
            n290, n287, n1465, n338, n283, n281, n1459, n280, 
            n1373, n3493, n3207, sdaOut, clk_c_enable_67, n4, n3183, 
            \dataFromRegIF[2] , \dataFromRegIF[7] , \dataFromRegIF[1] , 
            n15, startStopDetState, n16, n3213, n4_adj_1, n3420, 
            sdaDeb, n7, n14, n1, n3) /* synthesis syn_module_defined=1 */ ;
    output [7:0]regAddr;
    input clk_c;
    input \sclDelayed[6] ;
    output n282;
    input clk_c_enable_50;
    input n20;
    output n286;
    input clk_c_enable_64;
    input n3648;
    output n293;
    output n291;
    input GND_net;
    output writeEn;
    input clk_c_enable_9;
    input n1323;
    output n3647;
    output [7:0]dataToRegIF;
    output n279;
    output n292;
    output n3650;
    output next_writeEn;
    input clk_c_enable_13;
    output n6;
    input n3396;
    output n3645;
    output n289;
    input \dataFromRegIF[5] ;
    output n288;
    input \dataFromRegIF[4] ;
    input n3637;
    output n56;
    input \dataFromRegIF[3] ;
    output \txData[6] ;
    output \txData[5] ;
    output clearStartStopDet;
    input clk_c_enable_57;
    input n1325;
    input \dataFromRegIF[6] ;
    output n290;
    output n287;
    input n1465;
    input n338;
    output n283;
    output n281;
    input n1459;
    output n280;
    input n1373;
    output n3493;
    input n3207;
    output sdaOut;
    input clk_c_enable_67;
    input n4;
    input n3183;
    input \dataFromRegIF[2] ;
    input \dataFromRegIF[7] ;
    input \dataFromRegIF[1] ;
    output n15;
    input [1:0]startStopDetState;
    output n16;
    output n3213;
    output n4_adj_1;
    input n3420;
    input sdaDeb;
    input n7;
    output n14;
    input n1;
    input n3;
    
    wire clk_c /* synthesis SET_AS_NETWORK=clk_c, is_clock=1 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/i2cslave.v(63[7:10])
    
    wire clk_c_enable_48, n598;
    wire [1:0]streamSt;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(86[11:19])
    
    wire n3348, n25, n22, n1485, n1573;
    wire [2:0]bitCnt;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(84[11:17])
    
    wire n7_c, n3652;
    wire [7:0]rxData;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(85[11:17])
    wire [7:0]n1173;
    wire [7:0]txData;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(87[11:17])
    
    wire n3651, n3175;
    wire [7:0]next_regAddr_7__N_317;
    
    wire n3176, n3638, n19, clk_c_enable_74, clk_c_enable_10;
    wire [1:0]n1149;
    
    wire n567, n2022, n1389, n2, clk_c_enable_68;
    wire [7:0]next_txData;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(87[19:30])
    
    wire n604, n1388, n605, n3649, n3635, n3411, n6_adj_400, n1359, 
        n3640, n354, n3641, n599, n600, n601, n602, n603, n3164, 
        n3373, n571, n3369, n570, n3198, n1467, n325, n1463;
    wire [15:0]n277;
    
    wire n1365, n1461, n568, n3644, n3349, n3639, n3374, n3653, 
        n569, n572, n573, n574, n3370, n3371, n35, n3646, n3643, 
        n10, n3422, n6_adj_402, n4_adj_403, n3173, n3174, n4_adj_404, 
        n4_adj_406;
    
    FD1P3AX regAddr__i0 (.D(n598), .SP(clk_c_enable_48), .CK(clk_c), .Q(regAddr[0])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam regAddr__i0.GSR = "ENABLED";
    LUT4 i1_2_lut_3_lut (.A(streamSt[1]), .B(n3348), .C(streamSt[0]), 
         .Z(n25)) /* synthesis lut_function=(A (C)+!A (B+(C))) */ ;
    defparam i1_2_lut_3_lut.init = 16'hf4f4;
    LUT4 i1_4_lut (.A(n22), .B(\sclDelayed[6] ), .C(n282), .D(n1485), 
         .Z(n1573)) /* synthesis lut_function=(!(A (B+!(C+(D))))) */ ;
    defparam i1_4_lut.init = 16'h7775;
    FD1P3AX bitCnt_651__i0 (.D(n20), .SP(clk_c_enable_50), .CK(clk_c), 
            .Q(bitCnt[0]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(352[3] 362[6])
    defparam bitCnt_651__i0.GSR = "ENABLED";
    LUT4 i2_2_lut (.A(n7_c), .B(n286), .Z(n1485)) /* synthesis lut_function=(!(A+!(B))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i2_2_lut.init = 16'h4444;
    LUT4 streamSt_0__bdd_4_lut_then_3_lut (.A(streamSt[0]), .B(n3348), .C(streamSt[1]), 
         .Z(n3652)) /* synthesis lut_function=(!(A+((C)+!B))) */ ;
    defparam streamSt_0__bdd_4_lut_then_3_lut.init = 16'h0404;
    FD1P3IX rxData__i0 (.D(n1173[0]), .SP(clk_c_enable_64), .CD(n3648), 
            .CK(clk_c), .Q(rxData[0])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam rxData__i0.GSR = "ENABLED";
    FD1S3AX CurrState_SISt_FSM_i0 (.D(n3648), .CK(clk_c), .Q(n293));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i0.GSR = "ENABLED";
    LUT4 streamSt_0__bdd_4_lut_else_3_lut (.A(n291), .B(txData[7]), .C(n293), 
         .Z(n3651)) /* synthesis lut_function=(A (B)+!A (B+!(C))) */ ;
    defparam streamSt_0__bdd_4_lut_else_3_lut.init = 16'hcdcd;
    CCU2D regAddr_7__I_0_167_7 (.A0(regAddr[5]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(regAddr[6]), .B1(GND_net), .C1(GND_net), 
          .D1(GND_net), .CIN(n3175), .COUT(n3176), .S0(next_regAddr_7__N_317[5]), 
          .S1(next_regAddr_7__N_317[6]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(314[23:37])
    defparam regAddr_7__I_0_167_7.INIT0 = 16'h5aaa;
    defparam regAddr_7__I_0_167_7.INIT1 = 16'h5aaa;
    defparam regAddr_7__I_0_167_7.INJECT1_0 = "NO";
    defparam regAddr_7__I_0_167_7.INJECT1_1 = "NO";
    FD1P3AX writeEn_132 (.D(n1323), .SP(clk_c_enable_9), .CK(clk_c), .Q(writeEn));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam writeEn_132.GSR = "ENABLED";
    LUT4 i2912_3_lut_4_lut (.A(n3638), .B(n3647), .C(n19), .D(n3648), 
         .Z(clk_c_enable_74)) /* synthesis lut_function=(A (B (C+(D)))+!A (C+(D))) */ ;
    defparam i2912_3_lut_4_lut.init = 16'hddd0;
    FD1P3IX streamSt__i1 (.D(n1149[1]), .SP(clk_c_enable_10), .CD(n3648), 
            .CK(clk_c), .Q(streamSt[1])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam streamSt__i1.GSR = "ENABLED";
    FD1P3IX dataOut__i0 (.D(n567), .SP(clk_c_enable_74), .CD(n3648), .CK(clk_c), 
            .Q(dataToRegIF[0])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam dataOut__i0.GSR = "ENABLED";
    LUT4 i1_4_lut_adj_12 (.A(n2022), .B(n1389), .C(n2), .D(n3648), .Z(clk_c_enable_48)) /* synthesis lut_function=(!(A ((D)+!B)+!A ((C+(D))+!B))) */ ;
    defparam i1_4_lut_adj_12.init = 16'h008c;
    FD1P3IX txData__i7 (.D(next_txData[7]), .SP(clk_c_enable_68), .CD(n3648), 
            .CK(clk_c), .Q(txData[7])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam txData__i7.GSR = "ENABLED";
    LUT4 i1410_4_lut (.A(rxData[6]), .B(next_regAddr_7__N_317[6]), .C(n2), 
         .D(n2022), .Z(n604)) /* synthesis lut_function=(A (B ((D)+!C)+!B (C (D)))+!A !((C)+!B)) */ ;
    defparam i1410_4_lut.init = 16'hac0c;
    LUT4 i764_3_lut (.A(n1388), .B(writeEn), .C(n279), .Z(n1389)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i764_3_lut.init = 16'hcaca;
    LUT4 i1414_4_lut (.A(rxData[7]), .B(next_regAddr_7__N_317[7]), .C(n2), 
         .D(n2022), .Z(n605)) /* synthesis lut_function=(A (B ((D)+!C)+!B (C (D)))+!A !((C)+!B)) */ ;
    defparam i1414_4_lut.init = 16'hac0c;
    LUT4 i763_4_lut (.A(n292), .B(n3650), .C(next_writeEn), .D(n3649), 
         .Z(n1388)) /* synthesis lut_function=(A (B (C+!(D))+!B !(C+(D)))+!A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i763_4_lut.init = 16'hc0ca;
    FD1P3IX streamSt__i0 (.D(n1149[0]), .SP(clk_c_enable_13), .CD(n3648), 
            .CK(clk_c), .Q(streamSt[0])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam streamSt__i0.GSR = "ENABLED";
    LUT4 i2798_4_lut (.A(n3635), .B(bitCnt[2]), .C(bitCnt[1]), .D(n6), 
         .Z(n3411)) /* synthesis lut_function=(!((B (C (D))+!B !(C (D)))+!A)) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(352[3] 362[6])
    defparam i2798_4_lut.init = 16'h2888;
    LUT4 i1_2_lut_rep_55 (.A(streamSt[0]), .B(streamSt[1]), .Z(n3649)) /* synthesis lut_function=((B)+!A) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam i1_2_lut_rep_55.init = 16'hdddd;
    LUT4 i1938_4_lut (.A(n3648), .B(n3396), .C(n3649), .D(n292), .Z(clk_c_enable_68)) /* synthesis lut_function=(A+!(B (C (D))+!B (C+!(D)))) */ ;
    defparam i1938_4_lut.init = 16'hafee;
    LUT4 i1_2_lut_3_lut_4_lut (.A(n3645), .B(n289), .C(n292), .D(n3649), 
         .Z(n6_adj_400)) /* synthesis lut_function=(A (C (D))+!A (B+(C (D)))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i1_2_lut_3_lut_4_lut.init = 16'hf444;
    LUT4 i2140_3_lut (.A(rxData[0]), .B(next_regAddr_7__N_317[0]), .C(n2), 
         .Z(n598)) /* synthesis lut_function=(A (B+(C))+!A !((C)+!B)) */ ;
    defparam i2140_3_lut.init = 16'hacac;
    LUT4 i818_4_lut (.A(txData[4]), .B(\dataFromRegIF[5] ), .C(n292), 
         .D(n293), .Z(next_txData[5])) /* synthesis lut_function=(A (B (C+!(D))+!B !(C+(D)))+!A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i818_4_lut.init = 16'hc0ca;
    LUT4 i2_3_lut_rep_56 (.A(bitCnt[2]), .B(bitCnt[1]), .C(bitCnt[0]), 
         .Z(n3650)) /* synthesis lut_function=(A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(352[3] 362[6])
    defparam i2_3_lut_rep_56.init = 16'h8080;
    LUT4 i2_3_lut (.A(next_writeEn), .B(n3650), .C(n279), .Z(n2)) /* synthesis lut_function=(!(((C)+!B)+!A)) */ ;
    defparam i2_3_lut.init = 16'h0808;
    LUT4 i735_3_lut_4_lut (.A(n3645), .B(n289), .C(n288), .D(\sclDelayed[6] ), 
         .Z(n1359)) /* synthesis lut_function=(A (C (D))+!A (B+(C (D)))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i735_3_lut_4_lut.init = 16'hf444;
    LUT4 i816_4_lut (.A(txData[3]), .B(\dataFromRegIF[4] ), .C(n292), 
         .D(n293), .Z(next_txData[4])) /* synthesis lut_function=(A (B (C+!(D))+!B !(C+(D)))+!A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i816_4_lut.init = 16'hc0ca;
    LUT4 i1_2_lut_rep_46 (.A(\sclDelayed[6] ), .B(n7_c), .Z(n3640)) /* synthesis lut_function=(A+!(B)) */ ;
    defparam i1_2_lut_rep_46.init = 16'hbbbb;
    LUT4 i1_2_lut_4_lut (.A(bitCnt[2]), .B(bitCnt[1]), .C(bitCnt[0]), 
         .D(next_writeEn), .Z(n22)) /* synthesis lut_function=(A (B (C+!(D))+!B !(D))+!A !(D)) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(352[3] 362[6])
    defparam i1_2_lut_4_lut.init = 16'h80ff;
    LUT4 i178_2_lut_3_lut (.A(\sclDelayed[6] ), .B(n7_c), .C(n286), .Z(n354)) /* synthesis lut_function=(!(A+!(B (C)))) */ ;
    defparam i178_2_lut_3_lut.init = 16'h4040;
    LUT4 i177_2_lut_rep_44_4_lut (.A(bitCnt[2]), .B(bitCnt[1]), .C(bitCnt[0]), 
         .D(next_writeEn), .Z(n3638)) /* synthesis lut_function=(A (B (C (D)))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(352[3] 362[6])
    defparam i177_2_lut_rep_44_4_lut.init = 16'h8000;
    LUT4 n9_bdd_4_lut (.A(n3637), .B(n56), .C(n3648), .D(bitCnt[0]), 
         .Z(n6)) /* synthesis lut_function=(!(A (B+(C))+!A (C+!(D)))) */ ;
    defparam n9_bdd_4_lut.init = 16'h0702;
    LUT4 i1_2_lut_3_lut_adj_13 (.A(n3650), .B(next_writeEn), .C(rxData[0]), 
         .Z(n567)) /* synthesis lut_function=(A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i1_2_lut_3_lut_adj_13.init = 16'h8080;
    LUT4 i1_2_lut_rep_47 (.A(streamSt[0]), .B(streamSt[1]), .Z(n3641)) /* synthesis lut_function=(!((B)+!A)) */ ;
    defparam i1_2_lut_rep_47.init = 16'h2222;
    LUT4 i814_4_lut (.A(txData[2]), .B(\dataFromRegIF[3] ), .C(n292), 
         .D(n293), .Z(next_txData[3])) /* synthesis lut_function=(A (B (C+!(D))+!B !(C+(D)))+!A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i814_4_lut.init = 16'hc0ca;
    FD1P3AX regAddr__i1 (.D(n599), .SP(clk_c_enable_48), .CK(clk_c), .Q(regAddr[1])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam regAddr__i1.GSR = "ENABLED";
    FD1P3AX regAddr__i2 (.D(n600), .SP(clk_c_enable_48), .CK(clk_c), .Q(regAddr[2])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam regAddr__i2.GSR = "ENABLED";
    FD1P3AX regAddr__i3 (.D(n601), .SP(clk_c_enable_48), .CK(clk_c), .Q(regAddr[3])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam regAddr__i3.GSR = "ENABLED";
    FD1P3AX regAddr__i4 (.D(n602), .SP(clk_c_enable_48), .CK(clk_c), .Q(regAddr[4])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam regAddr__i4.GSR = "ENABLED";
    FD1P3AX regAddr__i5 (.D(n603), .SP(clk_c_enable_48), .CK(clk_c), .Q(regAddr[5])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam regAddr__i5.GSR = "ENABLED";
    FD1P3AX regAddr__i6 (.D(n604), .SP(clk_c_enable_48), .CK(clk_c), .Q(regAddr[6])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam regAddr__i6.GSR = "ENABLED";
    FD1P3AX regAddr__i7 (.D(n605), .SP(clk_c_enable_48), .CK(clk_c), .Q(regAddr[7])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam regAddr__i7.GSR = "ENABLED";
    FD1P3AX bitCnt_651__i1 (.D(n3164), .SP(clk_c_enable_50), .CK(clk_c), 
            .Q(bitCnt[1]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(352[3] 362[6])
    defparam bitCnt_651__i1.GSR = "ENABLED";
    FD1P3AX bitCnt_651__i2 (.D(n3411), .SP(clk_c_enable_50), .CK(clk_c), 
            .Q(bitCnt[2]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(352[3] 362[6])
    defparam bitCnt_651__i2.GSR = "ENABLED";
    FD1P3IX txData__i6 (.D(next_txData[6]), .SP(clk_c_enable_68), .CD(n3648), 
            .CK(clk_c), .Q(\txData[6] )) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam txData__i6.GSR = "ENABLED";
    FD1P3IX txData__i5 (.D(next_txData[5]), .SP(clk_c_enable_68), .CD(n3648), 
            .CK(clk_c), .Q(\txData[5] )) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam txData__i5.GSR = "ENABLED";
    FD1P3IX txData__i4 (.D(next_txData[4]), .SP(clk_c_enable_68), .CD(n3648), 
            .CK(clk_c), .Q(txData[4])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam txData__i4.GSR = "ENABLED";
    FD1P3IX txData__i3 (.D(next_txData[3]), .SP(clk_c_enable_68), .CD(n3648), 
            .CK(clk_c), .Q(txData[3])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam txData__i3.GSR = "ENABLED";
    FD1P3IX txData__i2 (.D(next_txData[2]), .SP(clk_c_enable_68), .CD(n3648), 
            .CK(clk_c), .Q(txData[2])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam txData__i2.GSR = "ENABLED";
    FD1P3IX txData__i1 (.D(next_txData[1]), .SP(clk_c_enable_68), .CD(n3648), 
            .CK(clk_c), .Q(txData[1])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam txData__i1.GSR = "ENABLED";
    FD1P3AX clearStartStopDet_134 (.D(n1325), .SP(clk_c_enable_57), .CK(clk_c), 
            .Q(clearStartStopDet));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam clearStartStopDet_134.GSR = "ENABLED";
    FD1P3IX rxData__i1 (.D(n1173[1]), .SP(clk_c_enable_64), .CD(n3648), 
            .CK(clk_c), .Q(rxData[1])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam rxData__i1.GSR = "ENABLED";
    FD1P3IX rxData__i2 (.D(n1173[2]), .SP(clk_c_enable_64), .CD(n3648), 
            .CK(clk_c), .Q(rxData[2])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam rxData__i2.GSR = "ENABLED";
    FD1P3IX rxData__i3 (.D(n1173[3]), .SP(clk_c_enable_64), .CD(n3648), 
            .CK(clk_c), .Q(rxData[3])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam rxData__i3.GSR = "ENABLED";
    FD1P3IX rxData__i4 (.D(n1173[4]), .SP(clk_c_enable_64), .CD(n3648), 
            .CK(clk_c), .Q(rxData[4])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam rxData__i4.GSR = "ENABLED";
    FD1P3IX rxData__i5 (.D(n1173[5]), .SP(clk_c_enable_64), .CD(n3648), 
            .CK(clk_c), .Q(rxData[5])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam rxData__i5.GSR = "ENABLED";
    FD1P3IX rxData__i6 (.D(n1173[6]), .SP(clk_c_enable_64), .CD(n3648), 
            .CK(clk_c), .Q(rxData[6])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam rxData__i6.GSR = "ENABLED";
    FD1P3IX rxData__i7 (.D(n1173[7]), .SP(clk_c_enable_64), .CD(n3648), 
            .CK(clk_c), .Q(rxData[7])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam rxData__i7.GSR = "ENABLED";
    FD1S3IX CurrState_SISt_FSM_i1 (.D(n3373), .CK(clk_c), .CD(n3648), 
            .Q(n292));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i1.GSR = "ENABLED";
    LUT4 i1_2_lut (.A(streamSt[1]), .B(streamSt[0]), .Z(n2022)) /* synthesis lut_function=(!((B)+!A)) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam i1_2_lut.init = 16'h2222;
    LUT4 i1_2_lut_3_lut_adj_14 (.A(n3650), .B(next_writeEn), .C(rxData[4]), 
         .Z(n571)) /* synthesis lut_function=(A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i1_2_lut_3_lut_adj_14.init = 16'h8080;
    LUT4 i1_2_lut_3_lut_adj_15 (.A(streamSt[0]), .B(streamSt[1]), .C(\dataFromRegIF[6] ), 
         .Z(n3369)) /* synthesis lut_function=(!((B+!(C))+!A)) */ ;
    defparam i1_2_lut_3_lut_adj_15.init = 16'h2020;
    LUT4 i1931_2_lut_3_lut (.A(n3650), .B(next_writeEn), .C(rxData[3]), 
         .Z(n570)) /* synthesis lut_function=(A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i1931_2_lut_3_lut.init = 16'h8080;
    FD1S3IX CurrState_SISt_FSM_i2 (.D(n3198), .CK(clk_c), .CD(n3648), 
            .Q(n291));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i2.GSR = "ENABLED";
    FD1S3IX CurrState_SISt_FSM_i3 (.D(n1467), .CK(clk_c), .CD(n3648), 
            .Q(n290));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i3.GSR = "ENABLED";
    FD1S3IX CurrState_SISt_FSM_i4 (.D(n325), .CK(clk_c), .CD(n3648), .Q(n289));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i4.GSR = "ENABLED";
    FD1S3IX CurrState_SISt_FSM_i5 (.D(n1359), .CK(clk_c), .CD(n3648), 
            .Q(n288));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i5.GSR = "ENABLED";
    FD1S3IX CurrState_SISt_FSM_i6 (.D(n1465), .CK(clk_c), .CD(n3648), 
            .Q(n287));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i6.GSR = "ENABLED";
    FD1S3IX CurrState_SISt_FSM_i7 (.D(n1463), .CK(clk_c), .CD(n3648), 
            .Q(n286));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i7.GSR = "ENABLED";
    FD1S3IX CurrState_SISt_FSM_i8 (.D(n1365), .CK(clk_c), .CD(n3648), 
            .Q(n277[8]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i8.GSR = "ENABLED";
    FD1S3IX CurrState_SISt_FSM_i9 (.D(n338), .CK(clk_c), .CD(n3648), .Q(next_writeEn));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i9.GSR = "ENABLED";
    FD1S3IX CurrState_SISt_FSM_i10 (.D(n1461), .CK(clk_c), .CD(n3648), 
            .Q(n283));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i10.GSR = "ENABLED";
    FD1S3IX CurrState_SISt_FSM_i11 (.D(n1573), .CK(clk_c), .CD(n3648), 
            .Q(n282));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i11.GSR = "ENABLED";
    FD1S3IX CurrState_SISt_FSM_i12 (.D(n1459), .CK(clk_c), .CD(n3648), 
            .Q(n281));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i12.GSR = "ENABLED";
    FD1S3IX CurrState_SISt_FSM_i13 (.D(n1373), .CK(clk_c), .CD(n3648), 
            .Q(n280));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i13.GSR = "ENABLED";
    FD1S3IX CurrState_SISt_FSM_i14 (.D(n3638), .CK(clk_c), .CD(n3648), 
            .Q(n279));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i14.GSR = "ENABLED";
    FD1S3IX CurrState_SISt_FSM_i15 (.D(n354), .CK(clk_c), .CD(n3648), 
            .Q(n277[15]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i15.GSR = "ENABLED";
    FD1P3IX dataOut__i1 (.D(n568), .SP(clk_c_enable_74), .CD(n3648), .CK(clk_c), 
            .Q(dataToRegIF[1])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam dataOut__i1.GSR = "ENABLED";
    LUT4 i2909_4_lut (.A(n3644), .B(n3349), .C(n3638), .D(n6_adj_400), 
         .Z(n3493)) /* synthesis lut_function=(!(A+(B+(C+(D))))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(352[3] 362[6])
    defparam i2909_4_lut.init = 16'h0001;
    LUT4 i1_4_lut_adj_16 (.A(n3639), .B(n286), .C(n3374), .D(n3207), 
         .Z(n3349)) /* synthesis lut_function=(A (B (D))+!A (B ((D)+!C)+!B !(C))) */ ;
    defparam i1_4_lut_adj_16.init = 16'hcd05;
    FD1P3JX sdaOut_131 (.D(n3653), .SP(clk_c_enable_67), .PD(n4), .CK(clk_c), 
            .Q(sdaOut)) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam sdaOut_131.GSR = "ENABLED";
    LUT4 i1_2_lut_3_lut_adj_17 (.A(n3650), .B(next_writeEn), .C(rxData[2]), 
         .Z(n569)) /* synthesis lut_function=(A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i1_2_lut_3_lut_adj_17.init = 16'h8080;
    FD1P3AX txData__i0 (.D(n3183), .SP(clk_c_enable_68), .CK(clk_c), .Q(txData[0])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam txData__i0.GSR = "ENABLED";
    FD1P3IX dataOut__i2 (.D(n569), .SP(clk_c_enable_74), .CD(n3648), .CK(clk_c), 
            .Q(dataToRegIF[2])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam dataOut__i2.GSR = "ENABLED";
    FD1P3IX dataOut__i3 (.D(n570), .SP(clk_c_enable_74), .CD(n3648), .CK(clk_c), 
            .Q(dataToRegIF[3])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam dataOut__i3.GSR = "ENABLED";
    FD1P3IX dataOut__i4 (.D(n571), .SP(clk_c_enable_74), .CD(n3648), .CK(clk_c), 
            .Q(dataToRegIF[4])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam dataOut__i4.GSR = "ENABLED";
    FD1P3IX dataOut__i5 (.D(n572), .SP(clk_c_enable_74), .CD(n3648), .CK(clk_c), 
            .Q(dataToRegIF[5])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam dataOut__i5.GSR = "ENABLED";
    FD1P3IX dataOut__i6 (.D(n573), .SP(clk_c_enable_74), .CD(n3648), .CK(clk_c), 
            .Q(dataToRegIF[6])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam dataOut__i6.GSR = "ENABLED";
    FD1P3IX dataOut__i7 (.D(n574), .SP(clk_c_enable_74), .CD(n3648), .CK(clk_c), 
            .Q(dataToRegIF[7])) /* synthesis LSE_LINE_FILE_ID=6, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=189, LSE_RLINE=201 */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam dataOut__i7.GSR = "ENABLED";
    LUT4 i812_4_lut (.A(txData[1]), .B(\dataFromRegIF[2] ), .C(n292), 
         .D(n293), .Z(next_txData[2])) /* synthesis lut_function=(A (B (C+!(D))+!B !(C+(D)))+!A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i812_4_lut.init = 16'hc0ca;
    LUT4 i1_2_lut_3_lut_adj_18 (.A(streamSt[0]), .B(streamSt[1]), .C(\dataFromRegIF[7] ), 
         .Z(n3370)) /* synthesis lut_function=(!((B+!(C))+!A)) */ ;
    defparam i1_2_lut_3_lut_adj_18.init = 16'h2020;
    LUT4 i810_4_lut (.A(txData[0]), .B(\dataFromRegIF[1] ), .C(n292), 
         .D(n293), .Z(next_txData[1])) /* synthesis lut_function=(A (B (C+!(D))+!B !(C+(D)))+!A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i810_4_lut.init = 16'hc0ca;
    LUT4 i1_2_lut_3_lut_adj_19 (.A(n3650), .B(next_writeEn), .C(rxData[1]), 
         .Z(n568)) /* synthesis lut_function=(A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i1_2_lut_3_lut_adj_19.init = 16'h8080;
    LUT4 i2908_4_lut (.A(next_writeEn), .B(n3648), .C(n25), .D(n15), 
         .Z(clk_c_enable_10)) /* synthesis lut_function=(!(A (B (C)+!B (C+(D)))+!A !(B+!(D)))) */ ;
    defparam i2908_4_lut.init = 16'h4c5f;
    LUT4 i1_4_lut_adj_20 (.A(startStopDetState[0]), .B(streamSt[0]), .C(startStopDetState[1]), 
         .D(streamSt[1]), .Z(n7_c)) /* synthesis lut_function=(!(A+!(B (C)+!B (C+!(D))))) */ ;
    defparam i1_4_lut_adj_20.init = 16'h5051;
    LUT4 i25_3_lut (.A(n293), .B(n3650), .C(next_writeEn), .Z(n19)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i25_3_lut.init = 16'hcaca;
    CCU2D regAddr_7__I_0_167_9 (.A0(regAddr[7]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(GND_net), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n3176), .S0(next_regAddr_7__N_317[7]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(314[23:37])
    defparam regAddr_7__I_0_167_9.INIT0 = 16'h5aaa;
    defparam regAddr_7__I_0_167_9.INIT1 = 16'h0000;
    defparam regAddr_7__I_0_167_9.INJECT1_0 = "NO";
    defparam regAddr_7__I_0_167_9.INJECT1_1 = "NO";
    LUT4 i1_2_lut_3_lut_adj_21 (.A(n3650), .B(next_writeEn), .C(rxData[7]), 
         .Z(n574)) /* synthesis lut_function=(A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i1_2_lut_3_lut_adj_21.init = 16'h8080;
    LUT4 i1_2_lut_3_lut_adj_22 (.A(n3650), .B(next_writeEn), .C(rxData[6]), 
         .Z(n573)) /* synthesis lut_function=(A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i1_2_lut_3_lut_adj_22.init = 16'h8080;
    LUT4 i29_4_lut (.A(n280), .B(n3640), .C(n286), .D(n3371), .Z(n16)) /* synthesis lut_function=(A (B (C))+!A (B (C+!(D))+!B !(C+(D)))) */ ;
    defparam i29_4_lut.init = 16'hc0c5;
    LUT4 i1_2_lut_3_lut_adj_23 (.A(n3650), .B(next_writeEn), .C(rxData[5]), 
         .Z(n572)) /* synthesis lut_function=(A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i1_2_lut_3_lut_adj_23.init = 16'h8080;
    LUT4 i1_4_lut_adj_24 (.A(next_writeEn), .B(streamSt[1]), .C(n35), 
         .D(streamSt[0]), .Z(n1149[0])) /* synthesis lut_function=(A (B+(C+(D)))) */ ;
    defparam i1_4_lut_adj_24.init = 16'haaa8;
    LUT4 i2_3_lut_4_lut (.A(n293), .B(n286), .C(n282), .D(n3646), .Z(n3213)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;
    defparam i2_3_lut_4_lut.init = 16'hfffe;
    LUT4 i1_2_lut_adj_25 (.A(rxData[0]), .B(n3348), .Z(n35)) /* synthesis lut_function=(!((B)+!A)) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam i1_2_lut_adj_25.init = 16'h2222;
    LUT4 i1_2_lut_adj_26 (.A(n293), .B(n277[15]), .Z(n3371)) /* synthesis lut_function=(A+(B)) */ ;
    defparam i1_2_lut_adj_26.init = 16'heeee;
    LUT4 mux_296_i2_3_lut (.A(rxData[1]), .B(next_regAddr_7__N_317[1]), 
         .C(n2), .Z(n599)) /* synthesis lut_function=(A (B+(C))+!A !((C)+!B)) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam mux_296_i2_3_lut.init = 16'hacac;
    LUT4 i1_4_lut_adj_27 (.A(n286), .B(n3643), .C(\sclDelayed[6] ), .D(n10), 
         .Z(n4_adj_1)) /* synthesis lut_function=(!(A (B+(C+(D)))+!A !((C)+!B))) */ ;
    defparam i1_4_lut_adj_27.init = 16'h5153;
    LUT4 i1_2_lut_rep_49 (.A(n282), .B(n277[8]), .Z(n3643)) /* synthesis lut_function=(A+(B)) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i1_2_lut_rep_49.init = 16'heeee;
    LUT4 i1_2_lut_3_lut_adj_28 (.A(n282), .B(n277[8]), .C(rxData[5]), 
         .Z(n1173[6])) /* synthesis lut_function=(A (C)+!A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i1_2_lut_3_lut_adj_28.init = 16'he0e0;
    LUT4 i1_3_lut (.A(next_writeEn), .B(rxData[0]), .C(streamSt[1]), .Z(n1149[1])) /* synthesis lut_function=(A ((C)+!B)) */ ;
    defparam i1_3_lut.init = 16'ha2a2;
    LUT4 i1_2_lut_3_lut_adj_29 (.A(n293), .B(n286), .C(next_writeEn), 
         .Z(n3374)) /* synthesis lut_function=(A+(B+(C))) */ ;
    defparam i1_2_lut_3_lut_adj_29.init = 16'hfefe;
    LUT4 i2_3_lut_adj_30 (.A(streamSt[0]), .B(streamSt[1]), .C(n292), 
         .Z(n56)) /* synthesis lut_function=((B+!(C))+!A) */ ;
    defparam i2_3_lut_adj_30.init = 16'hdfdf;
    LUT4 i1950_2_lut_3_lut (.A(n282), .B(n277[8]), .C(rxData[3]), .Z(n1173[4])) /* synthesis lut_function=(A (C)+!A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i1950_2_lut_3_lut.init = 16'he0e0;
    LUT4 i4_4_lut (.A(rxData[1]), .B(n3422), .C(n10), .D(n6_adj_402), 
         .Z(n3348)) /* synthesis lut_function=(A+((C+(D))+!B)) */ ;
    defparam i4_4_lut.init = 16'hfffb;
    LUT4 i1_2_lut_3_lut_adj_31 (.A(n282), .B(n277[8]), .C(rxData[1]), 
         .Z(n1173[2])) /* synthesis lut_function=(A (C)+!A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i1_2_lut_3_lut_adj_31.init = 16'he0e0;
    LUT4 i1_2_lut_adj_32 (.A(rxData[2]), .B(rxData[7]), .Z(n6_adj_402)) /* synthesis lut_function=(A+(B)) */ ;
    defparam i1_2_lut_adj_32.init = 16'heeee;
    LUT4 i2808_4_lut (.A(rxData[6]), .B(rxData[3]), .C(rxData[5]), .D(rxData[4]), 
         .Z(n3422)) /* synthesis lut_function=(A (B (C (D)))) */ ;
    defparam i2808_4_lut.init = 16'h8000;
    LUT4 i2557_3_lut_4_lut_4_lut (.A(n3648), .B(bitCnt[1]), .C(n6), .D(n3637), 
         .Z(n3164)) /* synthesis lut_function=(!(A+(B (C+(D))+!B ((D)+!C)))) */ ;
    defparam i2557_3_lut_4_lut_4_lut.init = 16'h0014;
    LUT4 mux_296_i3_3_lut (.A(rxData[2]), .B(next_regAddr_7__N_317[2]), 
         .C(n2), .Z(n600)) /* synthesis lut_function=(A (B+(C))+!A !((C)+!B)) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam mux_296_i3_3_lut.init = 16'hacac;
    LUT4 i1_4_lut_adj_33 (.A(n281), .B(n3371), .C(n287), .D(\sclDelayed[6] ), 
         .Z(n3373)) /* synthesis lut_function=(A (B+(C+!(D)))+!A (B+(C (D)))) */ ;
    defparam i1_4_lut_adj_33.init = 16'hfcee;
    LUT4 i2_3_lut_adj_34 (.A(\sclDelayed[6] ), .B(n4_adj_403), .C(n291), 
         .Z(n3198)) /* synthesis lut_function=(A (B+(C))+!A (B)) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i2_3_lut_adj_34.init = 16'hecec;
    LUT4 i1_2_lut_3_lut_adj_35 (.A(n282), .B(n277[8]), .C(rxData[2]), 
         .Z(n1173[3])) /* synthesis lut_function=(A (C)+!A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i1_2_lut_3_lut_adj_35.init = 16'he0e0;
    LUT4 mux_296_i4_3_lut (.A(rxData[3]), .B(next_regAddr_7__N_317[3]), 
         .C(n2), .Z(n601)) /* synthesis lut_function=(A (B+(C))+!A !((C)+!B)) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam mux_296_i4_3_lut.init = 16'hacac;
    LUT4 i2915_2_lut_rep_41_4_lut_4_lut (.A(n3648), .B(n3207), .C(n3420), 
         .D(n286), .Z(n3635)) /* synthesis lut_function=(!(A+!(B (C)+!B (C+(D))))) */ ;
    defparam i2915_2_lut_rep_41_4_lut_4_lut.init = 16'h5150;
    LUT4 equal_649_i10_2_lut (.A(startStopDetState[0]), .B(startStopDetState[1]), 
         .Z(n10)) /* synthesis lut_function=((B)+!A) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(252[13:40])
    defparam equal_649_i10_2_lut.init = 16'hdddd;
    LUT4 i1_4_lut_adj_36 (.A(n289), .B(n292), .C(n3645), .D(n3641), 
         .Z(n4_adj_403)) /* synthesis lut_function=(A (B (C+(D))+!B (C))+!A (B (D))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i1_4_lut_adj_36.init = 16'heca0;
    LUT4 i1_2_lut_3_lut_adj_37 (.A(n282), .B(n277[8]), .C(rxData[4]), 
         .Z(n1173[5])) /* synthesis lut_function=(A (C)+!A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i1_2_lut_3_lut_adj_37.init = 16'he0e0;
    LUT4 i1_2_lut_rep_52 (.A(n292), .B(n277[8]), .Z(n3646)) /* synthesis lut_function=(A+(B)) */ ;
    defparam i1_2_lut_rep_52.init = 16'heeee;
    LUT4 i1_2_lut_rep_45_3_lut (.A(n292), .B(n277[8]), .C(n289), .Z(n3639)) /* synthesis lut_function=(A+(B+(C))) */ ;
    defparam i1_2_lut_rep_45_3_lut.init = 16'hfefe;
    LUT4 i1_2_lut_3_lut_adj_38 (.A(n282), .B(n277[8]), .C(sdaDeb), .Z(n1173[0])) /* synthesis lut_function=(A (C)+!A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i1_2_lut_3_lut_adj_38.init = 16'he0e0;
    LUT4 mux_296_i5_3_lut (.A(rxData[4]), .B(next_regAddr_7__N_317[4]), 
         .C(n2), .Z(n602)) /* synthesis lut_function=(A (B+(C))+!A !((C)+!B)) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam mux_296_i5_3_lut.init = 16'hacac;
    LUT4 i1_2_lut_3_lut_adj_39 (.A(n282), .B(n277[8]), .C(rxData[0]), 
         .Z(n1173[1])) /* synthesis lut_function=(A (C)+!A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i1_2_lut_3_lut_adj_39.init = 16'he0e0;
    CCU2D regAddr_7__I_0_167_3 (.A0(regAddr[1]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(regAddr[2]), .B1(GND_net), .C1(GND_net), 
          .D1(GND_net), .CIN(n3173), .COUT(n3174), .S0(next_regAddr_7__N_317[1]), 
          .S1(next_regAddr_7__N_317[2]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(314[23:37])
    defparam regAddr_7__I_0_167_3.INIT0 = 16'h5aaa;
    defparam regAddr_7__I_0_167_3.INIT1 = 16'h5aaa;
    defparam regAddr_7__I_0_167_3.INJECT1_0 = "NO";
    defparam regAddr_7__I_0_167_3.INJECT1_1 = "NO";
    LUT4 i1_4_lut_adj_40 (.A(n22), .B(n287), .C(n4_adj_404), .D(n3374), 
         .Z(n15)) /* synthesis lut_function=((B (C)+!B (C+!(D)))+!A) */ ;
    defparam i1_4_lut_adj_40.init = 16'hf5f7;
    LUT4 i1_2_lut_rep_53 (.A(streamSt[1]), .B(streamSt[0]), .Z(n3647)) /* synthesis lut_function=(A (B)) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(337[8] 363[4])
    defparam i1_2_lut_rep_53.init = 16'h8888;
    LUT4 i1_4_lut_adj_41 (.A(n3640), .B(n7), .C(n4_adj_406), .D(n287), 
         .Z(n4_adj_404)) /* synthesis lut_function=(A (B (C)+!B (C+(D)))+!A !(B+!(D))) */ ;
    defparam i1_4_lut_adj_41.init = 16'hb3a0;
    LUT4 i837_3_lut (.A(\sclDelayed[6] ), .B(n290), .C(n291), .Z(n1467)) /* synthesis lut_function=(!(A+!(B+(C)))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i837_3_lut.init = 16'h5454;
    LUT4 i149_2_lut (.A(\sclDelayed[6] ), .B(n290), .Z(n325)) /* synthesis lut_function=(A (B)) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i149_2_lut.init = 16'h8888;
    LUT4 i835_3_lut (.A(\sclDelayed[6] ), .B(n286), .C(n277[8]), .Z(n1463)) /* synthesis lut_function=(A (B+(C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i835_3_lut.init = 16'ha8a8;
    LUT4 i5_3_lut_4_lut (.A(n289), .B(n3646), .C(n283), .D(n277[15]), 
         .Z(n14)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;
    defparam i5_3_lut_4_lut.init = 16'hfffe;
    LUT4 i1418_4_lut (.A(rxData[5]), .B(next_regAddr_7__N_317[5]), .C(n2), 
         .D(n2022), .Z(n603)) /* synthesis lut_function=(A (B ((D)+!C)+!B (C (D)))+!A !((C)+!B)) */ ;
    defparam i1418_4_lut.init = 16'hac0c;
    LUT4 i1_2_lut_3_lut_adj_42 (.A(n282), .B(n277[8]), .C(rxData[6]), 
         .Z(n1173[7])) /* synthesis lut_function=(A (C)+!A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i1_2_lut_3_lut_adj_42.init = 16'he0e0;
    LUT4 i740_2_lut_rep_50 (.A(\sclDelayed[6] ), .B(n277[8]), .Z(n3644)) /* synthesis lut_function=(!(A+!(B))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i740_2_lut_rep_50.init = 16'h4444;
    LUT4 i741_2_lut_3_lut_4_lut (.A(\sclDelayed[6] ), .B(n277[8]), .C(n292), 
         .D(n3649), .Z(n1365)) /* synthesis lut_function=(A (C (D))+!A (B+(C (D)))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i741_2_lut_3_lut_4_lut.init = 16'hf444;
    LUT4 i2_3_lut_rep_51 (.A(bitCnt[1]), .B(bitCnt[2]), .C(bitCnt[0]), 
         .Z(n3645)) /* synthesis lut_function=(A+(B+(C))) */ ;
    defparam i2_3_lut_rep_51.init = 16'hfefe;
    LUT4 i1_4_lut_adj_43 (.A(n286), .B(n10), .C(\sclDelayed[6] ), .D(next_writeEn), 
         .Z(n4_adj_406)) /* synthesis lut_function=(A (B (C+!(D))+!B (C))) */ ;
    defparam i1_4_lut_adj_43.init = 16'ha0a8;
    CCU2D regAddr_7__I_0_167_1 (.A0(GND_net), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(regAddr[0]), .B1(GND_net), .C1(GND_net), 
          .D1(GND_net), .COUT(n3173), .S1(next_regAddr_7__N_317[0]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(314[23:37])
    defparam regAddr_7__I_0_167_1.INIT0 = 16'hF000;
    defparam regAddr_7__I_0_167_1.INIT1 = 16'h5555;
    defparam regAddr_7__I_0_167_1.INJECT1_0 = "NO";
    defparam regAddr_7__I_0_167_1.INJECT1_1 = "NO";
    PFUMX i1881 (.BLUT(n1), .ALUT(n3370), .C0(n292), .Z(next_txData[7]));
    LUT4 i834_3_lut (.A(\sclDelayed[6] ), .B(n283), .C(n282), .Z(n1461)) /* synthesis lut_function=(A (B+(C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(130[3] 324[10])
    defparam i834_3_lut.init = 16'ha8a8;
    CCU2D regAddr_7__I_0_167_5 (.A0(regAddr[3]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(regAddr[4]), .B1(GND_net), .C1(GND_net), 
          .D1(GND_net), .CIN(n3174), .COUT(n3175), .S0(next_regAddr_7__N_317[3]), 
          .S1(next_regAddr_7__N_317[4]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_test_4/serialinterface.v(314[23:37])
    defparam regAddr_7__I_0_167_5.INIT0 = 16'h5aaa;
    defparam regAddr_7__I_0_167_5.INIT1 = 16'h5aaa;
    defparam regAddr_7__I_0_167_5.INJECT1_0 = "NO";
    defparam regAddr_7__I_0_167_5.INJECT1_1 = "NO";
    PFUMX i1887 (.BLUT(n3), .ALUT(n3369), .C0(n292), .Z(next_txData[6]));
    PFUMX i2977 (.BLUT(n3651), .ALUT(n3652), .C0(next_writeEn), .Z(n3653));
    
endmodule
