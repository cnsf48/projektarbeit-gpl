// Verilog netlist produced by program LSE :  version Diamond (64-bit) 3.9.1.119
// Netlist written on Sun Sep 16 23:19:06 2018
//
// Verilog Description of module serialize
//

module serialize (clk, reset, data, tmp_out, load, serial_out);   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(5[8:17])
    input clk;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(7[3:6])
    input reset;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(8[3:8])
    input [7:0]data;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(9[3:7])
    output [7:0]tmp_out;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(10[3:10])
    output load;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(11[3:7])
    output serial_out;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(12[3:13])
    
    wire clk_c /* synthesis is_clock=1, SET_AS_NETWORK=clk_c */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(7[3:6])
    
    wire VCC_net, GND_net, reset_c, data_c_7, data_c_6, data_c_5, 
        data_c_4, data_c_3, data_c_2, data_c_1, data_c_0, serial_out_c, 
        tmp_out_c_6, tmp_out_c_5, tmp_out_c_4, tmp_out_c_3, tmp_out_c_2, 
        tmp_out_c_1, tmp_out_c_0, load_c;
    wire [2:0]state;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(19[9:14])
    wire [7:0]tmp_out_7__N_1;
    wire [2:0]state_2__N_9;
    
    wire clk_c_enable_1, n79, n236, n165;
    
    VLO i201 (.Z(GND_net));
    LUT4 mux_43_i6_3_lut (.A(data_c_5), .B(tmp_out_c_4), .C(n79), .Z(tmp_out_7__N_1[5])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(31[4] 64[13])
    defparam mux_43_i6_3_lut.init = 16'hcaca;
    IB data_pad_5 (.I(data[5]), .O(data_c_5));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(9[3:7])
    IB data_pad_0 (.I(data[0]), .O(data_c_0));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(9[3:7])
    IB data_pad_6 (.I(data[6]), .O(data_c_6));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(9[3:7])
    FD1P3IX loading_16 (.D(n236), .SP(clk_c_enable_1), .CD(n165), .CK(clk_c), 
            .Q(load_c));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(27[3] 65[10])
    defparam loading_16.GSR = "DISABLED";
    VHI i202 (.Z(VCC_net));
    IB data_pad_7 (.I(data[7]), .O(data_c_7));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(9[3:7])
    IB reset_pad (.I(reset), .O(reset_c));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(8[3:8])
    FD1S3AX tmp_i3 (.D(tmp_out_7__N_1[2]), .CK(clk_c), .Q(tmp_out_c_2));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(27[3] 65[10])
    defparam tmp_i3.GSR = "ENABLED";
    IB clk_pad (.I(clk), .O(clk_c));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(7[3:6])
    IB data_pad_1 (.I(data[1]), .O(data_c_1));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(9[3:7])
    OB serial_out_pad (.I(serial_out_c), .O(serial_out));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(12[3:13])
    IB data_pad_2 (.I(data[2]), .O(data_c_2));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(9[3:7])
    LUT4 i24_4_lut_3_lut (.A(state[0]), .B(state[1]), .C(state[2]), .Z(n79)) /* synthesis lut_function=(A+(B+(C))) */ ;
    defparam i24_4_lut_3_lut.init = 16'hfefe;
    OB load_pad (.I(load_c), .O(load));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(11[3:7])
    OB tmp_out_pad_0 (.I(tmp_out_c_0), .O(tmp_out[0]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(10[3:10])
    LUT4 i198_2_lut (.A(state[2]), .B(reset_c), .Z(n165)) /* synthesis lut_function=(!(A+(B))) */ ;
    defparam i198_2_lut.init = 16'h1111;
    OB tmp_out_pad_1 (.I(tmp_out_c_1), .O(tmp_out[1]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(10[3:10])
    OB tmp_out_pad_2 (.I(tmp_out_c_2), .O(tmp_out[2]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(10[3:10])
    TSALL TSALL_INST (.TSALL(GND_net));
    OB tmp_out_pad_3 (.I(tmp_out_c_3), .O(tmp_out[3]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(10[3:10])
    IB data_pad_3 (.I(data[3]), .O(data_c_3));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(9[3:7])
    IB data_pad_4 (.I(data[4]), .O(data_c_4));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(9[3:7])
    LUT4 i47_2_lut (.A(state[0]), .B(state[1]), .Z(state_2__N_9[1])) /* synthesis lut_function=(!(A (B)+!A !(B))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(31[4] 64[13])
    defparam i47_2_lut.init = 16'h6666;
    GSR GSR_INST (.GSR(clk_c_enable_1));
    FD1S3AX tmp_i2 (.D(tmp_out_7__N_1[1]), .CK(clk_c), .Q(tmp_out_c_1));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(27[3] 65[10])
    defparam tmp_i2.GSR = "ENABLED";
    FD1S3IX tmp_i1 (.D(data_c_0), .CK(clk_c), .CD(n79), .Q(tmp_out_c_0));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(27[3] 65[10])
    defparam tmp_i1.GSR = "ENABLED";
    OB tmp_out_pad_4 (.I(tmp_out_c_4), .O(tmp_out[4]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(10[3:10])
    LUT4 mux_43_i2_3_lut (.A(data_c_1), .B(tmp_out_c_0), .C(n79), .Z(tmp_out_7__N_1[1])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(31[4] 64[13])
    defparam mux_43_i2_3_lut.init = 16'hcaca;
    OB tmp_out_pad_5 (.I(tmp_out_c_5), .O(tmp_out[5]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(10[3:10])
    FD1S3AX tmp_i4 (.D(tmp_out_7__N_1[3]), .CK(clk_c), .Q(tmp_out_c_3));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(27[3] 65[10])
    defparam tmp_i4.GSR = "ENABLED";
    LUT4 mux_43_i4_3_lut (.A(data_c_3), .B(tmp_out_c_2), .C(n79), .Z(tmp_out_7__N_1[3])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(31[4] 64[13])
    defparam mux_43_i4_3_lut.init = 16'hcaca;
    LUT4 mux_43_i3_3_lut (.A(data_c_2), .B(tmp_out_c_1), .C(n79), .Z(tmp_out_7__N_1[2])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(31[4] 64[13])
    defparam mux_43_i3_3_lut.init = 16'hcaca;
    LUT4 mux_43_i5_3_lut (.A(data_c_4), .B(tmp_out_c_3), .C(n79), .Z(tmp_out_7__N_1[4])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(31[4] 64[13])
    defparam mux_43_i5_3_lut.init = 16'hcaca;
    OB tmp_out_pad_6 (.I(tmp_out_c_6), .O(tmp_out[6]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(10[3:10])
    OB tmp_out_pad_7 (.I(serial_out_c), .O(tmp_out[7]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(10[3:10])
    FD1S3AX tmp_i5 (.D(tmp_out_7__N_1[4]), .CK(clk_c), .Q(tmp_out_c_4));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(27[3] 65[10])
    defparam tmp_i5.GSR = "ENABLED";
    FD1S3AX tmp_i6 (.D(tmp_out_7__N_1[5]), .CK(clk_c), .Q(tmp_out_c_5));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(27[3] 65[10])
    defparam tmp_i6.GSR = "ENABLED";
    FD1S3AX tmp_i7 (.D(tmp_out_7__N_1[6]), .CK(clk_c), .Q(tmp_out_c_6));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(27[3] 65[10])
    defparam tmp_i7.GSR = "ENABLED";
    FD1S3AX tmp_i8 (.D(tmp_out_7__N_1[7]), .CK(clk_c), .Q(serial_out_c));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(27[3] 65[10])
    defparam tmp_i8.GSR = "ENABLED";
    FD1S3AX state_i1 (.D(state_2__N_9[1]), .CK(clk_c), .Q(state[1]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(27[3] 65[10])
    defparam state_i1.GSR = "ENABLED";
    FD1S3AX state_i2 (.D(state_2__N_9[2]), .CK(clk_c), .Q(state[2]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(27[3] 65[10])
    defparam state_i2.GSR = "ENABLED";
    LUT4 i1_2_lut_rep_2 (.A(state[0]), .B(state[1]), .Z(n236)) /* synthesis lut_function=(A (B)) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(31[4] 64[13])
    defparam i1_2_lut_rep_2.init = 16'h8888;
    LUT4 i72_1_lut (.A(state[0]), .Z(state_2__N_9[0])) /* synthesis lut_function=(!(A)) */ ;
    defparam i72_1_lut.init = 16'h5555;
    LUT4 i54_1_lut (.A(reset_c), .Z(clk_c_enable_1)) /* synthesis lut_function=(!(A)) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(8[3:8])
    defparam i54_1_lut.init = 16'h5555;
    PUR PUR_INST (.PUR(VCC_net));
    defparam PUR_INST.RST_PULSE = 1;
    FD1S3AX state_i0 (.D(state_2__N_9[0]), .CK(clk_c), .Q(state[0]));   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(27[3] 65[10])
    defparam state_i0.GSR = "ENABLED";
    LUT4 mux_43_i7_3_lut (.A(data_c_6), .B(tmp_out_c_5), .C(n79), .Z(tmp_out_7__N_1[6])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(31[4] 64[13])
    defparam mux_43_i7_3_lut.init = 16'hcaca;
    LUT4 i15_2_lut_3_lut (.A(state[0]), .B(state[1]), .C(state[2]), .Z(state_2__N_9[2])) /* synthesis lut_function=(!(A (B (C)+!B !(C))+!A !(C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(31[4] 64[13])
    defparam i15_2_lut_3_lut.init = 16'h7878;
    LUT4 mux_43_i8_3_lut (.A(data_c_7), .B(tmp_out_c_6), .C(n79), .Z(tmp_out_7__N_1[7])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // g:/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialize.vhd(31[4] 64[13])
    defparam mux_43_i8_3_lut.init = 16'hcaca;
    
endmodule
//
// Verilog Description of module TSALL
// module not written out since it is a black-box. 
//

//
// Verilog Description of module PUR
// module not written out since it is a black-box. 
//

