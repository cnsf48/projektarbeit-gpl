// Verilog netlist produced by program LSE :  version Diamond (64-bit) 3.9.1.119
// Netlist written on Thu Dec 20 16:07:25 2018
//
// Verilog Description of module TinyFPGA_A2
//

module TinyFPGA_A2 (pin_sda1, pin_scl1, pin_gpio_08_cs, pin_spi_0_sclk, 
            pin_spi_0_miso, pin_spi_0_mosi, pin_gpio_04, pin_gpio_05, 
            pin_gpio_06, pin_gpio_26, pin_led_1, pin_led_2, pin_led_3, 
            pin_led_4, pin_led_5, pin_led_6, pin_led_7, pin_led_8, 
            pin_led_in_1, pin_led_in_2, pin_led_out_1, pin_led_out_2, 
            pin_lcd_reset, pin_lcd_rs, pin_lcd_csb, pin_lcd_rw, pin_lcd_strb, 
            pin_lcd_psb, pin_lcd_d_0, pin_lcd_d_1, pin_lcd_d_2, pin_lcd_d_3, 
            pin_lcd_d_4, pin_lcd_d_5, pin_lcd_d_6, pin_lcd_d_7, pin_ta_1, 
            pin_ta_2, pin_enc_a, pin_enc_b, pin_out_1, pin_out_2, 
            pin_in_1, pin_in_2, pin_tp_1);   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(15[8:19])
    inout pin_sda1;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(23[3:11])
    input pin_scl1;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(24[3:11])
    input pin_gpio_08_cs;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(25[3:17])
    input pin_spi_0_sclk;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(26[3:17])
    output pin_spi_0_miso;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(27[3:17])
    input pin_spi_0_mosi;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(28[3:17])
    input pin_gpio_04;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(29[3:14])
    input pin_gpio_05;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(38[3:14])
    output pin_gpio_06;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(39[3:14])
    output pin_gpio_26;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(40[3:14])
    output pin_led_1;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(41[3:12])
    output pin_led_2;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(42[3:12])
    output pin_led_3;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(43[3:12])
    output pin_led_4;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(44[3:12])
    output pin_led_5;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(45[3:12])
    output pin_led_6;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(46[3:12])
    output pin_led_7;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(47[3:12])
    output pin_led_8;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(48[3:12])
    output pin_led_in_1;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(49[3:15])
    output pin_led_in_2;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(50[3:15])
    output pin_led_out_1;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(51[3:16])
    output pin_led_out_2;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(52[3:16])
    output pin_lcd_reset;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(53[3:16])
    output pin_lcd_rs;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(54[3:13])
    output pin_lcd_csb;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(55[3:14])
    output pin_lcd_rw;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(56[3:13])
    output pin_lcd_strb;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(57[3:15])
    output pin_lcd_psb;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(58[3:14])
    output pin_lcd_d_0;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(59[3:14])
    output pin_lcd_d_1;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(60[3:14])
    output pin_lcd_d_2;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(61[3:14])
    output pin_lcd_d_3;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(62[3:14])
    output pin_lcd_d_4;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(63[3:14])
    output pin_lcd_d_5;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(64[3:14])
    output pin_lcd_d_6;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(65[3:14])
    output pin_lcd_d_7;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(66[3:14])
    input pin_ta_1;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(67[3:11])
    input pin_ta_2;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(68[3:11])
    input pin_enc_a;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(69[3:12])
    input pin_enc_b;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(70[3:12])
    output pin_out_1;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(71[3:12])
    output pin_out_2;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(72[3:12])
    input pin_in_1;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(79[3:11])
    input pin_in_2;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(80[3:11])
    output pin_tp_1;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(85[3:11])
    
    wire pin_spi_0_sclk_c /* synthesis is_clock=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(26[3:17])
    wire pin_gpio_04_c /* synthesis is_clock=1, SET_AS_NETWORK=pin_gpio_04_c */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(29[3:14])
    wire clk_mst /* synthesis is_clock=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(206[9:16])
    wire clk_int /* synthesis SET_AS_NETWORK=clk_int, is_clock=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(207[9:16])
    wire [31:0]led_timer /* synthesis is_clock=1, SET_AS_NETWORK=led_timer[10] */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(208[9:18])
    wire i2c_done /* synthesis is_clock=1, SET_AS_NETWORK=i2c_done */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(219[9:17])
    wire var_clk /* synthesis SET_AS_NETWORK=var_clk, is_clock=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(233[9:16])
    wire clk_N_111 /* synthesis is_inv_clock=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(78[8:16])
    
    wire GND_net, VCC_net, pin_scl1_c, pin_spi_0_mosi_c_0, pin_gpio_05_c, 
        pin_gpio_06_c_5, pin_gpio_26_c_6, pin_led_1_c, pin_led_2_c, 
        pin_led_3_c, pin_led_4_c, pin_led_5_c, pin_led_6_c, pin_led_7_c, 
        pin_led_8_c, pin_led_in_1_c_c, pin_led_in_2_c_c, pin_led_out_1_c, 
        pin_led_out_2_c, pin_lcd_rs_c, pin_lcd_strb_c, pin_lcd_d_0_c_0, 
        pin_lcd_d_1_c_1, pin_lcd_d_2_c_2, pin_lcd_d_3_c_3, pin_lcd_d_4_c_4, 
        pin_lcd_d_5_c_5, pin_lcd_d_6_c_6, pin_lcd_d_7_c_7, pin_ta_1_c_0, 
        pin_ta_2_c, pin_enc_a_c, pin_out_1_c, pin_out_2_c_0, pin_tp_1_c;
    wire [7:0]led_output;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(209[9:19])
    wire [7:0]mode_reg2;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(212[9:18])
    wire [7:0]data_reg3;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(213[9:18])
    wire [7:0]data_reg4;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(214[9:18])
    wire [7:0]enc_counter;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(216[9:20])
    wire [7:0]regAddr;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(217[9:16])
    wire [7:0]data;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(218[9:13])
    wire [15:0]countdown;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(231[10:19])
    
    wire pin_out_1_N_79, n4431, n70, pin_tp_1_N_80, n4723, sclDeb;
    wire [5:0]sclDelayed;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(94[24:34])
    wire [1:0]sdaDelayed;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(95[24:34])
    
    wire clearStartStopDet, sdaOut, n101, startEdgeDet, n83, n4430, 
        n76, n89, n81, n80, n96, n95, n94, n75, n100, n4429, 
        n98, n99, n97, n90, n88, n4039, n22, n4052, n4428, 
        n4719;
    wire [2:0]state_2__N_492;
    
    wire n4051, n4030, led_timer_3_enable_56, n82, n4427, n79, n4028, 
        n4032, n4027, n4426, n18, n17, n4038, n4050, n4425, 
        n103, n4424, n31, n4435, n4423, n4422, n4049, n4434, 
        next_writeEn, n4421, n4433, n4034, n4048, n4420, n84, 
        n4432, n93, n92, n91, n4369, n4419, n102, n4359, n74, 
        n73, n4418, n78, n4417, n4416, n2104, n4415, led_timer_3_enable_70, 
        n72, n71, n501, n504, n508, n4414, n85, n1218, n4026, 
        n4413, n77, n4412, n4406, n4035, n4031, n4411, n4410, 
        n4409, n4408, n4, n4407, n130, n131, n132, n133, n134, 
        n135, n136, n137, n138, n139, n140, n141, n142, n143, 
        n144, n145, n146, n147, n148, n149, n150, n151, n152, 
        n153, n154, n155, n156, n157, n158, n159, n160, n4720, 
        n4037, n4852, n4868, n4_adj_577, n4047, n4033, pin_sda1_out, 
        n4025, n4036, n4046, n30, n4045, n4029, n28, n4725, 
        n4724, n26;
    
    VHI i2 (.Z(VCC_net));
    INV i3776 (.A(led_timer[3]), .Z(clk_N_111));
    CCU2D led_timer_31__70_797_add_4_27 (.A0(led_timer[26]), .B0(GND_net), 
          .C0(GND_net), .D0(GND_net), .A1(led_timer[27]), .B1(GND_net), 
          .C1(GND_net), .D1(GND_net), .CIN(n4037), .COUT(n4038), .S0(n135), 
          .S1(n134));   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797_add_4_27.INIT0 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_27.INIT1 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_27.INJECT1_0 = "NO";
    defparam led_timer_31__70_797_add_4_27.INJECT1_1 = "NO";
    FD1S3AX led_timer_31__70_797__i30 (.D(n131), .CK(clk_int), .Q(led_timer[30])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i30.GSR = "ENABLED";
    PFUMX i3501 (.BLUT(n4408), .ALUT(n4409), .C0(data_reg3[1]), .Z(n4423));
    CCU2D led_timer_31__70_797_add_4_11 (.A0(led_timer[10]), .B0(GND_net), 
          .C0(GND_net), .D0(GND_net), .A1(led_timer[11]), .B1(GND_net), 
          .C1(GND_net), .D1(GND_net), .CIN(n4029), .COUT(n4030), .S0(n151), 
          .S1(n150));   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797_add_4_11.INIT0 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_11.INIT1 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_11.INJECT1_0 = "NO";
    defparam led_timer_31__70_797_add_4_11.INJECT1_1 = "NO";
    LUT4 i1_4_lut (.A(clearStartStopDet), .B(sclDeb), .C(sdaDelayed[0]), 
         .D(sdaDelayed[1]), .Z(led_timer_3_enable_56)) /* synthesis lut_function=(A+!((C (D)+!C !(D))+!B)) */ ;
    defparam i1_4_lut.init = 16'haeea;
    LUT4 i15_4_lut (.A(n17), .B(n30), .C(n26), .D(n18), .Z(n31)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(279[7:30])
    defparam i15_4_lut.init = 16'hfffe;
    FD1S3AX toggle_102 (.D(pin_tp_1_N_80), .CK(var_clk), .Q(pin_tp_1_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(278[3] 286[10])
    defparam toggle_102.GSR = "ENABLED";
    OSCH internal_oscillator_inst (.STDBY(GND_net), .OSC(clk_mst)) /* synthesis syn_black_box=true, syn_instantiated=1 */ ;
    defparam internal_oscillator_inst.NOM_FREQ = "133.00";
    i2cSlave i2cSlave_inst (.\led_timer[3] (led_timer[3]), .pin_sda1_out(pin_sda1_out), 
            .pin_scl1_c(pin_scl1_c), .sclDeb(sclDeb), .sdaDelayed({sdaDelayed}), 
            .startEdgeDet(startEdgeDet), .regAddr({regAddr}), .i2c_done(i2c_done), 
            .clk_N_111(clk_N_111), .data({data}), .next_writeEn(next_writeEn), 
            .\led_timer[3]_enable_56 (led_timer_3_enable_56), .clearStartStopDet(clearStartStopDet), 
            .\sclDelayed[5] (sclDelayed[5]), .n4724(n4724), .n4868(n4868), 
            .mode_reg2({mode_reg2}), .data_reg4({data_reg4}), .\data_reg3[0] (data_reg3[0]), 
            .pin_gpio_26_c_6(pin_gpio_26_c_6), .enc_counter({enc_counter}), 
            .\led_output[4] (led_output[4]), .\led_output[7] (led_output[7]), 
            .pin_gpio_06_c_5(pin_gpio_06_c_5), .\data_reg3[1] (data_reg3[1]), 
            .\data_reg3[4] (data_reg3[4]), .\data_reg3[3] (data_reg3[3]), 
            .\data_reg3[2] (data_reg3[2]), .pin_ta_1_c_0(pin_ta_1_c_0), 
            .pin_ta_2_c(pin_ta_2_c), .n501(n501), .n4720(n4720), .n508(n508), 
            .n4(n4_adj_577), .\led_timer[3]_enable_70 (led_timer_3_enable_70), 
            .n504(n504), .n4723(n4723), .n4359(n4359), .n4725(n4725), 
            .GND_net(GND_net), .n1218(n1218), .n2104(n2104), .n4719(n4719), 
            .sdaOut(sdaOut)) /* synthesis syn_module_defined=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(353[16:24])
    FD1S3AX output_clk_69 (.D(pin_out_1_N_79), .CK(pin_gpio_04_c), .Q(pin_out_1_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(247[3] 249[10])
    defparam output_clk_69.GSR = "ENABLED";
    FD1S3AX led_timer_31__70_797__i29 (.D(n132), .CK(clk_int), .Q(led_timer[29])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i29.GSR = "ENABLED";
    CCU2D countdown_798_add_4_9 (.A0(countdown[7]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(countdown[8]), .B1(GND_net), .C1(GND_net), 
          .D1(GND_net), .CIN(n4048), .COUT(n4049), .S0(n78), .S1(n77));   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798_add_4_9.INIT0 = 16'h0555;
    defparam countdown_798_add_4_9.INIT1 = 16'h0555;
    defparam countdown_798_add_4_9.INJECT1_0 = "NO";
    defparam countdown_798_add_4_9.INJECT1_1 = "NO";
    LUT4 countdown_798_mux_6_i3_3_lut (.A(data_reg4[2]), .B(n83), .C(n31), 
         .Z(n101)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798_mux_6_i3_3_lut.init = 16'hcaca;
    FD1S3AX countdown_798__i0 (.D(n103), .CK(var_clk), .Q(countdown[0])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798__i0.GSR = "ENABLED";
    LUT4 countdown_798_mux_6_i2_3_lut (.A(data_reg4[1]), .B(n84), .C(n31), 
         .Z(n102)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798_mux_6_i2_3_lut.init = 16'hcaca;
    LUT4 i843_4_lut (.A(n2104), .B(startEdgeDet), .C(n4359), .D(n4_adj_577), 
         .Z(led_timer_3_enable_70)) /* synthesis lut_function=(A (B)+!A (B+!((D)+!C))) */ ;
    defparam i843_4_lut.init = 16'hccdc;
    CCU2D led_timer_31__70_797_add_4_5 (.A0(led_timer[4]), .B0(GND_net), 
          .C0(GND_net), .D0(GND_net), .A1(led_timer[5]), .B1(GND_net), 
          .C1(GND_net), .D1(GND_net), .CIN(n4026), .COUT(n4027), .S0(n157), 
          .S1(n156));   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797_add_4_5.INIT0 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_5.INIT1 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_5.INJECT1_0 = "NO";
    defparam led_timer_31__70_797_add_4_5.INJECT1_1 = "NO";
    FD1S3AX led_timer_31__70_797__i28 (.D(n133), .CK(clk_int), .Q(led_timer[28])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i28.GSR = "ENABLED";
    PFUMX i3502 (.BLUT(n4410), .ALUT(n4411), .C0(data_reg3[1]), .Z(n4424));
    FD1S3AX led_timer_31__70_797__i27 (.D(n134), .CK(clk_int), .Q(led_timer[27])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i27.GSR = "ENABLED";
    drehimpulsgeber enc_counter_7__I_0 (.pin_enc_a_c(pin_enc_a_c), .\state_2__N_492[1] (state_2__N_492[1]), 
            .\led_timer[5] (led_timer[5]), .enc_counter({enc_counter}), 
            .GND_net(GND_net));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(372[23:38])
    CCU2D countdown_798_add_4_7 (.A0(countdown[5]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(countdown[6]), .B1(GND_net), .C1(GND_net), 
          .D1(GND_net), .CIN(n4047), .COUT(n4048), .S0(n80), .S1(n79));   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798_add_4_7.INIT0 = 16'h0555;
    defparam countdown_798_add_4_7.INIT1 = 16'h0555;
    defparam countdown_798_add_4_7.INJECT1_0 = "NO";
    defparam countdown_798_add_4_7.INJECT1_1 = "NO";
    OB pin_gpio_06_pad (.I(pin_gpio_06_c_5), .O(pin_gpio_06));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(39[3:14])
    LUT4 i1_2_lut (.A(countdown[0]), .B(countdown[6]), .Z(n17)) /* synthesis lut_function=(A+(B)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(279[7:30])
    defparam i1_2_lut.init = 16'heeee;
    FD1S3AX led_timer_31__70_797__i26 (.D(n135), .CK(clk_int), .Q(led_timer[26])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i26.GSR = "ENABLED";
    FD1S3AX led_timer_31__70_797__i25 (.D(n136), .CK(clk_int), .Q(led_timer[25])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i25.GSR = "ENABLED";
    PFUMX i3505 (.BLUT(n4416), .ALUT(n4417), .C0(data_reg3[1]), .Z(n4427));
    LUT4 i14_4_lut (.A(countdown[10]), .B(n28), .C(n22), .D(countdown[12]), 
         .Z(n30)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(279[7:30])
    defparam i14_4_lut.init = 16'hfffe;
    FD1S3AX led_timer_31__70_797__i24 (.D(n137), .CK(clk_int), .Q(led_timer[24])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i24.GSR = "ENABLED";
    OB pin_gpio_26_pad (.I(pin_gpio_26_c_6), .O(pin_gpio_26));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(40[3:14])
    L6MUX21 i3512 (.D0(n4430), .D1(n4431), .SD(data_reg3[3]), .Z(n4434));
    OB pin_spi_0_miso_pad (.I(GND_net), .O(pin_spi_0_miso));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(27[3:17])
    FD1S3AX led_timer_31__70_797__i1 (.D(n160), .CK(clk_int), .Q(led_timer[1])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i1.GSR = "ENABLED";
    L6MUX21 i3513 (.D0(n4432), .D1(n4433), .SD(data_reg3[3]), .Z(n4435));
    FD1S3AX led_timer_31__70_797__i23 (.D(n138), .CK(clk_int), .Q(led_timer[23])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i23.GSR = "ENABLED";
    FD1S3AX led_timer_31__70_797__i22 (.D(n139), .CK(clk_int), .Q(led_timer[22])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i22.GSR = "ENABLED";
    FD1S3AX led_timer_31__70_797__i21 (.D(n140), .CK(clk_int), .Q(led_timer[21])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i21.GSR = "ENABLED";
    CCU2D countdown_798_add_4_5 (.A0(countdown[3]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(countdown[4]), .B1(GND_net), .C1(GND_net), 
          .D1(GND_net), .CIN(n4046), .COUT(n4047), .S0(n82), .S1(n81));   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798_add_4_5.INIT0 = 16'h0555;
    defparam countdown_798_add_4_5.INIT1 = 16'h0555;
    defparam countdown_798_add_4_5.INJECT1_0 = "NO";
    defparam countdown_798_add_4_5.INJECT1_1 = "NO";
    BB pin_sda1_pad (.I(GND_net), .T(sdaOut), .B(pin_sda1), .O(pin_sda1_out));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(108[8:11])
    FD1S3AX led_timer_31__70_797__i20 (.D(n141), .CK(clk_int), .Q(led_timer[20])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i20.GSR = "ENABLED";
    CCU2D led_timer_31__70_797_add_4_15 (.A0(led_timer[14]), .B0(GND_net), 
          .C0(GND_net), .D0(GND_net), .A1(led_timer[15]), .B1(GND_net), 
          .C1(GND_net), .D1(GND_net), .CIN(n4031), .COUT(n4032), .S0(n147), 
          .S1(n146));   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797_add_4_15.INIT0 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_15.INIT1 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_15.INJECT1_0 = "NO";
    defparam led_timer_31__70_797_add_4_15.INJECT1_1 = "NO";
    CCU2D led_timer_31__70_797_add_4_25 (.A0(led_timer[24]), .B0(GND_net), 
          .C0(GND_net), .D0(GND_net), .A1(led_timer[25]), .B1(GND_net), 
          .C1(GND_net), .D1(GND_net), .CIN(n4036), .COUT(n4037), .S0(n137), 
          .S1(n136));   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797_add_4_25.INIT0 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_25.INIT1 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_25.INJECT1_0 = "NO";
    defparam led_timer_31__70_797_add_4_25.INJECT1_1 = "NO";
    FD1S3AX led_timer_31__70_797__i19 (.D(n142), .CK(clk_int), .Q(led_timer[19])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i19.GSR = "ENABLED";
    FD1S3AX led_timer_31__70_797__i18 (.D(n143), .CK(clk_int), .Q(led_timer[18])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i18.GSR = "ENABLED";
    FD1S3AX led_timer_31__70_797__i17 (.D(n144), .CK(clk_int), .Q(led_timer[17])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i17.GSR = "ENABLED";
    FD1S3AX led_timer_31__70_797__i16 (.D(n145), .CK(clk_int), .Q(led_timer[16])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i16.GSR = "ENABLED";
    PFUMX i3503 (.BLUT(n4412), .ALUT(n4413), .C0(data_reg3[1]), .Z(n4425));
    FD1S3AX led_timer_31__70_797__i15 (.D(n146), .CK(clk_int), .Q(led_timer[15])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i15.GSR = "ENABLED";
    LUT4 i10_4_lut (.A(countdown[8]), .B(countdown[3]), .C(countdown[13]), 
         .D(countdown[5]), .Z(n26)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(279[7:30])
    defparam i10_4_lut.init = 16'hfffe;
    PFUMX i3504 (.BLUT(n4414), .ALUT(n4415), .C0(data_reg3[1]), .Z(n4426));
    FD1S3AX led_timer_31__70_797__i14 (.D(n147), .CK(clk_int), .Q(led_timer[14])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i14.GSR = "ENABLED";
    CCU2D led_timer_31__70_797_add_4_3 (.A0(led_timer[2]), .B0(GND_net), 
          .C0(GND_net), .D0(GND_net), .A1(led_timer[3]), .B1(GND_net), 
          .C1(GND_net), .D1(GND_net), .CIN(n4025), .COUT(n4026), .S0(n159), 
          .S1(n158));   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797_add_4_3.INIT0 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_3.INIT1 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_3.INJECT1_0 = "NO";
    defparam led_timer_31__70_797_add_4_3.INJECT1_1 = "NO";
    FD1S3AX led_timer_31__70_797__i13 (.D(n148), .CK(clk_int), .Q(led_timer[13])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i13.GSR = "ENABLED";
    FD1S3AX led_timer_31__70_797__i12 (.D(n149), .CK(clk_int), .Q(led_timer[12])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i12.GSR = "ENABLED";
    FD1S3AX led_timer_31__70_797__i11 (.D(n150), .CK(clk_int), .Q(led_timer[11])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i11.GSR = "ENABLED";
    CCU2D led_timer_31__70_797_add_4_23 (.A0(led_timer[22]), .B0(GND_net), 
          .C0(GND_net), .D0(GND_net), .A1(led_timer[23]), .B1(GND_net), 
          .C1(GND_net), .D1(GND_net), .CIN(n4035), .COUT(n4036), .S0(n139), 
          .S1(n138));   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797_add_4_23.INIT0 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_23.INIT1 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_23.INJECT1_0 = "NO";
    defparam led_timer_31__70_797_add_4_23.INJECT1_1 = "NO";
    LUT4 i2_2_lut (.A(countdown[1]), .B(countdown[4]), .Z(n18)) /* synthesis lut_function=(A+(B)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(279[7:30])
    defparam i2_2_lut.init = 16'heeee;
    FD1S3AX led_timer_31__70_797__i10 (.D(n151), .CK(clk_int), .Q(led_timer[10])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i10.GSR = "ENABLED";
    LUT4 i3485_3_lut (.A(led_timer[2]), .B(led_timer[3]), .C(data_reg3[0]), 
         .Z(n4407)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i3485_3_lut.init = 16'hcaca;
    FD1S3AX led_timer_31__70_797__i9 (.D(n152), .CK(clk_int), .Q(led_timer[9])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i9.GSR = "ENABLED";
    FD1S3AX led_timer_31__70_797__i8 (.D(n153), .CK(clk_int), .Q(led_timer[8])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i8.GSR = "ENABLED";
    FD1S3AX led_timer_31__70_797__i7 (.D(n154), .CK(clk_int), .Q(led_timer[7])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i7.GSR = "ENABLED";
    FD1S3AX led_timer_31__70_797__i6 (.D(n155), .CK(clk_int), .Q(led_timer[6])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i6.GSR = "ENABLED";
    FD1S3AX led_timer_31__70_797__i5 (.D(n156), .CK(clk_int), .Q(led_timer[5])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i5.GSR = "ENABLED";
    FD1S3AX led_timer_31__70_797__i4 (.D(n157), .CK(clk_int), .Q(led_timer[4])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i4.GSR = "ENABLED";
    FD1S3AX led_timer_31__70_797__i3 (.D(n158), .CK(clk_int), .Q(led_timer[3])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i3.GSR = "ENABLED";
    FD1S3AX led_timer_31__70_797__i2 (.D(n159), .CK(clk_int), .Q(led_timer[2])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i2.GSR = "ENABLED";
    CCU2D led_timer_31__70_797_add_4_21 (.A0(led_timer[20]), .B0(GND_net), 
          .C0(GND_net), .D0(GND_net), .A1(led_timer[21]), .B1(GND_net), 
          .C1(GND_net), .D1(GND_net), .CIN(n4034), .COUT(n4035), .S0(n141), 
          .S1(n140));   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797_add_4_21.INIT0 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_21.INIT1 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_21.INJECT1_0 = "NO";
    defparam led_timer_31__70_797_add_4_21.INJECT1_1 = "NO";
    CCU2D countdown_798_add_4_3 (.A0(countdown[1]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(countdown[2]), .B1(GND_net), .C1(GND_net), 
          .D1(GND_net), .CIN(n4045), .COUT(n4046), .S0(n84), .S1(n83));   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798_add_4_3.INIT0 = 16'h0555;
    defparam countdown_798_add_4_3.INIT1 = 16'h0555;
    defparam countdown_798_add_4_3.INJECT1_0 = "NO";
    defparam countdown_798_add_4_3.INJECT1_1 = "NO";
    CCU2D countdown_798_add_4_1 (.A0(GND_net), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(countdown[0]), .B1(GND_net), .C1(GND_net), 
          .D1(GND_net), .COUT(n4045), .S1(n85));   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798_add_4_1.INIT0 = 16'hF000;
    defparam countdown_798_add_4_1.INIT1 = 16'h0555;
    defparam countdown_798_add_4_1.INJECT1_0 = "NO";
    defparam countdown_798_add_4_1.INJECT1_1 = "NO";
    LUT4 led_timer_29__I_0_1_lut (.A(led_timer[29]), .Z(pin_led_1_c)) /* synthesis lut_function=(!(A)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(289[15:32])
    defparam led_timer_29__I_0_1_lut.init = 16'h5555;
    CCU2D led_timer_31__70_797_add_4_19 (.A0(led_timer[18]), .B0(GND_net), 
          .C0(GND_net), .D0(GND_net), .A1(led_timer[19]), .B1(GND_net), 
          .C1(GND_net), .D1(GND_net), .CIN(n4033), .COUT(n4034), .S0(n143), 
          .S1(n142));   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797_add_4_19.INIT0 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_19.INIT1 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_19.INJECT1_0 = "NO";
    defparam led_timer_31__70_797_add_4_19.INJECT1_1 = "NO";
    CCU2D led_timer_31__70_797_add_4_7 (.A0(led_timer[6]), .B0(GND_net), 
          .C0(GND_net), .D0(GND_net), .A1(led_timer[7]), .B1(GND_net), 
          .C1(GND_net), .D1(GND_net), .CIN(n4027), .COUT(n4028), .S0(n155), 
          .S1(n154));   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797_add_4_7.INIT0 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_7.INIT1 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_7.INJECT1_0 = "NO";
    defparam led_timer_31__70_797_add_4_7.INJECT1_1 = "NO";
    LUT4 led_timer_28__I_0_1_lut (.A(led_timer[28]), .Z(pin_led_2_c)) /* synthesis lut_function=(!(A)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(290[15:32])
    defparam led_timer_28__I_0_1_lut.init = 16'h5555;
    LUT4 i3499_3_lut (.A(led_timer[30]), .B(led_timer[31]), .C(data_reg3[0]), 
         .Z(n4421)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i3499_3_lut.init = 16'hcaca;
    LUT4 i1_2_lut_rep_43 (.A(n508), .B(n501), .Z(n4723)) /* synthesis lut_function=(A+(B)) */ ;
    defparam i1_2_lut_rep_43.init = 16'heeee;
    LUT4 i1_2_lut_rep_39_3_lut (.A(n508), .B(n501), .C(next_writeEn), 
         .Z(n4719)) /* synthesis lut_function=(A+(B+(C))) */ ;
    defparam i1_2_lut_rep_39_3_lut.init = 16'hfefe;
    LUT4 led_timer_27__I_0_106_1_lut (.A(led_timer[27]), .Z(pin_led_3_c)) /* synthesis lut_function=(!(A)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(291[15:32])
    defparam led_timer_27__I_0_106_1_lut.init = 16'h5555;
    LUT4 led_timer_26__I_0_105_1_lut (.A(led_timer[26]), .Z(pin_led_4_c)) /* synthesis lut_function=(!(A)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(292[15:32])
    defparam led_timer_26__I_0_105_1_lut.init = 16'h5555;
    LUT4 led_output_4__I_0_1_lut (.A(led_output[4]), .Z(pin_led_5_c)) /* synthesis lut_function=(!(A)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(293[15:32])
    defparam led_output_4__I_0_1_lut.init = 16'h5555;
    LUT4 pin_gpio_26_I_0_1_lut (.A(pin_gpio_26_c_6), .Z(pin_led_6_c)) /* synthesis lut_function=(!(A)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(294[15:32])
    defparam pin_gpio_26_I_0_1_lut.init = 16'h5555;
    LUT4 i3495_3_lut (.A(led_timer[22]), .B(led_timer[23]), .C(data_reg3[0]), 
         .Z(n4417)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i3495_3_lut.init = 16'hcaca;
    LUT4 i3498_3_lut (.A(led_timer[28]), .B(led_timer[29]), .C(data_reg3[0]), 
         .Z(n4420)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i3498_3_lut.init = 16'hcaca;
    FD1S3AX countdown_798__i15 (.D(n88), .CK(var_clk), .Q(countdown[15])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798__i15.GSR = "ENABLED";
    FD1S3AX countdown_798__i14 (.D(n89), .CK(var_clk), .Q(countdown[14])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798__i14.GSR = "ENABLED";
    FD1S3AX countdown_798__i13 (.D(n90), .CK(var_clk), .Q(countdown[13])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798__i13.GSR = "ENABLED";
    FD1S3AX countdown_798__i12 (.D(n91), .CK(var_clk), .Q(countdown[12])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798__i12.GSR = "ENABLED";
    FD1S3AX countdown_798__i11 (.D(n92), .CK(var_clk), .Q(countdown[11])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798__i11.GSR = "ENABLED";
    FD1S3AX countdown_798__i10 (.D(n93), .CK(var_clk), .Q(countdown[10])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798__i10.GSR = "ENABLED";
    FD1S3AX countdown_798__i9 (.D(n94), .CK(var_clk), .Q(countdown[9])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798__i9.GSR = "ENABLED";
    FD1S3AX countdown_798__i8 (.D(n95), .CK(var_clk), .Q(countdown[8])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798__i8.GSR = "ENABLED";
    FD1S3AX countdown_798__i7 (.D(n96), .CK(var_clk), .Q(countdown[7])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798__i7.GSR = "ENABLED";
    FD1S3AX countdown_798__i6 (.D(n97), .CK(var_clk), .Q(countdown[6])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798__i6.GSR = "ENABLED";
    FD1S3AX countdown_798__i5 (.D(n98), .CK(var_clk), .Q(countdown[5])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798__i5.GSR = "ENABLED";
    FD1S3AX countdown_798__i4 (.D(n99), .CK(var_clk), .Q(countdown[4])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798__i4.GSR = "ENABLED";
    FD1S3AX countdown_798__i3 (.D(n100), .CK(var_clk), .Q(countdown[3])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798__i3.GSR = "ENABLED";
    FD1S3AX countdown_798__i2 (.D(n101), .CK(var_clk), .Q(countdown[2])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798__i2.GSR = "ENABLED";
    FD1S3AX countdown_798__i1 (.D(n102), .CK(var_clk), .Q(countdown[1])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798__i1.GSR = "ENABLED";
    LUT4 pin_gpio_06_I_0_1_lut (.A(pin_gpio_06_c_5), .Z(pin_led_7_c)) /* synthesis lut_function=(!(A)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(295[15:32])
    defparam pin_gpio_06_I_0_1_lut.init = 16'h5555;
    OB pin_led_1_pad (.I(pin_led_1_c), .O(pin_led_1));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(41[3:12])
    OB pin_led_2_pad (.I(pin_led_2_c), .O(pin_led_2));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(42[3:12])
    OB pin_led_3_pad (.I(pin_led_3_c), .O(pin_led_3));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(43[3:12])
    OB pin_led_4_pad (.I(pin_led_4_c), .O(pin_led_4));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(44[3:12])
    OB pin_led_5_pad (.I(pin_led_5_c), .O(pin_led_5));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(45[3:12])
    OB pin_led_6_pad (.I(pin_led_6_c), .O(pin_led_6));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(46[3:12])
    OB pin_led_7_pad (.I(pin_led_7_c), .O(pin_led_7));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(47[3:12])
    OB pin_led_8_pad (.I(pin_led_8_c), .O(pin_led_8));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(48[3:12])
    OB pin_led_in_1_pad (.I(pin_led_in_1_c_c), .O(pin_led_in_1));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(49[3:15])
    OB pin_led_in_2_pad (.I(pin_led_in_2_c_c), .O(pin_led_in_2));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(50[3:15])
    OB pin_led_out_1_pad (.I(pin_led_out_1_c), .O(pin_led_out_1));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(51[3:16])
    OB pin_led_out_2_pad (.I(pin_led_out_2_c), .O(pin_led_out_2));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(52[3:16])
    OB pin_lcd_reset_pad (.I(VCC_net), .O(pin_lcd_reset));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(53[3:16])
    OB pin_lcd_rs_pad (.I(pin_lcd_rs_c), .O(pin_lcd_rs));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(54[3:13])
    OB pin_lcd_csb_pad (.I(GND_net), .O(pin_lcd_csb));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(55[3:14])
    OB pin_lcd_rw_pad (.I(GND_net), .O(pin_lcd_rw));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(56[3:13])
    OB pin_lcd_strb_pad (.I(pin_lcd_strb_c), .O(pin_lcd_strb));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(57[3:15])
    OB pin_lcd_psb_pad (.I(VCC_net), .O(pin_lcd_psb));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(58[3:14])
    OB pin_lcd_d_0_pad (.I(pin_lcd_d_0_c_0), .O(pin_lcd_d_0));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(59[3:14])
    OB pin_lcd_d_1_pad (.I(pin_lcd_d_1_c_1), .O(pin_lcd_d_1));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(60[3:14])
    OB pin_lcd_d_2_pad (.I(pin_lcd_d_2_c_2), .O(pin_lcd_d_2));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(61[3:14])
    OB pin_lcd_d_3_pad (.I(pin_lcd_d_3_c_3), .O(pin_lcd_d_3));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(62[3:14])
    OB pin_lcd_d_4_pad (.I(pin_lcd_d_4_c_4), .O(pin_lcd_d_4));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(63[3:14])
    OB pin_lcd_d_5_pad (.I(pin_lcd_d_5_c_5), .O(pin_lcd_d_5));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(64[3:14])
    OB pin_lcd_d_6_pad (.I(pin_lcd_d_6_c_6), .O(pin_lcd_d_6));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(65[3:14])
    OB pin_lcd_d_7_pad (.I(pin_lcd_d_7_c_7), .O(pin_lcd_d_7));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(66[3:14])
    OB pin_out_1_pad (.I(pin_out_1_c), .O(pin_out_1));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(71[3:12])
    OB pin_out_2_pad (.I(pin_out_2_c_0), .O(pin_out_2));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(72[3:12])
    OB pin_tp_1_pad (.I(pin_tp_1_c), .O(pin_tp_1));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(85[3:11])
    IB pin_scl1_pad (.I(pin_scl1), .O(pin_scl1_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(24[3:11])
    IB pin_spi_0_sclk_pad (.I(pin_spi_0_sclk), .O(pin_spi_0_sclk_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(26[3:17])
    IB pin_spi_0_mosi_pad (.I(pin_spi_0_mosi), .O(pin_spi_0_mosi_c_0));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(28[3:17])
    IB pin_gpio_04_pad (.I(pin_gpio_04), .O(pin_gpio_04_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(29[3:14])
    IB pin_gpio_05_pad (.I(pin_gpio_05), .O(pin_gpio_05_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(38[3:14])
    IB pin_ta_1_pad (.I(pin_ta_1), .O(pin_ta_1_c_0));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(67[3:11])
    IB pin_ta_2_pad (.I(pin_ta_2), .O(pin_ta_2_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(68[3:11])
    IB pin_enc_a_pad (.I(pin_enc_a), .O(pin_enc_a_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(69[3:12])
    IB pin_enc_b_pad (.I(pin_enc_b), .O(state_2__N_492[1]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(70[3:12])
    IB pin_led_in_1_c_pad (.I(pin_in_1), .O(pin_led_in_1_c_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(79[3:11])
    IB pin_led_in_2_c_pad (.I(pin_in_2), .O(pin_led_in_2_c_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(80[3:11])
    CCU2D led_timer_31__70_797_add_4_1 (.A0(GND_net), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(led_timer[1]), .B1(GND_net), .C1(GND_net), 
          .D1(GND_net), .COUT(n4025), .S1(n160));   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797_add_4_1.INIT0 = 16'hF000;
    defparam led_timer_31__70_797_add_4_1.INIT1 = 16'h0555;
    defparam led_timer_31__70_797_add_4_1.INJECT1_0 = "NO";
    defparam led_timer_31__70_797_add_4_1.INJECT1_1 = "NO";
    LUT4 led_output_7__I_0_1_lut (.A(led_output[7]), .Z(pin_led_8_c)) /* synthesis lut_function=(!(A)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(296[15:32])
    defparam led_output_7__I_0_1_lut.init = 16'h5555;
    LUT4 i1_4_lut_adj_47 (.A(n4868), .B(n4724), .C(n4369), .D(n4), .Z(n1218)) /* synthesis lut_function=(!(A+!(B (C)+!B (C+(D))))) */ ;
    defparam i1_4_lut_adj_47.init = 16'h5150;
    LUT4 i10_4_lut_adj_48 (.A(next_writeEn), .B(n4725), .C(n504), .D(n4720), 
         .Z(n4369)) /* synthesis lut_function=(A (C+!(D))+!A (B (C))) */ ;
    defparam i10_4_lut_adj_48.init = 16'he0ea;
    LUT4 i1_2_lut_adj_49 (.A(n501), .B(sclDelayed[5]), .Z(n4)) /* synthesis lut_function=(!((B)+!A)) */ ;
    defparam i1_2_lut_adj_49.init = 16'h2222;
    LUT4 led_timer_26__I_0_2_lut (.A(led_timer[26]), .B(pin_ta_1_c_0), .Z(pin_led_out_1_c)) /* synthesis lut_function=(A (B)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(301[19:45])
    defparam led_timer_26__I_0_2_lut.init = 16'h8888;
    LUT4 i3626_2_lut (.A(led_timer[27]), .B(pin_ta_2_c), .Z(pin_led_out_2_c)) /* synthesis lut_function=(!(A (B))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(302[19:46])
    defparam i3626_2_lut.init = 16'h7777;
    VLO i1 (.Z(GND_net));
    LUT4 i3487_3_lut (.A(led_timer[6]), .B(led_timer[7]), .C(data_reg3[0]), 
         .Z(n4409)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i3487_3_lut.init = 16'hcaca;
    LUT4 i12_4_lut (.A(countdown[11]), .B(countdown[9]), .C(countdown[14]), 
         .D(countdown[15]), .Z(n28)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(279[7:30])
    defparam i12_4_lut.init = 16'hfffe;
    LUT4 countdown_798_mux_6_i1_3_lut (.A(data_reg4[0]), .B(n85), .C(n31), 
         .Z(n103)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798_mux_6_i1_3_lut.init = 16'hcaca;
    PFUMX i3507 (.BLUT(n4420), .ALUT(n4421), .C0(data_reg3[1]), .Z(n4429));
    L6MUX21 i3510 (.D0(n4426), .D1(n4427), .SD(data_reg3[2]), .Z(n4432));
    L6MUX21 i3511 (.D0(n4428), .D1(n4429), .SD(data_reg3[2]), .Z(n4433));
    LUT4 i6_2_lut (.A(countdown[2]), .B(countdown[7]), .Z(n22)) /* synthesis lut_function=(A+(B)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(279[7:30])
    defparam i6_2_lut.init = 16'heeee;
    LUT4 pin_out_1_I_0_1_lut (.A(pin_out_1_c), .Z(pin_out_1_N_79)) /* synthesis lut_function=(!(A)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(248[18:32])
    defparam pin_out_1_I_0_1_lut.init = 16'h5555;
    LUT4 i3492_3_lut (.A(led_timer[16]), .B(led_timer[17]), .C(data_reg3[0]), 
         .Z(n4414)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i3492_3_lut.init = 16'hcaca;
    LUT4 i3491_3_lut (.A(led_timer[14]), .B(led_timer[15]), .C(data_reg3[0]), 
         .Z(n4413)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i3491_3_lut.init = 16'hcaca;
    LUT4 i3490_3_lut (.A(led_timer[12]), .B(led_timer[13]), .C(data_reg3[0]), 
         .Z(n4412)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i3490_3_lut.init = 16'hcaca;
    LUT4 i3497_3_lut (.A(led_timer[26]), .B(led_timer[27]), .C(data_reg3[0]), 
         .Z(n4419)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i3497_3_lut.init = 16'hcaca;
    TSALL TSALL_INST (.TSALL(GND_net));
    CCU2D led_timer_31__70_797_add_4_17 (.A0(led_timer[16]), .B0(GND_net), 
          .C0(GND_net), .D0(GND_net), .A1(led_timer[17]), .B1(GND_net), 
          .C1(GND_net), .D1(GND_net), .CIN(n4032), .COUT(n4033), .S0(n145), 
          .S1(n144));   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797_add_4_17.INIT0 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_17.INIT1 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_17.INJECT1_0 = "NO";
    defparam led_timer_31__70_797_add_4_17.INJECT1_1 = "NO";
    LUT4 i3488_3_lut (.A(led_timer[8]), .B(led_timer[9]), .C(data_reg3[0]), 
         .Z(n4410)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i3488_3_lut.init = 16'hcaca;
    LUT4 i3494_3_lut (.A(led_timer[20]), .B(led_timer[21]), .C(data_reg3[0]), 
         .Z(n4416)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i3494_3_lut.init = 16'hcaca;
    FD1S3AX led_timer_31__70_797__i31 (.D(n130), .CK(clk_int), .Q(led_timer[31])) /* synthesis syn_use_carry_chain=1 */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797__i31.GSR = "ENABLED";
    L6MUX21 i3508 (.D0(n4422), .D1(n4423), .SD(data_reg3[2]), .Z(n4430));
    LUT4 countdown_798_mux_6_i16_3_lut (.A(mode_reg2[7]), .B(n70), .C(n31), 
         .Z(n88)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798_mux_6_i16_3_lut.init = 16'hcaca;
    LUT4 countdown_798_mux_6_i15_3_lut (.A(mode_reg2[6]), .B(n71), .C(n31), 
         .Z(n89)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798_mux_6_i15_3_lut.init = 16'hcaca;
    GSR GSR_INST (.GSR(VCC_net));
    L6MUX21 i3509 (.D0(n4424), .D1(n4425), .SD(data_reg3[2]), .Z(n4431));
    LUT4 countdown_798_mux_6_i14_3_lut (.A(mode_reg2[5]), .B(n72), .C(n31), 
         .Z(n90)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798_mux_6_i14_3_lut.init = 16'hcaca;
    LUT4 countdown_798_mux_6_i13_3_lut (.A(mode_reg2[4]), .B(n73), .C(n31), 
         .Z(n91)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798_mux_6_i13_3_lut.init = 16'hcaca;
    LUT4 countdown_798_mux_6_i12_3_lut (.A(mode_reg2[3]), .B(n74), .C(n31), 
         .Z(n92)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798_mux_6_i12_3_lut.init = 16'hcaca;
    LUT4 countdown_798_mux_6_i11_3_lut (.A(mode_reg2[2]), .B(n75), .C(n31), 
         .Z(n93)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798_mux_6_i11_3_lut.init = 16'hcaca;
    CCU2D led_timer_31__70_797_add_4_13 (.A0(led_timer[12]), .B0(GND_net), 
          .C0(GND_net), .D0(GND_net), .A1(led_timer[13]), .B1(GND_net), 
          .C1(GND_net), .D1(GND_net), .CIN(n4030), .COUT(n4031), .S0(n149), 
          .S1(n148));   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797_add_4_13.INIT0 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_13.INIT1 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_13.INJECT1_0 = "NO";
    defparam led_timer_31__70_797_add_4_13.INJECT1_1 = "NO";
    LUT4 countdown_798_mux_6_i10_3_lut (.A(mode_reg2[1]), .B(n76), .C(n31), 
         .Z(n94)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798_mux_6_i10_3_lut.init = 16'hcaca;
    LUT4 i3486_3_lut (.A(led_timer[4]), .B(led_timer[5]), .C(data_reg3[0]), 
         .Z(n4408)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i3486_3_lut.init = 16'hcaca;
    LUT4 m1_lut (.Z(n4852)) /* synthesis lut_function=1, syn_instantiated=1 */ ;
    defparam m1_lut.init = 16'hffff;
    LUT4 i3489_3_lut (.A(led_timer[10]), .B(led_timer[11]), .C(data_reg3[0]), 
         .Z(n4411)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i3489_3_lut.init = 16'hcaca;
    LUT4 countdown_798_mux_6_i9_3_lut (.A(mode_reg2[0]), .B(n77), .C(n31), 
         .Z(n95)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798_mux_6_i9_3_lut.init = 16'hcaca;
    LUT4 i3514_3_lut (.A(n4434), .B(n4435), .C(data_reg3[4]), .Z(var_clk)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i3514_3_lut.init = 16'hcaca;
    CCU2D countdown_798_add_4_17 (.A0(countdown[15]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(GND_net), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n4052), .S0(n70));   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798_add_4_17.INIT0 = 16'h0555;
    defparam countdown_798_add_4_17.INIT1 = 16'h0000;
    defparam countdown_798_add_4_17.INJECT1_0 = "NO";
    defparam countdown_798_add_4_17.INJECT1_1 = "NO";
    LUT4 i1_2_lut_adj_50 (.A(pin_tp_1_c), .B(n31), .Z(pin_tp_1_N_80)) /* synthesis lut_function=(A (B)+!A !(B)) */ ;
    defparam i1_2_lut_adj_50.init = 16'h9999;
    LUT4 countdown_798_mux_6_i8_3_lut (.A(data_reg4[7]), .B(n78), .C(n31), 
         .Z(n96)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798_mux_6_i8_3_lut.init = 16'hcaca;
    LUT4 countdown_798_mux_6_i7_3_lut (.A(data_reg4[6]), .B(n79), .C(n31), 
         .Z(n97)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798_mux_6_i7_3_lut.init = 16'hcaca;
    LUT4 countdown_798_mux_6_i6_3_lut (.A(data_reg4[5]), .B(n80), .C(n31), 
         .Z(n98)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798_mux_6_i6_3_lut.init = 16'hcaca;
    LUT4 countdown_798_mux_6_i5_3_lut (.A(data_reg4[4]), .B(n81), .C(n31), 
         .Z(n99)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798_mux_6_i5_3_lut.init = 16'hcaca;
    LUT4 countdown_798_mux_6_i4_3_lut (.A(data_reg4[3]), .B(n82), .C(n31), 
         .Z(n100)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798_mux_6_i4_3_lut.init = 16'hcaca;
    PUR PUR_INST (.PUR(VCC_net));
    defparam PUR_INST.RST_PULSE = 1;
    pll pll_inst (.clk_mst(clk_mst), .clk_int(clk_int), .GND_net(GND_net)) /* synthesis NGD_DRC_MASK=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(427[11:14])
    CCU2D countdown_798_add_4_15 (.A0(countdown[13]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(countdown[14]), .B1(GND_net), .C1(GND_net), 
          .D1(GND_net), .CIN(n4051), .COUT(n4052), .S0(n72), .S1(n71));   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798_add_4_15.INIT0 = 16'h0555;
    defparam countdown_798_add_4_15.INIT1 = 16'h0555;
    defparam countdown_798_add_4_15.INJECT1_0 = "NO";
    defparam countdown_798_add_4_15.INJECT1_1 = "NO";
    LUT4 i3493_3_lut (.A(led_timer[18]), .B(led_timer[19]), .C(data_reg3[0]), 
         .Z(n4415)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i3493_3_lut.init = 16'hcaca;
    CCU2D led_timer_31__70_797_add_4_9 (.A0(led_timer[8]), .B0(GND_net), 
          .C0(GND_net), .D0(GND_net), .A1(led_timer[9]), .B1(GND_net), 
          .C1(GND_net), .D1(GND_net), .CIN(n4028), .COUT(n4029), .S0(n153), 
          .S1(n152));   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797_add_4_9.INIT0 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_9.INIT1 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_9.INJECT1_0 = "NO";
    defparam led_timer_31__70_797_add_4_9.INJECT1_1 = "NO";
    CCU2D countdown_798_add_4_13 (.A0(countdown[11]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(countdown[12]), .B1(GND_net), .C1(GND_net), 
          .D1(GND_net), .CIN(n4050), .COUT(n4051), .S0(n74), .S1(n73));   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798_add_4_13.INIT0 = 16'h0555;
    defparam countdown_798_add_4_13.INIT1 = 16'h0555;
    defparam countdown_798_add_4_13.INJECT1_0 = "NO";
    defparam countdown_798_add_4_13.INJECT1_1 = "NO";
    CCU2D countdown_798_add_4_11 (.A0(countdown[9]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(countdown[10]), .B1(GND_net), .C1(GND_net), 
          .D1(GND_net), .CIN(n4049), .COUT(n4050), .S0(n76), .S1(n75));   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1308[12:13])
    defparam countdown_798_add_4_11.INIT0 = 16'h0555;
    defparam countdown_798_add_4_11.INIT1 = 16'h0555;
    defparam countdown_798_add_4_11.INJECT1_0 = "NO";
    defparam countdown_798_add_4_11.INJECT1_1 = "NO";
    LCD LCD_inst (.\led_timer[10] (led_timer[10]), .i2c_done(i2c_done), 
        .regAddr({regAddr}), .pin_lcd_d_0_c_0(pin_lcd_d_0_c_0), .data({data}), 
        .pin_lcd_rs_c(pin_lcd_rs_c), .pin_lcd_strb_c(pin_lcd_strb_c), .n4852(n4852), 
        .pin_lcd_d_7_c_7(pin_lcd_d_7_c_7), .pin_lcd_d_6_c_6(pin_lcd_d_6_c_6), 
        .pin_lcd_d_5_c_5(pin_lcd_d_5_c_5), .pin_lcd_d_4_c_4(pin_lcd_d_4_c_4), 
        .pin_lcd_d_3_c_3(pin_lcd_d_3_c_3), .pin_lcd_d_2_c_2(pin_lcd_d_2_c_2), 
        .pin_lcd_d_1_c_1(pin_lcd_d_1_c_1));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(383[11:14])
    CCU2D led_timer_31__70_797_add_4_31 (.A0(led_timer[30]), .B0(GND_net), 
          .C0(GND_net), .D0(GND_net), .A1(led_timer[31]), .B1(GND_net), 
          .C1(GND_net), .D1(GND_net), .CIN(n4039), .S0(n131), .S1(n130));   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797_add_4_31.INIT0 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_31.INIT1 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_31.INJECT1_0 = "NO";
    defparam led_timer_31__70_797_add_4_31.INJECT1_1 = "NO";
    CCU2D led_timer_31__70_797_add_4_29 (.A0(led_timer[28]), .B0(GND_net), 
          .C0(GND_net), .D0(GND_net), .A1(led_timer[29]), .B1(GND_net), 
          .C1(GND_net), .D1(GND_net), .CIN(n4038), .COUT(n4039), .S0(n133), 
          .S1(n132));   // C:/Program Files/Lattice_Diamond/diamond/3.9_x64/ispfpga/vhdl_packages/numeric_std.vhd(1241[12:13])
    defparam led_timer_31__70_797_add_4_29.INIT0 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_29.INIT1 = 16'hfaaa;
    defparam led_timer_31__70_797_add_4_29.INJECT1_0 = "NO";
    defparam led_timer_31__70_797_add_4_29.INJECT1_1 = "NO";
    LUT4 i3484_3_lut (.A(clk_int), .B(led_timer[1]), .C(data_reg3[0]), 
         .Z(n4406)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i3484_3_lut.init = 16'hcaca;
    fifo fifo_inst (.pin_spi_0_mosi_c_0(pin_spi_0_mosi_c_0), .pin_spi_0_sclk_c(pin_spi_0_sclk_c), 
         .pin_out_1_c(pin_out_1_c), .pin_gpio_05_c(pin_gpio_05_c), .VCC_net(VCC_net), 
         .pin_ta_1_c_0(pin_ta_1_c_0), .GND_net(GND_net), .pin_out_2_c_0(pin_out_2_c_0), 
         .\led_output[7] (led_output[7]), .\led_output[4] (led_output[4]), 
         .pin_gpio_06_c_5(pin_gpio_06_c_5), .pin_gpio_26_c_6(pin_gpio_26_c_6)) /* synthesis NGD_DRC_MASK=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(394[12:16])
    PFUMX i3506 (.BLUT(n4418), .ALUT(n4419), .C0(data_reg3[1]), .Z(n4428));
    LUT4 i3496_3_lut (.A(led_timer[24]), .B(led_timer[25]), .C(data_reg3[0]), 
         .Z(n4418)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i3496_3_lut.init = 16'hcaca;
    PFUMX i3500 (.BLUT(n4406), .ALUT(n4407), .C0(data_reg3[1]), .Z(n4422));
    
endmodule
//
// Verilog Description of module i2cSlave
//

module i2cSlave (\led_timer[3] , pin_sda1_out, pin_scl1_c, sclDeb, sdaDelayed, 
            startEdgeDet, regAddr, i2c_done, clk_N_111, data, next_writeEn, 
            \led_timer[3]_enable_56 , clearStartStopDet, \sclDelayed[5] , 
            n4724, n4868, mode_reg2, data_reg4, \data_reg3[0] , pin_gpio_26_c_6, 
            enc_counter, \led_output[4] , \led_output[7] , pin_gpio_06_c_5, 
            \data_reg3[1] , \data_reg3[4] , \data_reg3[3] , \data_reg3[2] , 
            pin_ta_1_c_0, pin_ta_2_c, n501, n4720, n508, n4, \led_timer[3]_enable_70 , 
            n504, n4723, n4359, n4725, GND_net, n1218, n2104, 
            n4719, sdaOut) /* synthesis syn_module_defined=1 */ ;
    input \led_timer[3] ;
    input pin_sda1_out;
    input pin_scl1_c;
    output sclDeb;
    output [1:0]sdaDelayed;
    output startEdgeDet;
    output [7:0]regAddr;
    output i2c_done;
    input clk_N_111;
    output [7:0]data;
    output next_writeEn;
    input \led_timer[3]_enable_56 ;
    output clearStartStopDet;
    output \sclDelayed[5] ;
    output n4724;
    output n4868;
    output [7:0]mode_reg2;
    output [7:0]data_reg4;
    output \data_reg3[0] ;
    input pin_gpio_26_c_6;
    input [7:0]enc_counter;
    input \led_output[4] ;
    input \led_output[7] ;
    input pin_gpio_06_c_5;
    output \data_reg3[1] ;
    output \data_reg3[4] ;
    output \data_reg3[3] ;
    output \data_reg3[2] ;
    input pin_ta_1_c_0;
    input pin_ta_2_c;
    output n501;
    output n4720;
    output n508;
    output n4;
    input \led_timer[3]_enable_70 ;
    output n504;
    input n4723;
    output n4359;
    output n4725;
    input GND_net;
    input n1218;
    output n2104;
    input n4719;
    output sdaOut;
    
    wire \led_timer[3]  /* synthesis SET_AS_NETWORK=led_timer[3], is_clock=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(208[9:18])
    wire writeEn /* synthesis is_clock=1, SET_AS_NETWORK=\i2cSlave_inst/writeEn */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(102[6:13])
    wire i2c_done /* synthesis is_clock=1, SET_AS_NETWORK=i2c_done */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(219[9:17])
    wire clk_N_111 /* synthesis is_inv_clock=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(78[8:16])
    wire [5:0]sdaPipe;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(91[24:31])
    wire [5:0]sclPipe;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(92[24:31])
    
    wire sdaDeb, n2412;
    wire [5:0]sclDelayed;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(94[24:34])
    
    wire n2416, startEdgeDet_N_123, i2c_done_buff;
    wire [7:0]regAddr_adj_576;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(100[12:19])
    wire [7:0]dataToRegIF;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(101[12:23])
    wire [1:0]startStopDetState;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(96[11:28])
    
    wire n4_c, n4728;
    wire [15:0]n492;
    
    wire n540, n36, n4730, n90, n8, n8_adj_572, n2415, n2411, 
        n8_adj_573, n52, n4726, led_timer_3_enable_44, n8_adj_574, 
        n1972;
    wire [7:0]dataFromRegIF;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(103[12:25])
    
    FD1S3AX sdaPipe_i0 (.D(pin_sda1_out), .CK(\led_timer[3] ), .Q(sdaPipe[0])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(122[8] 141[4])
    defparam sdaPipe_i0.GSR = "ENABLED";
    FD1S3AX sclPipe_i0 (.D(pin_scl1_c), .CK(\led_timer[3] ), .Q(sclPipe[0])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(122[8] 141[4])
    defparam sclPipe_i0.GSR = "ENABLED";
    FD1S3AX sdaDeb_68 (.D(n2412), .CK(\led_timer[3] ), .Q(sdaDeb)) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(122[8] 141[4])
    defparam sdaDeb_68.GSR = "ENABLED";
    FD1S3AX sclDelayed_i0 (.D(sclDeb), .CK(\led_timer[3] ), .Q(sclDelayed[0])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(150[8] 159[4])
    defparam sclDelayed_i0.GSR = "ENABLED";
    FD1S3AX sclDeb_70 (.D(n2416), .CK(\led_timer[3] ), .Q(sclDeb)) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(122[8] 141[4])
    defparam sclDeb_70.GSR = "ENABLED";
    FD1S3AX sdaDelayed_i0 (.D(sdaDeb), .CK(\led_timer[3] ), .Q(sdaDelayed[0])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(150[8] 159[4])
    defparam sdaDelayed_i0.GSR = "ENABLED";
    FD1S3AX startEdgeDet_74 (.D(startEdgeDet_N_123), .CK(\led_timer[3] ), 
            .Q(startEdgeDet)) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(162[8] 181[4])
    defparam startEdgeDet_74.GSR = "ENABLED";
    FD1S3AX i2c_done_buff_75 (.D(writeEn), .CK(\led_timer[3] ), .Q(i2c_done_buff)) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(214[8] 217[4])
    defparam i2c_done_buff_75.GSR = "ENABLED";
    FD1S3AX addr_i0 (.D(regAddr_adj_576[0]), .CK(writeEn), .Q(regAddr[0])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(221[8] 224[4])
    defparam addr_i0.GSR = "ENABLED";
    FD1S3AX i2c_done_76 (.D(i2c_done_buff), .CK(clk_N_111), .Q(i2c_done)) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(218[8] 220[4])
    defparam i2c_done_76.GSR = "ENABLED";
    FD1S3AX data_i0 (.D(dataToRegIF[0]), .CK(writeEn), .Q(data[0])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(221[8] 224[4])
    defparam data_i0.GSR = "ENABLED";
    LUT4 i1_2_lut_3_lut (.A(startStopDetState[0]), .B(startStopDetState[1]), 
         .C(next_writeEn), .Z(n4_c)) /* synthesis lut_function=(!(A ((C)+!B)+!A (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(162[8] 181[4])
    defparam i1_2_lut_3_lut.init = 16'h0d0d;
    FD1P3IX startStopDetState__i0 (.D(n4728), .SP(\led_timer[3]_enable_56 ), 
            .CD(clearStartStopDet), .CK(\led_timer[3] ), .Q(startStopDetState[0])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(162[8] 181[4])
    defparam startStopDetState__i0.GSR = "ENABLED";
    LUT4 i1_2_lut_3_lut_adj_42 (.A(sdaDelayed[1]), .B(sdaDelayed[0]), .C(sclDeb), 
         .Z(startEdgeDet_N_123)) /* synthesis lut_function=(!((B+!(C))+!A)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(168[9:99])
    defparam i1_2_lut_3_lut_adj_42.init = 16'h2020;
    LUT4 i1_2_lut (.A(\sclDelayed[5] ), .B(n492[3]), .Z(n540)) /* synthesis lut_function=(A (B)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(150[8] 159[4])
    defparam i1_2_lut.init = 16'h8888;
    LUT4 i1_2_lut_rep_48 (.A(sdaDelayed[1]), .B(sdaDelayed[0]), .Z(n4728)) /* synthesis lut_function=(!((B)+!A)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(168[9:99])
    defparam i1_2_lut_rep_48.init = 16'h2222;
    LUT4 i1_2_lut_rep_44 (.A(startStopDetState[0]), .B(startStopDetState[1]), 
         .Z(n4724)) /* synthesis lut_function=(A+(B)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(162[8] 181[4])
    defparam i1_2_lut_rep_44.init = 16'heeee;
    LUT4 i1_2_lut_3_lut_adj_43 (.A(startStopDetState[0]), .B(startStopDetState[1]), 
         .C(\sclDelayed[5] ), .Z(n36)) /* synthesis lut_function=(A+(B+(C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(162[8] 181[4])
    defparam i1_2_lut_3_lut_adj_43.init = 16'hfefe;
    LUT4 i1_2_lut_rep_50 (.A(startStopDetState[0]), .B(startStopDetState[1]), 
         .Z(n4730)) /* synthesis lut_function=((B)+!A) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(162[8] 181[4])
    defparam i1_2_lut_rep_50.init = 16'hdddd;
    FD1S3AX data_i7 (.D(dataToRegIF[7]), .CK(writeEn), .Q(data[7])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(221[8] 224[4])
    defparam data_i7.GSR = "ENABLED";
    FD1S3AX data_i6 (.D(dataToRegIF[6]), .CK(writeEn), .Q(data[6])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(221[8] 224[4])
    defparam data_i6.GSR = "ENABLED";
    FD1S3AX data_i5 (.D(dataToRegIF[5]), .CK(writeEn), .Q(data[5])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(221[8] 224[4])
    defparam data_i5.GSR = "ENABLED";
    FD1S3AX data_i4 (.D(dataToRegIF[4]), .CK(writeEn), .Q(data[4])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(221[8] 224[4])
    defparam data_i4.GSR = "ENABLED";
    FD1S3AX data_i3 (.D(dataToRegIF[3]), .CK(writeEn), .Q(data[3])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(221[8] 224[4])
    defparam data_i3.GSR = "ENABLED";
    FD1S3AX data_i2 (.D(dataToRegIF[2]), .CK(writeEn), .Q(data[2])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(221[8] 224[4])
    defparam data_i2.GSR = "ENABLED";
    FD1S3AX data_i1 (.D(dataToRegIF[1]), .CK(writeEn), .Q(data[1])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(221[8] 224[4])
    defparam data_i1.GSR = "ENABLED";
    FD1S3AX addr_i7 (.D(regAddr_adj_576[7]), .CK(writeEn), .Q(regAddr[7])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(221[8] 224[4])
    defparam addr_i7.GSR = "ENABLED";
    FD1S3AX addr_i6 (.D(regAddr_adj_576[6]), .CK(writeEn), .Q(regAddr[6])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(221[8] 224[4])
    defparam addr_i6.GSR = "ENABLED";
    FD1S3AX addr_i5 (.D(regAddr_adj_576[5]), .CK(writeEn), .Q(regAddr[5])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(221[8] 224[4])
    defparam addr_i5.GSR = "ENABLED";
    FD1S3AX addr_i4 (.D(regAddr_adj_576[4]), .CK(writeEn), .Q(regAddr[4])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(221[8] 224[4])
    defparam addr_i4.GSR = "ENABLED";
    FD1S3AX addr_i3 (.D(regAddr_adj_576[3]), .CK(writeEn), .Q(regAddr[3])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(221[8] 224[4])
    defparam addr_i3.GSR = "ENABLED";
    FD1S3AX addr_i2 (.D(regAddr_adj_576[2]), .CK(writeEn), .Q(regAddr[2])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(221[8] 224[4])
    defparam addr_i2.GSR = "ENABLED";
    FD1S3AX addr_i1 (.D(regAddr_adj_576[1]), .CK(writeEn), .Q(regAddr[1])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(221[8] 224[4])
    defparam addr_i1.GSR = "ENABLED";
    FD1S3AX sdaDelayed_i1 (.D(sdaDelayed[0]), .CK(\led_timer[3] ), .Q(sdaDelayed[1])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(150[8] 159[4])
    defparam sdaDelayed_i1.GSR = "ENABLED";
    FD1S3AX sclDelayed_i5 (.D(sclDelayed[4]), .CK(\led_timer[3] ), .Q(\sclDelayed[5] )) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(150[8] 159[4])
    defparam sclDelayed_i5.GSR = "ENABLED";
    FD1S3AX sclDelayed_i4 (.D(sclDelayed[3]), .CK(\led_timer[3] ), .Q(sclDelayed[4])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(150[8] 159[4])
    defparam sclDelayed_i4.GSR = "ENABLED";
    FD1S3AX sclDelayed_i3 (.D(sclDelayed[2]), .CK(\led_timer[3] ), .Q(sclDelayed[3])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(150[8] 159[4])
    defparam sclDelayed_i3.GSR = "ENABLED";
    FD1S3AX sclDelayed_i2 (.D(sclDelayed[1]), .CK(\led_timer[3] ), .Q(sclDelayed[2])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(150[8] 159[4])
    defparam sclDelayed_i2.GSR = "ENABLED";
    FD1S3AX sclDelayed_i1 (.D(sclDelayed[0]), .CK(\led_timer[3] ), .Q(sclDelayed[1])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(150[8] 159[4])
    defparam sclDelayed_i1.GSR = "ENABLED";
    FD1S3AX sclPipe_i5 (.D(sclPipe[4]), .CK(\led_timer[3] ), .Q(sclPipe[5])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(122[8] 141[4])
    defparam sclPipe_i5.GSR = "ENABLED";
    FD1S3AX sclPipe_i4 (.D(sclPipe[3]), .CK(\led_timer[3] ), .Q(sclPipe[4])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(122[8] 141[4])
    defparam sclPipe_i4.GSR = "ENABLED";
    FD1S3AX sclPipe_i3 (.D(sclPipe[2]), .CK(\led_timer[3] ), .Q(sclPipe[3])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(122[8] 141[4])
    defparam sclPipe_i3.GSR = "ENABLED";
    FD1S3AX sclPipe_i2 (.D(sclPipe[1]), .CK(\led_timer[3] ), .Q(sclPipe[2])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(122[8] 141[4])
    defparam sclPipe_i2.GSR = "ENABLED";
    FD1S3AX sclPipe_i1 (.D(sclPipe[0]), .CK(\led_timer[3] ), .Q(sclPipe[1])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(122[8] 141[4])
    defparam sclPipe_i1.GSR = "ENABLED";
    FD1S3AX sdaPipe_i5 (.D(sdaPipe[4]), .CK(\led_timer[3] ), .Q(sdaPipe[5])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(122[8] 141[4])
    defparam sdaPipe_i5.GSR = "ENABLED";
    FD1S3AX sdaPipe_i4 (.D(sdaPipe[3]), .CK(\led_timer[3] ), .Q(sdaPipe[4])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(122[8] 141[4])
    defparam sdaPipe_i4.GSR = "ENABLED";
    FD1S3AX sdaPipe_i3 (.D(sdaPipe[2]), .CK(\led_timer[3] ), .Q(sdaPipe[3])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(122[8] 141[4])
    defparam sdaPipe_i3.GSR = "ENABLED";
    FD1S3AX sdaPipe_i2 (.D(sdaPipe[1]), .CK(\led_timer[3] ), .Q(sdaPipe[2])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(122[8] 141[4])
    defparam sdaPipe_i2.GSR = "ENABLED";
    FD1S3AX sdaPipe_i1 (.D(sdaPipe[0]), .CK(\led_timer[3] ), .Q(sdaPipe[1])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(122[8] 141[4])
    defparam sdaPipe_i1.GSR = "ENABLED";
    LUT4 i48_2_lut (.A(sdaDelayed[0]), .B(sdaDelayed[1]), .Z(n90)) /* synthesis lut_function=(!((B)+!A)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(175[11:83])
    defparam i48_2_lut.init = 16'h2222;
    LUT4 i3_3_lut (.A(sclPipe[3]), .B(sclPipe[2]), .C(sclPipe[5]), .Z(n8)) /* synthesis lut_function=(A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(132[9:35])
    defparam i3_3_lut.init = 16'h8080;
    FD1P3IX startStopDetState__i1 (.D(n90), .SP(\led_timer[3]_enable_56 ), 
            .CD(clearStartStopDet), .CK(\led_timer[3] ), .Q(startStopDetState[1])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(162[8] 181[4])
    defparam startStopDetState__i1.GSR = "ENABLED";
    LUT4 i3_3_lut_adj_44 (.A(sclPipe[3]), .B(sclPipe[2]), .C(sclPipe[5]), 
         .Z(n8_adj_572)) /* synthesis lut_function=(A+(B+(C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(134[14:40])
    defparam i3_3_lut_adj_44.init = 16'hfefe;
    LUT4 i1542_4_lut (.A(sclPipe[4]), .B(sclDeb), .C(n8_adj_572), .D(sclPipe[1]), 
         .Z(n2415)) /* synthesis lut_function=(A (B)+!A (B (C+(D)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(122[8] 141[4])
    defparam i1542_4_lut.init = 16'hccc8;
    LUT4 i1539_4_lut (.A(n2411), .B(sdaPipe[4]), .C(n8_adj_573), .D(sdaPipe[1]), 
         .Z(n2412)) /* synthesis lut_function=(A+(B (C (D)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(122[8] 141[4])
    defparam i1539_4_lut.init = 16'heaaa;
    LUT4 i1_4_lut (.A(startEdgeDet), .B(n52), .C(n4726), .D(n492[1]), 
         .Z(led_timer_3_enable_44)) /* synthesis lut_function=(A+(B (C+!(D))+!B (C (D)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(162[8] 181[4])
    defparam i1_4_lut.init = 16'hfaee;
    LUT4 i1538_4_lut (.A(sdaPipe[4]), .B(sdaDeb), .C(n8_adj_574), .D(sdaPipe[1]), 
         .Z(n2411)) /* synthesis lut_function=(A (B)+!A (B (C+(D)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(122[8] 141[4])
    defparam i1538_4_lut.init = 16'hccc8;
    LUT4 i3_3_lut_adj_45 (.A(sdaPipe[3]), .B(sdaPipe[2]), .C(sdaPipe[5]), 
         .Z(n8_adj_573)) /* synthesis lut_function=(A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(136[9:35])
    defparam i3_3_lut_adj_45.init = 16'h8080;
    LUT4 i3_3_lut_adj_46 (.A(sdaPipe[3]), .B(sdaPipe[2]), .C(sdaPipe[5]), 
         .Z(n8_adj_574)) /* synthesis lut_function=(A+(B+(C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(138[14:40])
    defparam i3_3_lut_adj_46.init = 16'hfefe;
    LUT4 i1543_4_lut (.A(n2415), .B(sclPipe[4]), .C(n8), .D(sclPipe[1]), 
         .Z(n2416)) /* synthesis lut_function=(A+(B (C (D)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(122[8] 141[4])
    defparam i1543_4_lut.init = 16'heaaa;
    LUT4 i1_3_lut (.A(\sclDelayed[5] ), .B(n492[2]), .C(n492[3]), .Z(n1972)) /* synthesis lut_function=(!(A+!(B+(C)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(205[11:24])
    defparam i1_3_lut.init = 16'h5454;
    FD1S3AX startEdgeDet_74_rep_53 (.D(startEdgeDet_N_123), .CK(\led_timer[3] ), 
            .Q(n4868)) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=16, LSE_RCOL=24, LSE_LLINE=353, LSE_RLINE=353 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(162[8] 181[4])
    defparam startEdgeDet_74_rep_53.GSR = "ENABLED";
    registerInterface u_registerInterface (.regAddr({regAddr_adj_576}), .mode_reg2({mode_reg2}), 
            .data_reg4({data_reg4}), .\data_reg3[0] (\data_reg3[0] ), .\led_timer[3] (\led_timer[3] ), 
            .dataToRegIF({dataToRegIF}), .pin_gpio_26_c_6(pin_gpio_26_c_6), 
            .enc_counter({enc_counter}), .writeEn(writeEn), .\led_output[4] (\led_output[4] ), 
            .\led_output[7] (\led_output[7] ), .pin_gpio_06_c_5(pin_gpio_06_c_5), 
            .\data_reg3[1] (\data_reg3[1] ), .\data_reg3[4] (\data_reg3[4] ), 
            .\data_reg3[3] (\data_reg3[3] ), .\data_reg3[2] (\data_reg3[2] ), 
            .dataFromRegIF({dataFromRegIF}), .pin_ta_1_c_0(pin_ta_1_c_0), 
            .pin_ta_2_c(pin_ta_2_c)) /* synthesis syn_module_defined=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(184[19] 198[2])
    serialInterface regAddr_7__I_0 (.\sclDelayed[5] (\sclDelayed[5] ), .n501(n501), 
            .startEdgeDet(startEdgeDet), .n4720(n4720), .n508(n508), .n507(n492[1]), 
            .dataFromRegIF({dataFromRegIF}), .dataToRegIF({dataToRegIF}), 
            .\led_timer[3] (\led_timer[3] ), .n4730(n4730), .n4(n4), .n4726(n4726), 
            .regAddr({regAddr_adj_576}), .sdaDeb(sdaDeb), .writeEn(writeEn), 
            .next_writeEn(next_writeEn), .startStopDetState({startStopDetState}), 
            .\led_timer[3]_enable_70 (\led_timer[3]_enable_70 ), .n4868(n4868), 
            .n504(n504), .n4723(n4723), .n4359(n4359), .n4725(n4725), 
            .GND_net(GND_net), .n36(n36), .n52(n52), .n1218(n1218), 
            .n2104(n2104), .\led_timer[3]_enable_44 (led_timer_3_enable_44), 
            .n506(n492[2]), .n505(n492[3]), .n1972(n1972), .n540(n540), 
            .n4_adj_1(n4_c), .n4719(n4719), .clearStartStopDet(clearStartStopDet), 
            .sdaOut(sdaOut)) /* synthesis syn_module_defined=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(200[17] 212[2])
    
endmodule
//
// Verilog Description of module registerInterface
//

module registerInterface (regAddr, mode_reg2, data_reg4, \data_reg3[0] , 
            \led_timer[3] , dataToRegIF, pin_gpio_26_c_6, enc_counter, 
            writeEn, \led_output[4] , \led_output[7] , pin_gpio_06_c_5, 
            \data_reg3[1] , \data_reg3[4] , \data_reg3[3] , \data_reg3[2] , 
            dataFromRegIF, pin_ta_1_c_0, pin_ta_2_c) /* synthesis syn_module_defined=1 */ ;
    input [7:0]regAddr;
    output [7:0]mode_reg2;
    output [7:0]data_reg4;
    output \data_reg3[0] ;
    input \led_timer[3] ;
    input [7:0]dataToRegIF;
    input pin_gpio_26_c_6;
    input [7:0]enc_counter;
    input writeEn;
    input \led_output[4] ;
    input \led_output[7] ;
    input pin_gpio_06_c_5;
    output \data_reg3[1] ;
    output \data_reg3[4] ;
    output \data_reg3[3] ;
    output \data_reg3[2] ;
    output [7:0]dataFromRegIF;
    input pin_ta_1_c_0;
    input pin_ta_2_c;
    
    wire \led_timer[3]  /* synthesis SET_AS_NETWORK=led_timer[3], is_clock=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(208[9:18])
    wire writeEn /* synthesis is_clock=1, SET_AS_NETWORK=\i2cSlave_inst/writeEn */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(102[6:13])
    
    wire n2110, led_timer_3_enable_55;
    wire [7:0]myReg0;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(70[14:20])
    wire [7:0]n1325;
    wire [7:0]myReg2;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(72[14:20])
    wire [7:0]n1344;
    
    wire led_timer_3_enable_31, led_timer_3_enable_24, n4444, n4341, 
        n7, n4347, n4447, n4403, n4404, n4394, n4395, n4396, 
        n4401;
    wire [7:0]n1356;
    
    wire n4438, n4398, led_timer_3_enable_63, n4098, n2470, n4397, 
        n4399, n4400, n4402, n4441, n4405, n4448, n4445, n4442, 
        n4446, n4439, n4443, n4437, n4440;
    
    LUT4 i1_2_lut_3_lut (.A(regAddr[0]), .B(n2110), .C(regAddr[1]), .Z(led_timer_3_enable_55)) /* synthesis lut_function=(!(((C)+!B)+!A)) */ ;
    defparam i1_2_lut_3_lut.init = 16'h0808;
    LUT4 mux_657_i6_3_lut (.A(myReg0[5]), .B(mode_reg2[5]), .C(regAddr[0]), 
         .Z(n1325[5])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam mux_657_i6_3_lut.init = 16'hcaca;
    LUT4 mux_668_i7_3_lut (.A(myReg2[6]), .B(data_reg4[6]), .C(regAddr[0]), 
         .Z(n1344[6])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam mux_668_i7_3_lut.init = 16'hcaca;
    LUT4 mux_657_i7_3_lut (.A(myReg0[6]), .B(mode_reg2[6]), .C(regAddr[0]), 
         .Z(n1325[6])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam mux_657_i7_3_lut.init = 16'hcaca;
    FD1P3AX myReg2_i0_i0 (.D(dataToRegIF[0]), .SP(led_timer_3_enable_31), 
            .CK(\led_timer[3] ), .Q(\data_reg3[0] )) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg2_i0_i0.GSR = "ENABLED";
    FD1P3AX myReg3_i0_i0 (.D(dataToRegIF[0]), .SP(led_timer_3_enable_24), 
            .CK(\led_timer[3] ), .Q(data_reg4[0])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg3_i0_i0.GSR = "ENABLED";
    LUT4 mux_668_i8_3_lut (.A(myReg2[7]), .B(data_reg4[7]), .C(regAddr[0]), 
         .Z(n1344[7])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam mux_668_i8_3_lut.init = 16'hcaca;
    LUT4 i3522_4_lut_4_lut (.A(regAddr[0]), .B(regAddr[1]), .C(pin_gpio_26_c_6), 
         .D(enc_counter[2]), .Z(n4444)) /* synthesis lut_function=(A (B (C))+!A (B (C)+!B (D))) */ ;
    defparam i3522_4_lut_4_lut.init = 16'hd1c0;
    LUT4 i1_4_lut (.A(writeEn), .B(n4341), .C(n7), .D(n4347), .Z(n2110)) /* synthesis lut_function=(!((B+(C+(D)))+!A)) */ ;
    defparam i1_4_lut.init = 16'h0002;
    LUT4 mux_657_i8_3_lut (.A(myReg0[7]), .B(mode_reg2[7]), .C(regAddr[0]), 
         .Z(n1325[7])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam mux_657_i8_3_lut.init = 16'hcaca;
    LUT4 mux_668_i1_3_lut (.A(\data_reg3[0] ), .B(data_reg4[0]), .C(regAddr[0]), 
         .Z(n1344[0])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam mux_668_i1_3_lut.init = 16'hcaca;
    LUT4 i3431_2_lut (.A(regAddr[2]), .B(regAddr[6]), .Z(n4347)) /* synthesis lut_function=(A+(B)) */ ;
    defparam i3431_2_lut.init = 16'heeee;
    LUT4 i3525_4_lut_4_lut (.A(regAddr[0]), .B(regAddr[1]), .C(\led_output[4] ), 
         .D(enc_counter[3]), .Z(n4447)) /* synthesis lut_function=(A (B (C))+!A (B (C)+!B (D))) */ ;
    defparam i3525_4_lut_4_lut.init = 16'hd1c0;
    LUT4 mux_657_i1_3_lut (.A(myReg0[0]), .B(mode_reg2[0]), .C(regAddr[0]), 
         .Z(n1325[0])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam mux_657_i1_3_lut.init = 16'hcaca;
    FD1P3AX myReg1_i0_i0 (.D(dataToRegIF[0]), .SP(led_timer_3_enable_55), 
            .CK(\led_timer[3] ), .Q(mode_reg2[0])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg1_i0_i0.GSR = "ENABLED";
    LUT4 i3426_2_lut (.A(regAddr[5]), .B(regAddr[4]), .Z(n4341)) /* synthesis lut_function=(A+(B)) */ ;
    defparam i3426_2_lut.init = 16'heeee;
    LUT4 i3579_3_lut (.A(n1325[7]), .B(n1344[7]), .C(regAddr[1]), .Z(n4403)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i3579_3_lut.init = 16'hcaca;
    LUT4 i1_2_lut (.A(regAddr[3]), .B(regAddr[7]), .Z(n7)) /* synthesis lut_function=(A+(B)) */ ;
    defparam i1_2_lut.init = 16'heeee;
    LUT4 i3482_4_lut_4_lut (.A(regAddr[0]), .B(regAddr[1]), .C(\led_output[4] ), 
         .D(enc_counter[7]), .Z(n4404)) /* synthesis lut_function=(A (B (C))+!A (B (C)+!B (D))) */ ;
    defparam i3482_4_lut_4_lut.init = 16'hd1c0;
    PFUMX i3474 (.BLUT(n4394), .ALUT(n4395), .C0(regAddr[2]), .Z(n4396));
    LUT4 i3479_4_lut_4_lut (.A(regAddr[0]), .B(regAddr[1]), .C(pin_gpio_26_c_6), 
         .D(enc_counter[6]), .Z(n4401)) /* synthesis lut_function=(A (B (C))+!A (B (C)+!B (D))) */ ;
    defparam i3479_4_lut_4_lut.init = 16'hd1c0;
    LUT4 i3516_3_lut (.A(n1356[0]), .B(\led_output[7] ), .C(regAddr[1]), 
         .Z(n4438)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i3516_3_lut.init = 16'hcaca;
    LUT4 i3476_4_lut_4_lut (.A(regAddr[0]), .B(regAddr[1]), .C(pin_gpio_06_c_5), 
         .D(enc_counter[5]), .Z(n4398)) /* synthesis lut_function=(A (B (C))+!A (B (C)+!B (D))) */ ;
    defparam i3476_4_lut_4_lut.init = 16'hd1c0;
    LUT4 i3473_4_lut_4_lut (.A(regAddr[0]), .B(regAddr[1]), .C(\led_output[7] ), 
         .D(enc_counter[4]), .Z(n4395)) /* synthesis lut_function=(A (B (C))+!A (B (C)+!B (D))) */ ;
    defparam i3473_4_lut_4_lut.init = 16'hd1c0;
    FD1P3AX myReg0_i0_i0 (.D(dataToRegIF[0]), .SP(led_timer_3_enable_63), 
            .CK(\led_timer[3] ), .Q(myReg0[0])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg0_i0_i0.GSR = "ENABLED";
    LUT4 i5_4_lut (.A(n4341), .B(n7), .C(n4098), .D(regAddr[6]), .Z(n2470)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;
    defparam i5_4_lut.init = 16'hfffe;
    LUT4 mux_668_i2_3_lut (.A(\data_reg3[1] ), .B(data_reg4[1]), .C(regAddr[0]), 
         .Z(n1344[1])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam mux_668_i2_3_lut.init = 16'hcaca;
    PFUMX i3477 (.BLUT(n4397), .ALUT(n4398), .C0(regAddr[2]), .Z(n4399));
    LUT4 i1_2_lut_3_lut_adj_39 (.A(regAddr[0]), .B(n2110), .C(regAddr[1]), 
         .Z(led_timer_3_enable_24)) /* synthesis lut_function=(A (B (C))) */ ;
    defparam i1_2_lut_3_lut_adj_39.init = 16'h8080;
    PFUMX i3480 (.BLUT(n4400), .ALUT(n4401), .C0(regAddr[2]), .Z(n4402));
    LUT4 mux_657_i2_3_lut (.A(myReg0[1]), .B(mode_reg2[1]), .C(regAddr[0]), 
         .Z(n1325[1])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam mux_657_i2_3_lut.init = 16'hcaca;
    LUT4 i2_3_lut (.A(regAddr[0]), .B(regAddr[2]), .C(regAddr[1]), .Z(n4098)) /* synthesis lut_function=(A (B (C))) */ ;
    defparam i2_3_lut.init = 16'h8080;
    LUT4 i3519_3_lut (.A(n1356[1]), .B(pin_gpio_06_c_5), .C(regAddr[1]), 
         .Z(n4441)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i3519_3_lut.init = 16'hcaca;
    FD1P3AX myReg3_i0_i7 (.D(dataToRegIF[7]), .SP(led_timer_3_enable_24), 
            .CK(\led_timer[3] ), .Q(data_reg4[7])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg3_i0_i7.GSR = "ENABLED";
    FD1P3AX myReg3_i0_i6 (.D(dataToRegIF[6]), .SP(led_timer_3_enable_24), 
            .CK(\led_timer[3] ), .Q(data_reg4[6])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg3_i0_i6.GSR = "ENABLED";
    FD1P3AX myReg3_i0_i5 (.D(dataToRegIF[5]), .SP(led_timer_3_enable_24), 
            .CK(\led_timer[3] ), .Q(data_reg4[5])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg3_i0_i5.GSR = "ENABLED";
    FD1P3AX myReg3_i0_i4 (.D(dataToRegIF[4]), .SP(led_timer_3_enable_24), 
            .CK(\led_timer[3] ), .Q(data_reg4[4])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg3_i0_i4.GSR = "ENABLED";
    FD1P3AX myReg3_i0_i3 (.D(dataToRegIF[3]), .SP(led_timer_3_enable_24), 
            .CK(\led_timer[3] ), .Q(data_reg4[3])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg3_i0_i3.GSR = "ENABLED";
    FD1P3AX myReg3_i0_i2 (.D(dataToRegIF[2]), .SP(led_timer_3_enable_24), 
            .CK(\led_timer[3] ), .Q(data_reg4[2])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg3_i0_i2.GSR = "ENABLED";
    FD1P3AX myReg3_i0_i1 (.D(dataToRegIF[1]), .SP(led_timer_3_enable_24), 
            .CK(\led_timer[3] ), .Q(data_reg4[1])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg3_i0_i1.GSR = "ENABLED";
    FD1P3AX myReg2_i0_i7 (.D(dataToRegIF[7]), .SP(led_timer_3_enable_31), 
            .CK(\led_timer[3] ), .Q(myReg2[7])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg2_i0_i7.GSR = "ENABLED";
    FD1P3AX myReg2_i0_i6 (.D(dataToRegIF[6]), .SP(led_timer_3_enable_31), 
            .CK(\led_timer[3] ), .Q(myReg2[6])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg2_i0_i6.GSR = "ENABLED";
    FD1P3AX myReg2_i0_i5 (.D(dataToRegIF[5]), .SP(led_timer_3_enable_31), 
            .CK(\led_timer[3] ), .Q(myReg2[5])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg2_i0_i5.GSR = "ENABLED";
    FD1P3AX myReg2_i0_i4 (.D(dataToRegIF[4]), .SP(led_timer_3_enable_31), 
            .CK(\led_timer[3] ), .Q(\data_reg3[4] )) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg2_i0_i4.GSR = "ENABLED";
    FD1P3AX myReg2_i0_i3 (.D(dataToRegIF[3]), .SP(led_timer_3_enable_31), 
            .CK(\led_timer[3] ), .Q(\data_reg3[3] )) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg2_i0_i3.GSR = "ENABLED";
    FD1P3AX myReg2_i0_i2 (.D(dataToRegIF[2]), .SP(led_timer_3_enable_31), 
            .CK(\led_timer[3] ), .Q(\data_reg3[2] )) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg2_i0_i2.GSR = "ENABLED";
    FD1P3AX myReg2_i0_i1 (.D(dataToRegIF[1]), .SP(led_timer_3_enable_31), 
            .CK(\led_timer[3] ), .Q(\data_reg3[1] )) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg2_i0_i1.GSR = "ENABLED";
    LUT4 mux_668_i3_3_lut (.A(\data_reg3[2] ), .B(data_reg4[2]), .C(regAddr[0]), 
         .Z(n1344[2])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam mux_668_i3_3_lut.init = 16'hcaca;
    LUT4 mux_657_i3_3_lut (.A(myReg0[2]), .B(mode_reg2[2]), .C(regAddr[0]), 
         .Z(n1325[2])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam mux_657_i3_3_lut.init = 16'hcaca;
    LUT4 mux_668_i4_3_lut (.A(\data_reg3[3] ), .B(data_reg4[3]), .C(regAddr[0]), 
         .Z(n1344[3])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam mux_668_i4_3_lut.init = 16'hcaca;
    PFUMX i3483 (.BLUT(n4403), .ALUT(n4404), .C0(regAddr[2]), .Z(n4405));
    LUT4 mux_657_i4_3_lut (.A(myReg0[3]), .B(mode_reg2[3]), .C(regAddr[0]), 
         .Z(n1325[3])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam mux_657_i4_3_lut.init = 16'hcaca;
    LUT4 mux_668_i5_3_lut (.A(\data_reg3[4] ), .B(data_reg4[4]), .C(regAddr[0]), 
         .Z(n1344[4])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam mux_668_i5_3_lut.init = 16'hcaca;
    LUT4 mux_657_i5_3_lut (.A(myReg0[4]), .B(mode_reg2[4]), .C(regAddr[0]), 
         .Z(n1325[4])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam mux_657_i5_3_lut.init = 16'hcaca;
    FD1S3IX dataOut__i7 (.D(n4405), .CK(\led_timer[3] ), .CD(n2470), .Q(dataFromRegIF[7])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(87[8] 99[4])
    defparam dataOut__i7.GSR = "ENABLED";
    FD1S3IX dataOut__i6 (.D(n4402), .CK(\led_timer[3] ), .CD(n2470), .Q(dataFromRegIF[6])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(87[8] 99[4])
    defparam dataOut__i6.GSR = "ENABLED";
    FD1S3IX dataOut__i5 (.D(n4399), .CK(\led_timer[3] ), .CD(n2470), .Q(dataFromRegIF[5])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(87[8] 99[4])
    defparam dataOut__i5.GSR = "ENABLED";
    FD1S3IX dataOut__i4 (.D(n4396), .CK(\led_timer[3] ), .CD(n2470), .Q(dataFromRegIF[4])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(87[8] 99[4])
    defparam dataOut__i4.GSR = "ENABLED";
    FD1S3IX dataOut__i3 (.D(n4448), .CK(\led_timer[3] ), .CD(n2470), .Q(dataFromRegIF[3])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(87[8] 99[4])
    defparam dataOut__i3.GSR = "ENABLED";
    FD1S3IX dataOut__i2 (.D(n4445), .CK(\led_timer[3] ), .CD(n2470), .Q(dataFromRegIF[2])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(87[8] 99[4])
    defparam dataOut__i2.GSR = "ENABLED";
    FD1S3IX dataOut__i1 (.D(n4442), .CK(\led_timer[3] ), .CD(n2470), .Q(dataFromRegIF[1])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(87[8] 99[4])
    defparam dataOut__i1.GSR = "ENABLED";
    LUT4 i3583_3_lut (.A(n1325[6]), .B(n1344[6]), .C(regAddr[1]), .Z(n4400)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i3583_3_lut.init = 16'hcaca;
    FD1P3AX myReg1_i0_i1 (.D(dataToRegIF[1]), .SP(led_timer_3_enable_55), 
            .CK(\led_timer[3] ), .Q(mode_reg2[1])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg1_i0_i1.GSR = "ENABLED";
    FD1P3AX myReg1_i0_i2 (.D(dataToRegIF[2]), .SP(led_timer_3_enable_55), 
            .CK(\led_timer[3] ), .Q(mode_reg2[2])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg1_i0_i2.GSR = "ENABLED";
    FD1P3AX myReg1_i0_i3 (.D(dataToRegIF[3]), .SP(led_timer_3_enable_55), 
            .CK(\led_timer[3] ), .Q(mode_reg2[3])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg1_i0_i3.GSR = "ENABLED";
    FD1P3AX myReg1_i0_i4 (.D(dataToRegIF[4]), .SP(led_timer_3_enable_55), 
            .CK(\led_timer[3] ), .Q(mode_reg2[4])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg1_i0_i4.GSR = "ENABLED";
    FD1P3AX myReg1_i0_i5 (.D(dataToRegIF[5]), .SP(led_timer_3_enable_55), 
            .CK(\led_timer[3] ), .Q(mode_reg2[5])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg1_i0_i5.GSR = "ENABLED";
    FD1P3AX myReg1_i0_i6 (.D(dataToRegIF[6]), .SP(led_timer_3_enable_55), 
            .CK(\led_timer[3] ), .Q(mode_reg2[6])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg1_i0_i6.GSR = "ENABLED";
    FD1P3AX myReg1_i0_i7 (.D(dataToRegIF[7]), .SP(led_timer_3_enable_55), 
            .CK(\led_timer[3] ), .Q(mode_reg2[7])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg1_i0_i7.GSR = "ENABLED";
    LUT4 i3585_3_lut (.A(n1325[5]), .B(n1344[5]), .C(regAddr[1]), .Z(n4397)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i3585_3_lut.init = 16'hcaca;
    LUT4 i3566_3_lut (.A(n1325[3]), .B(n1344[3]), .C(regAddr[1]), .Z(n4446)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i3566_3_lut.init = 16'hcaca;
    FD1P3AX myReg0_i0_i1 (.D(dataToRegIF[1]), .SP(led_timer_3_enable_63), 
            .CK(\led_timer[3] ), .Q(myReg0[1])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg0_i0_i1.GSR = "ENABLED";
    FD1P3AX myReg0_i0_i2 (.D(dataToRegIF[2]), .SP(led_timer_3_enable_63), 
            .CK(\led_timer[3] ), .Q(myReg0[2])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg0_i0_i2.GSR = "ENABLED";
    FD1P3AX myReg0_i0_i3 (.D(dataToRegIF[3]), .SP(led_timer_3_enable_63), 
            .CK(\led_timer[3] ), .Q(myReg0[3])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg0_i0_i3.GSR = "ENABLED";
    FD1P3AX myReg0_i0_i4 (.D(dataToRegIF[4]), .SP(led_timer_3_enable_63), 
            .CK(\led_timer[3] ), .Q(myReg0[4])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg0_i0_i4.GSR = "ENABLED";
    FD1P3AX myReg0_i0_i5 (.D(dataToRegIF[5]), .SP(led_timer_3_enable_63), 
            .CK(\led_timer[3] ), .Q(myReg0[5])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg0_i0_i5.GSR = "ENABLED";
    FD1P3AX myReg0_i0_i6 (.D(dataToRegIF[6]), .SP(led_timer_3_enable_63), 
            .CK(\led_timer[3] ), .Q(myReg0[6])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg0_i0_i6.GSR = "ENABLED";
    FD1P3AX myReg0_i0_i7 (.D(dataToRegIF[7]), .SP(led_timer_3_enable_63), 
            .CK(\led_timer[3] ), .Q(myReg0[7])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(102[8] 111[4])
    defparam myReg0_i0_i7.GSR = "ENABLED";
    FD1S3IX dataOut__i0 (.D(n4439), .CK(\led_timer[3] ), .CD(n2470), .Q(dataFromRegIF[0])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=19, LSE_RCOL=2, LSE_LLINE=184, LSE_RLINE=198 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/registerinterface.v(87[8] 99[4])
    defparam dataOut__i0.GSR = "ENABLED";
    LUT4 i3568_3_lut (.A(n1325[2]), .B(n1344[2]), .C(regAddr[1]), .Z(n4443)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i3568_3_lut.init = 16'hcaca;
    LUT4 i1_2_lut_3_lut_adj_40 (.A(regAddr[0]), .B(n2110), .C(regAddr[1]), 
         .Z(led_timer_3_enable_31)) /* synthesis lut_function=(!(A+!(B (C)))) */ ;
    defparam i1_2_lut_3_lut_adj_40.init = 16'h4040;
    LUT4 mux_672_i1_3_lut (.A(enc_counter[0]), .B(pin_ta_1_c_0), .C(regAddr[0]), 
         .Z(n1356[0])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam mux_672_i1_3_lut.init = 16'hcaca;
    LUT4 i3587_3_lut (.A(n1325[4]), .B(n1344[4]), .C(regAddr[1]), .Z(n4394)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i3587_3_lut.init = 16'hcaca;
    PFUMX i3517 (.BLUT(n4437), .ALUT(n4438), .C0(regAddr[2]), .Z(n4439));
    PFUMX i3520 (.BLUT(n4440), .ALUT(n4441), .C0(regAddr[2]), .Z(n4442));
    LUT4 i3570_3_lut (.A(n1325[1]), .B(n1344[1]), .C(regAddr[1]), .Z(n4440)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i3570_3_lut.init = 16'hcaca;
    LUT4 mux_672_i2_3_lut (.A(enc_counter[1]), .B(pin_ta_2_c), .C(regAddr[0]), 
         .Z(n1356[1])) /* synthesis lut_function=(!(A (B (C))+!A (B+!(C)))) */ ;
    defparam mux_672_i2_3_lut.init = 16'h3a3a;
    PFUMX i3523 (.BLUT(n4443), .ALUT(n4444), .C0(regAddr[2]), .Z(n4445));
    PFUMX i3526 (.BLUT(n4446), .ALUT(n4447), .C0(regAddr[2]), .Z(n4448));
    LUT4 i3572_3_lut (.A(n1325[0]), .B(n1344[0]), .C(regAddr[1]), .Z(n4437)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam i3572_3_lut.init = 16'hcaca;
    LUT4 i1_2_lut_3_lut_adj_41 (.A(regAddr[0]), .B(n2110), .C(regAddr[1]), 
         .Z(led_timer_3_enable_63)) /* synthesis lut_function=(!(A+((C)+!B))) */ ;
    defparam i1_2_lut_3_lut_adj_41.init = 16'h0404;
    LUT4 mux_668_i6_3_lut (.A(myReg2[5]), .B(data_reg4[5]), .C(regAddr[0]), 
         .Z(n1344[5])) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;
    defparam mux_668_i6_3_lut.init = 16'hcaca;
    
endmodule
//
// Verilog Description of module serialInterface
//

module serialInterface (\sclDelayed[5] , n501, startEdgeDet, n4720, 
            n508, n507, dataFromRegIF, dataToRegIF, \led_timer[3] , 
            n4730, n4, n4726, regAddr, sdaDeb, writeEn, next_writeEn, 
            startStopDetState, \led_timer[3]_enable_70 , n4868, n504, 
            n4723, n4359, n4725, GND_net, n36, n52, n1218, n2104, 
            \led_timer[3]_enable_44 , n506, n505, n1972, n540, n4_adj_1, 
            n4719, clearStartStopDet, sdaOut) /* synthesis syn_module_defined=1 */ ;
    input \sclDelayed[5] ;
    output n501;
    input startEdgeDet;
    output n4720;
    output n508;
    output n507;
    input [7:0]dataFromRegIF;
    output [7:0]dataToRegIF;
    input \led_timer[3] ;
    input n4730;
    output n4;
    output n4726;
    output [7:0]regAddr;
    input sdaDeb;
    output writeEn;
    output next_writeEn;
    input [1:0]startStopDetState;
    input \led_timer[3]_enable_70 ;
    input n4868;
    output n504;
    input n4723;
    output n4359;
    output n4725;
    input GND_net;
    input n36;
    output n52;
    input n1218;
    output n2104;
    input \led_timer[3]_enable_44 ;
    output n506;
    output n505;
    input n1972;
    input n540;
    input n4_adj_1;
    input n4719;
    output clearStartStopDet;
    output sdaOut;
    
    wire \led_timer[3]  /* synthesis SET_AS_NETWORK=led_timer[3], is_clock=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(208[9:18])
    wire writeEn /* synthesis is_clock=1, SET_AS_NETWORK=\i2cSlave_inst/writeEn */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(102[6:13])
    wire [15:0]n492;
    
    wire n1968;
    wire [1:0]streamSt;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(86[11:19])
    
    wire n4849, n4732;
    wire [7:0]txData;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(87[11:17])
    wire [7:0]next_txData;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(87[19:30])
    
    wire n4721, n4731;
    wire [7:0]rxData;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(85[11:17])
    wire [7:0]n1491;
    
    wire led_timer_3_enable_38, n776, n553, n1852, n4733, n1966, 
        led_timer_3_enable_17, n807, n4718;
    wire [7:0]next_regAddr_7__N_395;
    
    wire n812;
    wire [2:0]bitCnt;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(84[11:17])
    
    wire led_timer_3_enable_48;
    wire [2:0]n17;
    
    wire led_timer_3_enable_6;
    wire [1:0]n1467;
    
    wire n1959, n1839, n4717, n811, n1838, n4722, n8, n6, n2116, 
        n4_adj_556, n13, n1964, n7, led_timer_3_enable_40, next_sdaOut_N_470, 
        n8_adj_557, n4353, n814, n1846, n1860, n4044, n813, n810, 
        n809, n808, n6_adj_558, NextState_SISt_3__N_415, n569, led_timer_3_enable_73, 
        n783, n31, n32, n782, n781, n780, n779, n778, n777, 
        n4727, led_timer_3_enable_39, n29, n30, n4096, n4016, n4340, 
        n4306, n4281, n1970, n4043, n4042, n8_adj_559, n4375, 
        n6_adj_560, n4315, n4_adj_561, led_timer_3_enable_71, n1664, 
        n4343, n4304, n15, led_timer_3_enable_74, n12, n15_adj_563, 
        n14, n4041, n4848, n4715, n1868;
    
    LUT4 i1133_3_lut (.A(\sclDelayed[5] ), .B(n501), .C(n492[8]), .Z(n1968)) /* synthesis lut_function=(A (B+(C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i1133_3_lut.init = 16'ha8a8;
    LUT4 i3623_4_lut_then_4_lut (.A(startEdgeDet), .B(streamSt[1]), .C(streamSt[0]), 
         .D(n4720), .Z(n4849)) /* synthesis lut_function=(A (B (C+!(D))+!B !(D))+!A (B (C (D)))) */ ;
    defparam i3623_4_lut_then_4_lut.init = 16'hc0aa;
    LUT4 i1_2_lut_else_3_lut (.A(startEdgeDet), .B(n492[14]), .C(n508), 
         .Z(n4732)) /* synthesis lut_function=(A+(B+(C))) */ ;
    defparam i1_2_lut_else_3_lut.init = 16'hfefe;
    LUT4 i1101_4_lut_4_lut (.A(n508), .B(txData[0]), .C(n507), .D(dataFromRegIF[1]), 
         .Z(next_txData[1])) /* synthesis lut_function=(A (C (D))+!A (B ((D)+!C)+!B (C (D)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i1101_4_lut_4_lut.init = 16'hf404;
    LUT4 i1021_2_lut_rep_41 (.A(\sclDelayed[5] ), .B(n492[8]), .Z(n4721)) /* synthesis lut_function=(!(A+!(B))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i1021_2_lut_rep_41.init = 16'h4444;
    LUT4 i1_2_lut_rep_51 (.A(n492[11]), .B(n492[8]), .Z(n4731)) /* synthesis lut_function=(A+(B)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i1_2_lut_rep_51.init = 16'heeee;
    LUT4 i2362_2_lut_3_lut (.A(n492[11]), .B(n492[8]), .C(rxData[5]), 
         .Z(n1491[6])) /* synthesis lut_function=(A (C)+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i2362_2_lut_3_lut.init = 16'he0e0;
    LUT4 i2363_2_lut_3_lut (.A(n492[11]), .B(n492[8]), .C(rxData[6]), 
         .Z(n1491[7])) /* synthesis lut_function=(A (C)+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i2363_2_lut_3_lut.init = 16'he0e0;
    FD1P3IX dataOut__i0 (.D(n776), .SP(led_timer_3_enable_38), .CD(startEdgeDet), 
            .CK(\led_timer[3] ), .Q(dataToRegIF[0])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam dataOut__i0.GSR = "ENABLED";
    LUT4 i245_2_lut (.A(\sclDelayed[5] ), .B(n492[10]), .Z(n553)) /* synthesis lut_function=(!(A+!(B))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i245_2_lut.init = 16'h4444;
    LUT4 i1_4_lut (.A(n501), .B(n4731), .C(\sclDelayed[5] ), .D(n4730), 
         .Z(n4)) /* synthesis lut_function=(A (B+(C+(D)))+!A !((C)+!B)) */ ;
    defparam i1_4_lut.init = 16'haeac;
    LUT4 i1022_2_lut_3_lut_4_lut (.A(\sclDelayed[5] ), .B(n492[8]), .C(n507), 
         .D(n4726), .Z(n1852)) /* synthesis lut_function=(!(A ((D)+!C)+!A !(B+!((D)+!C)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i1022_2_lut_3_lut_4_lut.init = 16'h44f4;
    LUT4 i1_2_lut_then_3_lut_4_lut (.A(streamSt[0]), .B(streamSt[1]), .C(n4720), 
         .D(startEdgeDet), .Z(n4733)) /* synthesis lut_function=(A (B (C+(D))+!B (D))+!A (D)) */ ;
    defparam i1_2_lut_then_3_lut_4_lut.init = 16'hff80;
    LUT4 i2361_2_lut_3_lut (.A(n492[11]), .B(n492[8]), .C(rxData[4]), 
         .Z(n1491[5])) /* synthesis lut_function=(A (C)+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i2361_2_lut_3_lut.init = 16'he0e0;
    LUT4 i2360_2_lut_3_lut (.A(n492[11]), .B(n492[8]), .C(rxData[3]), 
         .Z(n1491[4])) /* synthesis lut_function=(A (C)+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i2360_2_lut_3_lut.init = 16'he0e0;
    LUT4 i1132_3_lut (.A(\sclDelayed[5] ), .B(n492[10]), .C(n492[11]), 
         .Z(n1966)) /* synthesis lut_function=(A (B+(C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i1132_3_lut.init = 16'ha8a8;
    LUT4 i1_2_lut_3_lut (.A(n492[11]), .B(n492[8]), .C(rxData[2]), .Z(n1491[3])) /* synthesis lut_function=(A (C)+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i1_2_lut_3_lut.init = 16'he0e0;
    LUT4 i2359_2_lut_3_lut (.A(n492[11]), .B(n492[8]), .C(rxData[1]), 
         .Z(n1491[2])) /* synthesis lut_function=(A (C)+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i2359_2_lut_3_lut.init = 16'he0e0;
    FD1P3AX regAddr__i0 (.D(n807), .SP(led_timer_3_enable_17), .CK(\led_timer[3] ), 
            .Q(regAddr[0])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam regAddr__i0.GSR = "ENABLED";
    LUT4 i2290_2_lut_3_lut (.A(n492[11]), .B(n492[8]), .C(sdaDeb), .Z(n1491[0])) /* synthesis lut_function=(A (C)+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i2290_2_lut_3_lut.init = 16'he0e0;
    LUT4 i1_2_lut_3_lut_adj_17 (.A(n492[11]), .B(n492[8]), .C(rxData[0]), 
         .Z(n1491[1])) /* synthesis lut_function=(A (C)+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i1_2_lut_3_lut_adj_17.init = 16'he0e0;
    LUT4 mux_373_i6_3_lut_4_lut (.A(n492[14]), .B(n4718), .C(next_regAddr_7__N_395[5]), 
         .D(rxData[5]), .Z(n812)) /* synthesis lut_function=(A (C)+!A (B (D)+!B (C))) */ ;
    defparam mux_373_i6_3_lut_4_lut.init = 16'hf4b0;
    FD1P3AX bitCnt_800__i0 (.D(n17[0]), .SP(led_timer_3_enable_48), .CK(\led_timer[3] ), 
            .Q(bitCnt[0]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(352[3] 362[6])
    defparam bitCnt_800__i0.GSR = "ENABLED";
    FD1P3IX streamSt__i0 (.D(n1467[0]), .SP(led_timer_3_enable_6), .CD(startEdgeDet), 
            .CK(\led_timer[3] ), .Q(streamSt[0])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam streamSt__i0.GSR = "ENABLED";
    FD1S3AX CurrState_SISt_FSM_i0 (.D(startEdgeDet), .CK(\led_timer[3] ), 
            .Q(n508));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i0.GSR = "ENABLED";
    LUT4 i3618_4_lut (.A(n1959), .B(n1839), .C(n4717), .D(startEdgeDet), 
         .Z(led_timer_3_enable_17)) /* synthesis lut_function=(!(A ((C+(D))+!B)+!A ((D)+!B))) */ ;
    defparam i3618_4_lut.init = 16'h004c;
    LUT4 mux_373_i5_3_lut_4_lut (.A(n492[14]), .B(n4718), .C(next_regAddr_7__N_395[4]), 
         .D(rxData[4]), .Z(n811)) /* synthesis lut_function=(A (C)+!A (B (D)+!B (C))) */ ;
    defparam mux_373_i5_3_lut_4_lut.init = 16'hf4b0;
    LUT4 i1009_3_lut (.A(n1838), .B(writeEn), .C(n492[14]), .Z(n1839)) /* synthesis lut_function=(A (B+!(C))+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i1009_3_lut.init = 16'hcaca;
    LUT4 i1008_4_lut (.A(n4726), .B(n4720), .C(next_writeEn), .D(n507), 
         .Z(n1838)) /* synthesis lut_function=(A (B (C+(D))+!B !(C+!(D)))+!A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i1008_4_lut.init = 16'hcac0;
    LUT4 i1_2_lut_rep_42 (.A(n507), .B(n492[8]), .Z(n4722)) /* synthesis lut_function=(A+(B)) */ ;
    defparam i1_2_lut_rep_42.init = 16'heeee;
    LUT4 i1_4_lut_adj_18 (.A(n8), .B(\sclDelayed[5] ), .C(n492[11]), .D(n6), 
         .Z(n2116)) /* synthesis lut_function=(!(A (B+!(C+(D))))) */ ;
    defparam i1_4_lut_adj_18.init = 16'h7775;
    LUT4 i1_4_lut_adj_19 (.A(n501), .B(startStopDetState[0]), .C(startStopDetState[1]), 
         .D(n4_adj_556), .Z(n6)) /* synthesis lut_function=(A (B+!(C+(D)))) */ ;
    defparam i1_4_lut_adj_19.init = 16'h888a;
    FD1P3IX rxData__i0 (.D(n1491[0]), .SP(\led_timer[3]_enable_70 ), .CD(n4868), 
            .CK(\led_timer[3] ), .Q(rxData[0])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam rxData__i0.GSR = "ENABLED";
    LUT4 i1128_2_lut (.A(streamSt[0]), .B(streamSt[1]), .Z(n1959)) /* synthesis lut_function=(A+!(B)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(250[9] 272[16])
    defparam i1128_2_lut.init = 16'hbbbb;
    LUT4 i4_2_lut_3_lut_4_lut (.A(n507), .B(n492[8]), .C(n492[11]), .D(n504), 
         .Z(n13)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;
    defparam i4_2_lut_3_lut_4_lut.init = 16'hfffe;
    LUT4 i1131_3_lut (.A(\sclDelayed[5] ), .B(n492[12]), .C(n492[13]), 
         .Z(n1964)) /* synthesis lut_function=(A (B+(C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i1131_3_lut.init = 16'ha8a8;
    LUT4 i1_4_lut_adj_20 (.A(led_timer_3_enable_6), .B(next_writeEn), .C(streamSt[0]), 
         .D(n7), .Z(led_timer_3_enable_40)) /* synthesis lut_function=(!((B (C+(D)))+!A)) */ ;
    defparam i1_4_lut_adj_20.init = 16'h222a;
    LUT4 i1_2_lut (.A(next_sdaOut_N_470), .B(streamSt[1]), .Z(n7)) /* synthesis lut_function=(!((B)+!A)) */ ;
    defparam i1_2_lut.init = 16'h2222;
    LUT4 i2331_3_lut (.A(streamSt[1]), .B(next_writeEn), .C(rxData[0]), 
         .Z(n1467[1])) /* synthesis lut_function=(A (B)+!A !((C)+!B)) */ ;
    defparam i2331_3_lut.init = 16'h8c8c;
    LUT4 i3629_4_lut (.A(startEdgeDet), .B(n4721), .C(n8_adj_557), .D(n4718), 
         .Z(led_timer_3_enable_48)) /* synthesis lut_function=(A+!(B+(C+(D)))) */ ;
    defparam i3629_4_lut.init = 16'haaab;
    LUT4 i3443_3_lut_4_lut (.A(n507), .B(n492[8]), .C(n492[11]), .D(n4723), 
         .Z(n4359)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;
    defparam i3443_3_lut_4_lut.init = 16'hfffe;
    LUT4 i3437_2_lut_3_lut_4_lut (.A(n504), .B(n4722), .C(n4723), .D(next_writeEn), 
         .Z(n4353)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;
    defparam i3437_2_lut_3_lut_4_lut.init = 16'hfffe;
    FD1P3AX regAddr__i7 (.D(n814), .SP(led_timer_3_enable_17), .CK(\led_timer[3] ), 
            .Q(regAddr[7])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam regAddr__i7.GSR = "ENABLED";
    LUT4 i1016_3_lut_4_lut (.A(n4725), .B(n504), .C(\sclDelayed[5] ), 
         .D(n492[5]), .Z(n1846)) /* synthesis lut_function=(A (C (D))+!A (B+(C (D)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i1016_3_lut_4_lut.init = 16'hf444;
    LUT4 i1030_3_lut (.A(n492[13]), .B(n492[14]), .C(\sclDelayed[5] ), 
         .Z(n1860)) /* synthesis lut_function=(A (B+!(C))+!A (B)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i1030_3_lut.init = 16'hcece;
    CCU2D regAddr_7__I_0_167_9 (.A0(regAddr[7]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(GND_net), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n4044), .S0(next_regAddr_7__N_395[7]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(314[23:37])
    defparam regAddr_7__I_0_167_9.INIT0 = 16'h5aaa;
    defparam regAddr_7__I_0_167_9.INIT1 = 16'h0000;
    defparam regAddr_7__I_0_167_9.INJECT1_0 = "NO";
    defparam regAddr_7__I_0_167_9.INJECT1_1 = "NO";
    FD1P3AX regAddr__i6 (.D(n813), .SP(led_timer_3_enable_17), .CK(\led_timer[3] ), 
            .Q(regAddr[6])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam regAddr__i6.GSR = "ENABLED";
    FD1P3AX regAddr__i5 (.D(n812), .SP(led_timer_3_enable_17), .CK(\led_timer[3] ), 
            .Q(regAddr[5])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam regAddr__i5.GSR = "ENABLED";
    FD1P3AX regAddr__i4 (.D(n811), .SP(led_timer_3_enable_17), .CK(\led_timer[3] ), 
            .Q(regAddr[4])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam regAddr__i4.GSR = "ENABLED";
    FD1P3AX regAddr__i3 (.D(n810), .SP(led_timer_3_enable_17), .CK(\led_timer[3] ), 
            .Q(regAddr[3])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam regAddr__i3.GSR = "ENABLED";
    FD1P3AX regAddr__i2 (.D(n809), .SP(led_timer_3_enable_17), .CK(\led_timer[3] ), 
            .Q(regAddr[2])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam regAddr__i2.GSR = "ENABLED";
    FD1P3AX regAddr__i1 (.D(n808), .SP(led_timer_3_enable_17), .CK(\led_timer[3] ), 
            .Q(regAddr[1])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam regAddr__i1.GSR = "ENABLED";
    LUT4 i3_4_lut (.A(n4353), .B(n6_adj_558), .C(n36), .D(n501), .Z(n8_adj_557)) /* synthesis lut_function=((B+(C (D)))+!A) */ ;
    defparam i3_4_lut.init = 16'hfddd;
    LUT4 i261_2_lut (.A(NextState_SISt_3__N_415), .B(n501), .Z(n569)) /* synthesis lut_function=(A (B)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i261_2_lut.init = 16'h8888;
    LUT4 i2_3_lut_rep_45 (.A(bitCnt[1]), .B(bitCnt[0]), .C(bitCnt[2]), 
         .Z(n4725)) /* synthesis lut_function=(A+(B+(C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(176[11:27])
    defparam i2_3_lut_rep_45.init = 16'hfefe;
    LUT4 i1_2_lut_3_lut_4_lut (.A(n4726), .B(n507), .C(n504), .D(n4725), 
         .Z(n6_adj_558)) /* synthesis lut_function=(!(A ((D)+!C)+!A !(B+!((D)+!C)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i1_2_lut_3_lut_4_lut.init = 16'h44f4;
    LUT4 i1_4_lut_4_lut (.A(n4726), .B(n507), .C(n52), .D(startEdgeDet), 
         .Z(led_timer_3_enable_73)) /* synthesis lut_function=(A (B+(C+(D)))+!A (B (D)+!B (C+(D)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i1_4_lut_4_lut.init = 16'hffb8;
    FD1P3IX dataOut__i7 (.D(n783), .SP(led_timer_3_enable_38), .CD(startEdgeDet), 
            .CK(\led_timer[3] ), .Q(dataToRegIF[7])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam dataOut__i7.GSR = "ENABLED";
    LUT4 i3141_4_lut (.A(startEdgeDet), .B(bitCnt[0]), .C(n1218), .D(n2104), 
         .Z(n17[0])) /* synthesis lut_function=(!(A (B+!(C))+!A (B (C+!(D))+!B !(C+(D))))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(352[3] 362[6])
    defparam i3141_4_lut.init = 16'h3530;
    PFUMX i2940 (.BLUT(n31), .ALUT(n32), .C0(n507), .Z(next_txData[6]));
    FD1P3IX dataOut__i6 (.D(n782), .SP(led_timer_3_enable_38), .CD(startEdgeDet), 
            .CK(\led_timer[3] ), .Q(dataToRegIF[6])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam dataOut__i6.GSR = "ENABLED";
    FD1P3IX dataOut__i5 (.D(n781), .SP(led_timer_3_enable_38), .CD(startEdgeDet), 
            .CK(\led_timer[3] ), .Q(dataToRegIF[5])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam dataOut__i5.GSR = "ENABLED";
    FD1P3IX dataOut__i4 (.D(n780), .SP(led_timer_3_enable_38), .CD(startEdgeDet), 
            .CK(\led_timer[3] ), .Q(dataToRegIF[4])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam dataOut__i4.GSR = "ENABLED";
    FD1P3IX dataOut__i3 (.D(n779), .SP(led_timer_3_enable_38), .CD(startEdgeDet), 
            .CK(\led_timer[3] ), .Q(dataToRegIF[3])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam dataOut__i3.GSR = "ENABLED";
    FD1P3IX dataOut__i2 (.D(n778), .SP(led_timer_3_enable_38), .CD(startEdgeDet), 
            .CK(\led_timer[3] ), .Q(dataToRegIF[2])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam dataOut__i2.GSR = "ENABLED";
    FD1P3IX dataOut__i1 (.D(n777), .SP(led_timer_3_enable_38), .CD(startEdgeDet), 
            .CK(\led_timer[3] ), .Q(dataToRegIF[1])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam dataOut__i1.GSR = "ENABLED";
    LUT4 i2291_2_lut_3_lut_4_lut (.A(bitCnt[2]), .B(n4727), .C(rxData[3]), 
         .D(next_writeEn), .Z(n779)) /* synthesis lut_function=(A (B (C (D)))) */ ;
    defparam i2291_2_lut_3_lut_4_lut.init = 16'h8000;
    FD1P3IX writeEn_132 (.D(next_writeEn), .SP(led_timer_3_enable_39), .CD(startEdgeDet), 
            .CK(\led_timer[3] ), .Q(writeEn));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam writeEn_132.GSR = "ENABLED";
    LUT4 i2310_2_lut_3_lut_4_lut (.A(bitCnt[2]), .B(n4727), .C(rxData[6]), 
         .D(next_writeEn), .Z(n782)) /* synthesis lut_function=(A (B (C (D)))) */ ;
    defparam i2310_2_lut_3_lut_4_lut.init = 16'h8000;
    PFUMX i2936 (.BLUT(n29), .ALUT(n30), .C0(n507), .Z(next_txData[7]));
    FD1P3IX streamSt__i1 (.D(n1467[1]), .SP(led_timer_3_enable_40), .CD(startEdgeDet), 
            .CK(\led_timer[3] ), .Q(streamSt[1])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam streamSt__i1.GSR = "ENABLED";
    LUT4 i2302_2_lut_3_lut_4_lut (.A(bitCnt[2]), .B(n4727), .C(rxData[5]), 
         .D(next_writeEn), .Z(n781)) /* synthesis lut_function=(A (B (C (D)))) */ ;
    defparam i2302_2_lut_3_lut_4_lut.init = 16'h8000;
    FD1P3IX txData__i7 (.D(next_txData[7]), .SP(\led_timer[3]_enable_44 ), 
            .CD(startEdgeDet), .CK(\led_timer[3] ), .Q(txData[7])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam txData__i7.GSR = "ENABLED";
    FD1P3IX txData__i6 (.D(next_txData[6]), .SP(\led_timer[3]_enable_44 ), 
            .CD(startEdgeDet), .CK(\led_timer[3] ), .Q(txData[6])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam txData__i6.GSR = "ENABLED";
    FD1P3IX txData__i5 (.D(next_txData[5]), .SP(led_timer_3_enable_73), 
            .CD(startEdgeDet), .CK(\led_timer[3] ), .Q(txData[5])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam txData__i5.GSR = "ENABLED";
    FD1P3AX txData__i0 (.D(n4096), .SP(\led_timer[3]_enable_44 ), .CK(\led_timer[3] ), 
            .Q(txData[0])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam txData__i0.GSR = "ENABLED";
    FD1P3IX txData__i4 (.D(next_txData[4]), .SP(led_timer_3_enable_73), 
            .CD(startEdgeDet), .CK(\led_timer[3] ), .Q(txData[4])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam txData__i4.GSR = "ENABLED";
    FD1P3IX txData__i3 (.D(next_txData[3]), .SP(led_timer_3_enable_73), 
            .CD(startEdgeDet), .CK(\led_timer[3] ), .Q(txData[3])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam txData__i3.GSR = "ENABLED";
    FD1P3AX bitCnt_800__i1 (.D(n4016), .SP(led_timer_3_enable_48), .CK(\led_timer[3] ), 
            .Q(bitCnt[1]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(352[3] 362[6])
    defparam bitCnt_800__i1.GSR = "ENABLED";
    FD1P3AX bitCnt_800__i2 (.D(n4340), .SP(led_timer_3_enable_48), .CK(\led_timer[3] ), 
            .Q(bitCnt[2]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(352[3] 362[6])
    defparam bitCnt_800__i2.GSR = "ENABLED";
    FD1S3IX CurrState_SISt_FSM_i1 (.D(n4306), .CK(\led_timer[3] ), .CD(n4868), 
            .Q(n507));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i1.GSR = "ENABLED";
    FD1S3IX CurrState_SISt_FSM_i2 (.D(n4281), .CK(\led_timer[3] ), .CD(startEdgeDet), 
            .Q(n506));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i2.GSR = "ENABLED";
    FD1S3IX CurrState_SISt_FSM_i3 (.D(n1972), .CK(\led_timer[3] ), .CD(n4868), 
            .Q(n505));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i3.GSR = "ENABLED";
    FD1S3IX CurrState_SISt_FSM_i4 (.D(n540), .CK(\led_timer[3] ), .CD(startEdgeDet), 
            .Q(n504));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i4.GSR = "ENABLED";
    FD1S3IX CurrState_SISt_FSM_i5 (.D(n1846), .CK(\led_timer[3] ), .CD(n4868), 
            .Q(n492[5]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i5.GSR = "ENABLED";
    FD1S3IX CurrState_SISt_FSM_i6 (.D(n1970), .CK(\led_timer[3] ), .CD(startEdgeDet), 
            .Q(n492[6]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i6.GSR = "ENABLED";
    FD1S3IX CurrState_SISt_FSM_i7 (.D(n1968), .CK(\led_timer[3] ), .CD(n4868), 
            .Q(n501));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i7.GSR = "ENABLED";
    FD1S3IX CurrState_SISt_FSM_i8 (.D(n1852), .CK(\led_timer[3] ), .CD(n4868), 
            .Q(n492[8]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i8.GSR = "ENABLED";
    FD1S3IX CurrState_SISt_FSM_i9 (.D(n553), .CK(\led_timer[3] ), .CD(n4868), 
            .Q(next_writeEn));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i9.GSR = "ENABLED";
    FD1S3IX CurrState_SISt_FSM_i10 (.D(n1966), .CK(\led_timer[3] ), .CD(n4868), 
            .Q(n492[10]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i10.GSR = "ENABLED";
    FD1S3IX CurrState_SISt_FSM_i11 (.D(n2116), .CK(\led_timer[3] ), .CD(startEdgeDet), 
            .Q(n492[11]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i11.GSR = "ENABLED";
    FD1S3IX CurrState_SISt_FSM_i12 (.D(n1964), .CK(\led_timer[3] ), .CD(n4868), 
            .Q(n492[12]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i12.GSR = "ENABLED";
    FD1S3IX CurrState_SISt_FSM_i13 (.D(n1860), .CK(\led_timer[3] ), .CD(startEdgeDet), 
            .Q(n492[13]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i13.GSR = "ENABLED";
    FD1S3IX CurrState_SISt_FSM_i14 (.D(n4718), .CK(\led_timer[3] ), .CD(startEdgeDet), 
            .Q(n492[14]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i14.GSR = "ENABLED";
    FD1S3IX CurrState_SISt_FSM_i15 (.D(n569), .CK(\led_timer[3] ), .CD(startEdgeDet), 
            .Q(n492[15]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam CurrState_SISt_FSM_i15.GSR = "ENABLED";
    CCU2D regAddr_7__I_0_167_7 (.A0(regAddr[5]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(regAddr[6]), .B1(GND_net), .C1(GND_net), 
          .D1(GND_net), .CIN(n4043), .COUT(n4044), .S0(next_regAddr_7__N_395[5]), 
          .S1(next_regAddr_7__N_395[6]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(314[23:37])
    defparam regAddr_7__I_0_167_7.INIT0 = 16'h5aaa;
    defparam regAddr_7__I_0_167_7.INIT1 = 16'h5aaa;
    defparam regAddr_7__I_0_167_7.INJECT1_0 = "NO";
    defparam regAddr_7__I_0_167_7.INJECT1_1 = "NO";
    LUT4 mux_373_i4_4_lut (.A(rxData[3]), .B(next_regAddr_7__N_395[3]), 
         .C(n4717), .D(n1959), .Z(n810)) /* synthesis lut_function=(!(A (B (C (D))+!B ((D)+!C))+!A ((C)+!B))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam mux_373_i4_4_lut.init = 16'h0cac;
    LUT4 i2_3_lut (.A(streamSt[1]), .B(streamSt[0]), .C(n507), .Z(n2104)) /* synthesis lut_function=(!(A+!(B (C)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i2_3_lut.init = 16'h4040;
    LUT4 i1109_4_lut_4_lut (.A(n508), .B(txData[4]), .C(n507), .D(dataFromRegIF[5]), 
         .Z(next_txData[5])) /* synthesis lut_function=(A (C (D))+!A (B ((D)+!C)+!B (C (D)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i1109_4_lut_4_lut.init = 16'hf404;
    LUT4 i1_2_lut_rep_38_3_lut_4_lut (.A(bitCnt[0]), .B(bitCnt[1]), .C(next_writeEn), 
         .D(bitCnt[2]), .Z(n4718)) /* synthesis lut_function=(A (B (C (D)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(352[3] 362[6])
    defparam i1_2_lut_rep_38_3_lut_4_lut.init = 16'h8000;
    LUT4 i2296_2_lut_3_lut_4_lut (.A(bitCnt[2]), .B(n4727), .C(rxData[4]), 
         .D(next_writeEn), .Z(n780)) /* synthesis lut_function=(A (B (C (D)))) */ ;
    defparam i2296_2_lut_3_lut_4_lut.init = 16'h8000;
    LUT4 i1_2_lut_3_lut_4_lut_adj_21 (.A(bitCnt[2]), .B(n4727), .C(rxData[0]), 
         .D(next_writeEn), .Z(n776)) /* synthesis lut_function=(A (B (C (D)))) */ ;
    defparam i1_2_lut_3_lut_4_lut_adj_21.init = 16'h8000;
    LUT4 i1_2_lut_3_lut_4_lut_adj_22 (.A(bitCnt[0]), .B(bitCnt[1]), .C(next_writeEn), 
         .D(bitCnt[2]), .Z(n8)) /* synthesis lut_function=(A (B ((D)+!C)+!B !(C))+!A !(C)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(352[3] 362[6])
    defparam i1_2_lut_3_lut_4_lut_adj_22.init = 16'h8f0f;
    LUT4 i1_3_lut (.A(n508), .B(\sclDelayed[5] ), .C(n506), .Z(n52)) /* synthesis lut_function=(A+!(B+!(C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(108[26:40])
    defparam i1_3_lut.init = 16'hbaba;
    FD1P3IX rxData__i1 (.D(n1491[1]), .SP(\led_timer[3]_enable_70 ), .CD(n4868), 
            .CK(\led_timer[3] ), .Q(rxData[1])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam rxData__i1.GSR = "ENABLED";
    CCU2D regAddr_7__I_0_167_5 (.A0(regAddr[3]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(regAddr[4]), .B1(GND_net), .C1(GND_net), 
          .D1(GND_net), .CIN(n4042), .COUT(n4043), .S0(next_regAddr_7__N_395[3]), 
          .S1(next_regAddr_7__N_395[4]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(314[23:37])
    defparam regAddr_7__I_0_167_5.INIT0 = 16'h5aaa;
    defparam regAddr_7__I_0_167_5.INIT1 = 16'h5aaa;
    defparam regAddr_7__I_0_167_5.INJECT1_0 = "NO";
    defparam regAddr_7__I_0_167_5.INJECT1_1 = "NO";
    LUT4 i1105_4_lut_4_lut (.A(n508), .B(txData[2]), .C(n507), .D(dataFromRegIF[3]), 
         .Z(next_txData[3])) /* synthesis lut_function=(A (C (D))+!A (B ((D)+!C)+!B (C (D)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i1105_4_lut_4_lut.init = 16'hf404;
    FD1P3IX rxData__i2 (.D(n1491[2]), .SP(\led_timer[3]_enable_70 ), .CD(n4868), 
            .CK(\led_timer[3] ), .Q(rxData[2])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam rxData__i2.GSR = "ENABLED";
    FD1P3IX rxData__i3 (.D(n1491[3]), .SP(\led_timer[3]_enable_70 ), .CD(startEdgeDet), 
            .CK(\led_timer[3] ), .Q(rxData[3])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam rxData__i3.GSR = "ENABLED";
    FD1P3IX rxData__i4 (.D(n1491[4]), .SP(\led_timer[3]_enable_70 ), .CD(n4868), 
            .CK(\led_timer[3] ), .Q(rxData[4])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam rxData__i4.GSR = "ENABLED";
    FD1P3IX rxData__i5 (.D(n1491[5]), .SP(\led_timer[3]_enable_70 ), .CD(n4868), 
            .CK(\led_timer[3] ), .Q(rxData[5])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam rxData__i5.GSR = "ENABLED";
    FD1P3IX rxData__i6 (.D(n1491[6]), .SP(\led_timer[3]_enable_70 ), .CD(n4868), 
            .CK(\led_timer[3] ), .Q(rxData[6])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam rxData__i6.GSR = "ENABLED";
    FD1P3IX rxData__i7 (.D(n1491[7]), .SP(\led_timer[3]_enable_70 ), .CD(n4868), 
            .CK(\led_timer[3] ), .Q(rxData[7])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam rxData__i7.GSR = "ENABLED";
    LUT4 mux_373_i3_4_lut (.A(n1959), .B(next_regAddr_7__N_395[2]), .C(n4717), 
         .D(rxData[2]), .Z(n809)) /* synthesis lut_function=(!(A ((C)+!B)+!A !(B ((D)+!C)+!B (C (D))))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam mux_373_i3_4_lut.init = 16'h5c0c;
    LUT4 mux_373_i2_4_lut (.A(rxData[1]), .B(next_regAddr_7__N_395[1]), 
         .C(n4717), .D(n1959), .Z(n808)) /* synthesis lut_function=(!(A (B (C (D))+!B ((D)+!C))+!A ((C)+!B))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam mux_373_i2_4_lut.init = 16'h0cac;
    LUT4 i2_2_lut_rep_37_3_lut_4_lut (.A(bitCnt[2]), .B(n4727), .C(n492[14]), 
         .D(next_writeEn), .Z(n4717)) /* synthesis lut_function=(!(((C+!(D))+!B)+!A)) */ ;
    defparam i2_2_lut_rep_37_3_lut_4_lut.init = 16'h0800;
    LUT4 i3425_3_lut_4_lut (.A(bitCnt[0]), .B(bitCnt[1]), .C(bitCnt[2]), 
         .D(n1218), .Z(n4340)) /* synthesis lut_function=(!(A (B (C+!(D))+!B !(C (D)))+!A !(C (D)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(352[3] 362[6])
    defparam i3425_3_lut_4_lut.init = 16'h7800;
    LUT4 i1_2_lut_4_lut (.A(bitCnt[1]), .B(bitCnt[0]), .C(bitCnt[2]), 
         .D(n504), .Z(n8_adj_559)) /* synthesis lut_function=(A (D)+!A (B (D)+!B (C (D)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(176[11:27])
    defparam i1_2_lut_4_lut.init = 16'hfe00;
    LUT4 i3615_2_lut_rep_46 (.A(streamSt[0]), .B(streamSt[1]), .Z(n4726)) /* synthesis lut_function=(!((B)+!A)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(145[11:35])
    defparam i3615_2_lut_rep_46.init = 16'h2222;
    LUT4 i4_4_lut (.A(rxData[7]), .B(n4375), .C(n4730), .D(n6_adj_560), 
         .Z(next_sdaOut_N_470)) /* synthesis lut_function=(A+((C+(D))+!B)) */ ;
    defparam i4_4_lut.init = 16'hfffb;
    LUT4 i1_2_lut_rep_40_3_lut (.A(bitCnt[0]), .B(bitCnt[1]), .C(bitCnt[2]), 
         .Z(n4720)) /* synthesis lut_function=(A (B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(352[3] 362[6])
    defparam i1_2_lut_rep_40_3_lut.init = 16'h8080;
    LUT4 i1_4_lut_adj_23 (.A(startEdgeDet), .B(n8), .C(n4315), .D(n4_adj_561), 
         .Z(led_timer_3_enable_6)) /* synthesis lut_function=(A+(B (C (D)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/i2cslave.v(94[24:34])
    defparam i1_4_lut_adj_23.init = 16'heaaa;
    LUT4 i1_2_lut_3_lut_adj_24 (.A(streamSt[0]), .B(streamSt[1]), .C(dataFromRegIF[7]), 
         .Z(n30)) /* synthesis lut_function=(!((B+!(C))+!A)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(145[11:35])
    defparam i1_2_lut_3_lut_adj_24.init = 16'h2020;
    LUT4 i2_4_lut (.A(NextState_SISt_3__N_415), .B(\sclDelayed[5] ), .C(n501), 
         .D(n4_adj_1), .Z(n4315)) /* synthesis lut_function=(A+!(B (C)+!B (C (D)))) */ ;
    defparam i2_4_lut.init = 16'hafbf;
    LUT4 i3537_4_lut (.A(n4719), .B(sdaDeb), .C(n492[6]), .D(\sclDelayed[5] ), 
         .Z(n4_adj_561)) /* synthesis lut_function=(A (B ((D)+!C)+!B !(C))+!A (B (C (D)))) */ ;
    defparam i3537_4_lut.init = 16'hca0a;
    LUT4 i2_3_lut_adj_25 (.A(startEdgeDet), .B(dataFromRegIF[0]), .C(n507), 
         .Z(n4096)) /* synthesis lut_function=(!(A+!(B (C)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam i2_3_lut_adj_25.init = 16'h4040;
    LUT4 i3458_4_lut (.A(rxData[3]), .B(rxData[5]), .C(rxData[4]), .D(rxData[6]), 
         .Z(n4375)) /* synthesis lut_function=(A (B (C (D)))) */ ;
    defparam i3458_4_lut.init = 16'h8000;
    LUT4 i2350_4_lut (.A(rxData[0]), .B(next_writeEn), .C(n4_adj_556), 
         .D(next_sdaOut_N_470), .Z(n1467[0])) /* synthesis lut_function=(!(A ((C (D))+!B)+!A ((C)+!B))) */ ;
    defparam i2350_4_lut.init = 16'h0c8c;
    LUT4 i1_2_lut_adj_26 (.A(rxData[2]), .B(rxData[1]), .Z(n6_adj_560)) /* synthesis lut_function=(A+(B)) */ ;
    defparam i1_2_lut_adj_26.init = 16'heeee;
    FD1P3AX clearStartStopDet_134 (.D(n1664), .SP(led_timer_3_enable_71), 
            .CK(\led_timer[3] ), .Q(clearStartStopDet));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam clearStartStopDet_134.GSR = "ENABLED";
    LUT4 i3634_2_lut (.A(streamSt[0]), .B(streamSt[1]), .Z(n4_adj_556)) /* synthesis lut_function=(!(A+(B))) */ ;
    defparam i3634_2_lut.init = 16'h1111;
    LUT4 i3441_3_lut (.A(n492[14]), .B(n4343), .C(startEdgeDet), .Z(led_timer_3_enable_71)) /* synthesis lut_function=(A+(B+(C))) */ ;
    defparam i3441_3_lut.init = 16'hfefe;
    FD1P3IX txData__i2 (.D(next_txData[2]), .SP(led_timer_3_enable_73), 
            .CD(startEdgeDet), .CK(\led_timer[3] ), .Q(txData[2])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam txData__i2.GSR = "ENABLED";
    LUT4 i2_4_lut_adj_27 (.A(startStopDetState[1]), .B(\sclDelayed[5] ), 
         .C(n4_adj_556), .D(startStopDetState[0]), .Z(NextState_SISt_3__N_415)) /* synthesis lut_function=(!(A (B+(D))+!A (B+((D)+!C)))) */ ;
    defparam i2_4_lut_adj_27.init = 16'h0032;
    LUT4 i29_4_lut (.A(n492[13]), .B(NextState_SISt_3__N_415), .C(n501), 
         .D(n4304), .Z(n4343)) /* synthesis lut_function=(A (B+!(C))+!A (B (C+(D))+!B !(C+!(D)))) */ ;
    defparam i29_4_lut.init = 16'hcfca;
    LUT4 i1_3_lut_adj_28 (.A(n501), .B(startEdgeDet), .C(n492[14]), .Z(n1664)) /* synthesis lut_function=(!(A (B)+!A (B+!(C)))) */ ;
    defparam i1_3_lut_adj_28.init = 16'h3232;
    LUT4 i2_3_lut_adj_29 (.A(startEdgeDet), .B(n15), .C(n508), .Z(led_timer_3_enable_74)) /* synthesis lut_function=(A+(B+(C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(352[3] 362[6])
    defparam i2_3_lut_adj_29.init = 16'hfefe;
    FD1P3IX txData__i1 (.D(next_txData[1]), .SP(led_timer_3_enable_73), 
            .CD(startEdgeDet), .CK(\led_timer[3] ), .Q(txData[1])) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam txData__i1.GSR = "ENABLED";
    LUT4 i2205_4_lut (.A(\sclDelayed[5] ), .B(n4720), .C(next_writeEn), 
         .D(n12), .Z(n15)) /* synthesis lut_function=(A (B (C))+!A (B (C+(D))+!B !(C+!(D)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(81[18:30])
    defparam i2205_4_lut.init = 16'hc5c0;
    LUT4 i1_4_lut_adj_30 (.A(n506), .B(n15_adj_563), .C(n13), .D(n14), 
         .Z(n12)) /* synthesis lut_function=(A+!(B+(C+(D)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i1_4_lut_adj_30.init = 16'haaab;
    LUT4 i1_2_lut_3_lut_adj_31 (.A(streamSt[0]), .B(streamSt[1]), .C(dataFromRegIF[6]), 
         .Z(n32)) /* synthesis lut_function=(!((B+!(C))+!A)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(145[11:35])
    defparam i1_2_lut_3_lut_adj_31.init = 16'h2020;
    CCU2D regAddr_7__I_0_167_3 (.A0(regAddr[1]), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(regAddr[2]), .B1(GND_net), .C1(GND_net), 
          .D1(GND_net), .CIN(n4041), .COUT(n4042), .S0(next_regAddr_7__N_395[1]), 
          .S1(next_regAddr_7__N_395[2]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(314[23:37])
    defparam regAddr_7__I_0_167_3.INIT0 = 16'h5aaa;
    defparam regAddr_7__I_0_167_3.INIT1 = 16'h5aaa;
    defparam regAddr_7__I_0_167_3.INJECT1_0 = "NO";
    defparam regAddr_7__I_0_167_3.INJECT1_1 = "NO";
    LUT4 i1107_4_lut_4_lut (.A(n508), .B(txData[3]), .C(n507), .D(dataFromRegIF[4]), 
         .Z(next_txData[4])) /* synthesis lut_function=(A (C (D))+!A (B ((D)+!C)+!B (C (D)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i1107_4_lut_4_lut.init = 16'hf404;
    FD1P3JX sdaOut_131 (.D(n4715), .SP(led_timer_3_enable_74), .PD(n4848), 
            .CK(\led_timer[3] ), .Q(sdaOut)) /* synthesis LSE_LINE_FILE_ID=3, LSE_LCOL=17, LSE_RCOL=2, LSE_LLINE=200, LSE_RLINE=212 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(337[8] 363[4])
    defparam sdaOut_131.GSR = "ENABLED";
    LUT4 i3150_3_lut (.A(n1218), .B(bitCnt[1]), .C(bitCnt[0]), .Z(n4016)) /* synthesis lut_function=(!((B (C)+!B !(C))+!A)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(352[3] 362[6])
    defparam i3150_3_lut.init = 16'h2828;
    LUT4 i1_4_lut_adj_32 (.A(n492[12]), .B(n4304), .C(n492[6]), .D(\sclDelayed[5] ), 
         .Z(n4306)) /* synthesis lut_function=(A (B+(C+!(D)))+!A (B+(C (D)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i1_4_lut_adj_32.init = 16'hfcee;
    LUT4 i1_2_lut_adj_33 (.A(n508), .B(n492[15]), .Z(n4304)) /* synthesis lut_function=(A+(B)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i1_2_lut_adj_33.init = 16'heeee;
    LUT4 mux_373_i8_3_lut_4_lut (.A(n492[14]), .B(n4718), .C(next_regAddr_7__N_395[7]), 
         .D(rxData[7]), .Z(n814)) /* synthesis lut_function=(A (C)+!A (B (D)+!B (C))) */ ;
    defparam mux_373_i8_3_lut_4_lut.init = 16'hf4b0;
    LUT4 i2058_3_lut_4_lut (.A(n492[14]), .B(n4718), .C(next_regAddr_7__N_395[0]), 
         .D(rxData[0]), .Z(n807)) /* synthesis lut_function=(A (C)+!A (B (D)+!B (C))) */ ;
    defparam i2058_3_lut_4_lut.init = 16'hf4b0;
    CCU2D regAddr_7__I_0_167_1 (.A0(GND_net), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(regAddr[0]), .B1(GND_net), .C1(GND_net), 
          .D1(GND_net), .COUT(n4041), .S1(next_regAddr_7__N_395[0]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(314[23:37])
    defparam regAddr_7__I_0_167_1.INIT0 = 16'hF000;
    defparam regAddr_7__I_0_167_1.INIT1 = 16'h5555;
    defparam regAddr_7__I_0_167_1.INJECT1_0 = "NO";
    defparam regAddr_7__I_0_167_1.INJECT1_1 = "NO";
    LUT4 i6_4_lut (.A(n501), .B(n492[15]), .C(n492[10]), .D(n492[14]), 
         .Z(n15_adj_563)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;
    defparam i6_4_lut.init = 16'hfffe;
    LUT4 i5_3_lut (.A(n492[13]), .B(n505), .C(n492[6]), .Z(n14)) /* synthesis lut_function=(A+(B+(C))) */ ;
    defparam i5_3_lut.init = 16'hfefe;
    LUT4 i1_2_lut_2_lut (.A(n508), .B(txData[6]), .Z(n29)) /* synthesis lut_function=(!(A+!(B))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i1_2_lut_2_lut.init = 16'h4444;
    LUT4 i3623_4_lut_else_4_lut (.A(n508), .B(startEdgeDet), .Z(n4848)) /* synthesis lut_function=(A+(B)) */ ;
    defparam i3623_4_lut_else_4_lut.init = 16'heeee;
    LUT4 i1_3_lut_adj_34 (.A(n508), .B(txData[7]), .C(n506), .Z(n1868)) /* synthesis lut_function=(A (B)+!A (B+!(C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i1_3_lut_adj_34.init = 16'hcdcd;
    LUT4 i1_2_lut_3_lut_4_lut_adj_35 (.A(bitCnt[2]), .B(n4727), .C(rxData[7]), 
         .D(next_writeEn), .Z(n783)) /* synthesis lut_function=(A (B (C (D)))) */ ;
    defparam i1_2_lut_3_lut_4_lut_adj_35.init = 16'h8000;
    LUT4 i1_2_lut_3_lut_4_lut_adj_36 (.A(bitCnt[2]), .B(n4727), .C(rxData[2]), 
         .D(next_writeEn), .Z(n778)) /* synthesis lut_function=(A (B (C (D)))) */ ;
    defparam i1_2_lut_3_lut_4_lut_adj_36.init = 16'h8000;
    LUT4 i2285_2_lut_3_lut_4_lut (.A(bitCnt[2]), .B(n4727), .C(rxData[1]), 
         .D(next_writeEn), .Z(n777)) /* synthesis lut_function=(A (B (C (D)))) */ ;
    defparam i2285_2_lut_3_lut_4_lut.init = 16'h8000;
    LUT4 i1_2_lut_2_lut_adj_37 (.A(n508), .B(txData[5]), .Z(n31)) /* synthesis lut_function=(!(A+!(B))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i1_2_lut_2_lut_adj_37.init = 16'h4444;
    LUT4 mux_373_i7_3_lut_4_lut (.A(n492[14]), .B(n4718), .C(next_regAddr_7__N_395[6]), 
         .D(rxData[6]), .Z(n813)) /* synthesis lut_function=(A (C)+!A (B (D)+!B (C))) */ ;
    defparam mux_373_i7_3_lut_4_lut.init = 16'hf4b0;
    LUT4 i1_4_lut_adj_38 (.A(n2104), .B(\sclDelayed[5] ), .C(n8_adj_559), 
         .D(n506), .Z(n4281)) /* synthesis lut_function=(A+(B (C+(D))+!B (C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i1_4_lut_adj_38.init = 16'hfefa;
    LUT4 i1_2_lut_rep_47 (.A(bitCnt[0]), .B(bitCnt[1]), .Z(n4727)) /* synthesis lut_function=(A (B)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(352[3] 362[6])
    defparam i1_2_lut_rep_47.init = 16'h8888;
    PFUMX i3754 (.BLUT(n4848), .ALUT(n4849), .C0(next_writeEn), .Z(led_timer_3_enable_38));
    LUT4 i1134_3_lut (.A(\sclDelayed[5] ), .B(n492[6]), .C(n492[5]), .Z(n1970)) /* synthesis lut_function=(!(A+!(B+(C)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i1134_3_lut.init = 16'h5454;
    PFUMX i3712 (.BLUT(n4732), .ALUT(n4733), .C0(next_writeEn), .Z(led_timer_3_enable_39));
    LUT4 next_sdaOut_N_470_bdd_4_lut (.A(next_sdaOut_N_470), .B(n4_adj_556), 
         .C(n1868), .D(next_writeEn), .Z(n4715)) /* synthesis lut_function=(A (B (C+(D))+!B !((D)+!C))+!A !((D)+!C)) */ ;
    defparam next_sdaOut_N_470_bdd_4_lut.init = 16'h88f0;
    LUT4 i2943_4_lut_4_lut (.A(n508), .B(txData[1]), .C(n507), .D(dataFromRegIF[2]), 
         .Z(next_txData[2])) /* synthesis lut_function=(A (C (D))+!A (B ((D)+!C)+!B (C (D)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/serialinterface.v(130[3] 324[10])
    defparam i2943_4_lut_4_lut.init = 16'hf404;
    
endmodule
//
// Verilog Description of module drehimpulsgeber
//

module drehimpulsgeber (pin_enc_a_c, \state_2__N_492[1] , \led_timer[5] , 
            enc_counter, GND_net);
    input pin_enc_a_c;
    input \state_2__N_492[1] ;
    input \led_timer[5] ;
    output [7:0]enc_counter;
    input GND_net;
    
    wire \led_timer[5]  /* synthesis SET_AS_NETWORK=led_timer[5], is_clock=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(208[9:18])
    wire [2:0]state;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/drehimpulsgeber.vhd(31[9:14])
    
    wire n4488, n4736, led_timer_5_enable_6;
    wire [2:0]state_2__N_486;
    
    wire n4735, n2449, n4489, n4, led_timer_5_enable_10, n3285;
    wire [7:0]n37;
    
    wire n4056, n4055, n4694, n4695, n4661, n4660, n4054, n4053, 
        led_timer_5_enable_11;
    
    LUT4 i3589_1_lut_3_lut_4_lut (.A(state[0]), .B(pin_enc_a_c), .C(\state_2__N_492[1] ), 
         .D(state[2]), .Z(n4488)) /* synthesis lut_function=(A (B+(C (D)+!C !(D)))+!A ((C (D)+!C !(D))+!B)) */ ;
    defparam i3589_1_lut_3_lut_4_lut.init = 16'hf99f;
    LUT4 i57_4_lut_then_4_lut (.A(\state_2__N_492[1] ), .B(pin_enc_a_c), 
         .C(state[1]), .D(state[2]), .Z(n4736)) /* synthesis lut_function=(A (B+(C+(D)))+!A (B ((D)+!C)+!B (C+!(D)))) */ ;
    defparam i57_4_lut_then_4_lut.init = 16'hfebd;
    FD1P3AX state_i0 (.D(state_2__N_486[0]), .SP(led_timer_5_enable_6), 
            .CK(\led_timer[5] ), .Q(state[0])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=23, LSE_RCOL=38, LSE_LLINE=372, LSE_RLINE=372 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/drehimpulsgeber.vhd(39[3] 95[10])
    defparam state_i0.GSR = "ENABLED";
    LUT4 i57_4_lut_else_4_lut (.A(\state_2__N_492[1] ), .B(pin_enc_a_c), 
         .C(state[1]), .D(state[2]), .Z(n4735)) /* synthesis lut_function=(A (B (C+(D))+!B !(C (D)))+!A (B (C+!(D))+!B ((D)+!C))) */ ;
    defparam i57_4_lut_else_4_lut.init = 16'hdbe7;
    LUT4 i2320_4_lut (.A(n2449), .B(state[0]), .C(\state_2__N_492[1] ), 
         .D(state[2]), .Z(state_2__N_486[0])) /* synthesis lut_function=(!(A (B+(C (D)))+!A (B+(C+!(D))))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/drehimpulsgeber.vhd(43[4] 94[13])
    defparam i2320_4_lut.init = 16'h0322;
    LUT4 i2309_2_lut (.A(pin_enc_a_c), .B(state[1]), .Z(n2449)) /* synthesis lut_function=((B)+!A) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/drehimpulsgeber.vhd(43[4] 94[13])
    defparam i2309_2_lut.init = 16'hdddd;
    PFUMX i70 (.BLUT(n4488), .ALUT(n4489), .C0(state[1]), .Z(led_timer_5_enable_6));
    LUT4 i2_4_lut (.A(pin_enc_a_c), .B(n4), .C(state[0]), .D(\state_2__N_492[1] ), 
         .Z(led_timer_5_enable_10)) /* synthesis lut_function=(A (B (C+(D)))+!A (B (C))) */ ;
    defparam i2_4_lut.init = 16'hc8c0;
    LUT4 i1_4_lut (.A(state[1]), .B(state[2]), .C(pin_enc_a_c), .D(state[0]), 
         .Z(n4)) /* synthesis lut_function=(A (B+(C (D)))) */ ;
    defparam i1_4_lut.init = 16'ha888;
    LUT4 i2409_4_lut (.A(led_timer_5_enable_10), .B(state[1]), .C(state[2]), 
         .D(state[0]), .Z(n3285)) /* synthesis lut_function=(A (B (C (D)))) */ ;
    defparam i2409_4_lut.init = 16'h8000;
    FD1P3IX counter_int_801__i7 (.D(n37[7]), .SP(led_timer_5_enable_10), 
            .CD(n3285), .CK(\led_timer[5] ), .Q(enc_counter[7])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/drehimpulsgeber.vhd(43[4] 94[13])
    defparam counter_int_801__i7.GSR = "ENABLED";
    FD1P3IX counter_int_801__i6 (.D(n37[6]), .SP(led_timer_5_enable_10), 
            .CD(n3285), .CK(\led_timer[5] ), .Q(enc_counter[6])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/drehimpulsgeber.vhd(43[4] 94[13])
    defparam counter_int_801__i6.GSR = "ENABLED";
    FD1P3IX counter_int_801__i5 (.D(n37[5]), .SP(led_timer_5_enable_10), 
            .CD(n3285), .CK(\led_timer[5] ), .Q(enc_counter[5])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/drehimpulsgeber.vhd(43[4] 94[13])
    defparam counter_int_801__i5.GSR = "ENABLED";
    FD1P3IX counter_int_801__i0 (.D(n37[0]), .SP(led_timer_5_enable_10), 
            .CD(n3285), .CK(\led_timer[5] ), .Q(enc_counter[0])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/drehimpulsgeber.vhd(43[4] 94[13])
    defparam counter_int_801__i0.GSR = "ENABLED";
    FD1P3AX state_i2 (.D(state_2__N_486[2]), .SP(led_timer_5_enable_6), 
            .CK(\led_timer[5] ), .Q(state[2])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=23, LSE_RCOL=38, LSE_LLINE=372, LSE_RLINE=372 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/drehimpulsgeber.vhd(39[3] 95[10])
    defparam state_i2.GSR = "ENABLED";
    FD1P3IX counter_int_801__i4 (.D(n37[4]), .SP(led_timer_5_enable_10), 
            .CD(n3285), .CK(\led_timer[5] ), .Q(enc_counter[4])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/drehimpulsgeber.vhd(43[4] 94[13])
    defparam counter_int_801__i4.GSR = "ENABLED";
    FD1P3IX counter_int_801__i3 (.D(n37[3]), .SP(led_timer_5_enable_10), 
            .CD(n3285), .CK(\led_timer[5] ), .Q(enc_counter[3])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/drehimpulsgeber.vhd(43[4] 94[13])
    defparam counter_int_801__i3.GSR = "ENABLED";
    FD1P3IX counter_int_801__i2 (.D(n37[2]), .SP(led_timer_5_enable_10), 
            .CD(n3285), .CK(\led_timer[5] ), .Q(enc_counter[2])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/drehimpulsgeber.vhd(43[4] 94[13])
    defparam counter_int_801__i2.GSR = "ENABLED";
    FD1P3IX counter_int_801__i1 (.D(n37[1]), .SP(led_timer_5_enable_10), 
            .CD(n3285), .CK(\led_timer[5] ), .Q(enc_counter[1])) /* synthesis syn_use_carry_chain=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/drehimpulsgeber.vhd(43[4] 94[13])
    defparam counter_int_801__i1.GSR = "ENABLED";
    CCU2D counter_int_801_add_4_9 (.A0(state[2]), .B0(enc_counter[7]), .C0(GND_net), 
          .D0(GND_net), .A1(GND_net), .B1(GND_net), .C1(GND_net), .D1(GND_net), 
          .CIN(n4056), .S0(n37[7]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/drehimpulsgeber.vhd(43[4] 94[13])
    defparam counter_int_801_add_4_9.INIT0 = 16'h5666;
    defparam counter_int_801_add_4_9.INIT1 = 16'h0000;
    defparam counter_int_801_add_4_9.INJECT1_0 = "NO";
    defparam counter_int_801_add_4_9.INJECT1_1 = "NO";
    CCU2D counter_int_801_add_4_7 (.A0(state[2]), .B0(enc_counter[5]), .C0(GND_net), 
          .D0(GND_net), .A1(state[2]), .B1(enc_counter[6]), .C1(GND_net), 
          .D1(GND_net), .CIN(n4055), .COUT(n4056), .S0(n37[5]), .S1(n37[6]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/drehimpulsgeber.vhd(43[4] 94[13])
    defparam counter_int_801_add_4_7.INIT0 = 16'h5666;
    defparam counter_int_801_add_4_7.INIT1 = 16'h5666;
    defparam counter_int_801_add_4_7.INJECT1_0 = "NO";
    defparam counter_int_801_add_4_7.INJECT1_1 = "NO";
    LUT4 state_2__N_492_1__bdd_2_lut (.A(state[1]), .B(state[2]), .Z(n4694)) /* synthesis lut_function=(!(A+!(B))) */ ;
    defparam state_2__N_492_1__bdd_2_lut.init = 16'h4444;
    LUT4 state_2__N_492_1__bdd_4_lut (.A(\state_2__N_492[1] ), .B(state[1]), 
         .C(pin_enc_a_c), .D(state[2]), .Z(n4695)) /* synthesis lut_function=(!(A (B+((D)+!C))+!A !(B (D)+!B (C+(D))))) */ ;
    defparam state_2__N_492_1__bdd_4_lut.init = 16'h5530;
    PFUMX i3689 (.BLUT(n4661), .ALUT(n4660), .C0(state[2]), .Z(state_2__N_486[1]));
    CCU2D counter_int_801_add_4_5 (.A0(state[2]), .B0(enc_counter[3]), .C0(GND_net), 
          .D0(GND_net), .A1(state[2]), .B1(enc_counter[4]), .C1(GND_net), 
          .D1(GND_net), .CIN(n4054), .COUT(n4055), .S0(n37[3]), .S1(n37[4]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/drehimpulsgeber.vhd(43[4] 94[13])
    defparam counter_int_801_add_4_5.INIT0 = 16'h5666;
    defparam counter_int_801_add_4_5.INIT1 = 16'h5666;
    defparam counter_int_801_add_4_5.INJECT1_0 = "NO";
    defparam counter_int_801_add_4_5.INJECT1_1 = "NO";
    CCU2D counter_int_801_add_4_3 (.A0(state[2]), .B0(enc_counter[1]), .C0(GND_net), 
          .D0(GND_net), .A1(state[2]), .B1(enc_counter[2]), .C1(GND_net), 
          .D1(GND_net), .CIN(n4053), .COUT(n4054), .S0(n37[1]), .S1(n37[2]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/drehimpulsgeber.vhd(43[4] 94[13])
    defparam counter_int_801_add_4_3.INIT0 = 16'h5666;
    defparam counter_int_801_add_4_3.INIT1 = 16'h5666;
    defparam counter_int_801_add_4_3.INJECT1_0 = "NO";
    defparam counter_int_801_add_4_3.INJECT1_1 = "NO";
    FD1P3AX state_i1 (.D(state_2__N_486[1]), .SP(led_timer_5_enable_11), 
            .CK(\led_timer[5] ), .Q(state[1])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=23, LSE_RCOL=38, LSE_LLINE=372, LSE_RLINE=372 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/drehimpulsgeber.vhd(39[3] 95[10])
    defparam state_i1.GSR = "ENABLED";
    CCU2D counter_int_801_add_4_1 (.A0(GND_net), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(\state_2__N_492[1] ), .B1(state[2]), .C1(enc_counter[0]), 
          .D1(GND_net), .COUT(n4053), .S1(n37[0]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/drehimpulsgeber.vhd(43[4] 94[13])
    defparam counter_int_801_add_4_1.INIT0 = 16'hF000;
    defparam counter_int_801_add_4_1.INIT1 = 16'h1e1e;
    defparam counter_int_801_add_4_1.INJECT1_0 = "NO";
    defparam counter_int_801_add_4_1.INJECT1_1 = "NO";
    LUT4 state_1__bdd_3_lut (.A(state[1]), .B(state[0]), .C(pin_enc_a_c), 
         .Z(n4661)) /* synthesis lut_function=(!(A (B (C)+!B !(C))+!A ((C)+!B))) */ ;
    defparam state_1__bdd_3_lut.init = 16'h2c2c;
    LUT4 state_1__bdd_3_lut_3688 (.A(state[1]), .B(state[0]), .C(\state_2__N_492[1] ), 
         .Z(n4660)) /* synthesis lut_function=(!(A+!(B (C)))) */ ;
    defparam state_1__bdd_3_lut_3688.init = 16'h4040;
    PFUMX i3700 (.BLUT(n4695), .ALUT(n4694), .C0(state[0]), .Z(state_2__N_486[2]));
    LUT4 i3590_1_lut_4_lut (.A(\state_2__N_492[1] ), .B(pin_enc_a_c), .C(state[0]), 
         .D(state[2]), .Z(n4489)) /* synthesis lut_function=(A (B+(C+!(D)))+!A (B ((D)+!C)+!B (C+(D)))) */ ;
    defparam i3590_1_lut_4_lut.init = 16'hfdbe;
    PFUMX i3714 (.BLUT(n4735), .ALUT(n4736), .C0(state[0]), .Z(led_timer_5_enable_11));
    
endmodule
//
// Verilog Description of module TSALL
// module not written out since it is a black-box. 
//

//
// Verilog Description of module PUR
// module not written out since it is a black-box. 
//

//
// Verilog Description of module pll
//

module pll (clk_mst, clk_int, GND_net) /* synthesis NGD_DRC_MASK=1 */ ;
    input clk_mst;
    output clk_int;
    input GND_net;
    
    wire clk_mst /* synthesis is_clock=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(206[9:16])
    wire clk_int /* synthesis SET_AS_NETWORK=clk_int, is_clock=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(207[9:16])
    
    EHXPLLJ PLLInst_0 (.CLKI(clk_mst), .CLKFB(clk_int), .PHASESEL0(GND_net), 
            .PHASESEL1(GND_net), .PHASEDIR(GND_net), .PHASESTEP(GND_net), 
            .LOADREG(GND_net), .STDBY(GND_net), .PLLWAKESYNC(GND_net), 
            .RST(GND_net), .RESETC(GND_net), .RESETD(GND_net), .RESETM(GND_net), 
            .ENCLKOP(GND_net), .ENCLKOS(GND_net), .ENCLKOS2(GND_net), 
            .ENCLKOS3(GND_net), .PLLCLK(GND_net), .PLLRST(GND_net), .PLLSTB(GND_net), 
            .PLLWE(GND_net), .PLLDATI0(GND_net), .PLLDATI1(GND_net), .PLLDATI2(GND_net), 
            .PLLDATI3(GND_net), .PLLDATI4(GND_net), .PLLDATI5(GND_net), 
            .PLLDATI6(GND_net), .PLLDATI7(GND_net), .PLLADDR0(GND_net), 
            .PLLADDR1(GND_net), .PLLADDR2(GND_net), .PLLADDR3(GND_net), 
            .PLLADDR4(GND_net), .CLKOP(clk_int)) /* synthesis FREQUENCY_PIN_CLKOP="266.000000", FREQUENCY_PIN_CLKI="133.000000", ICP_CURRENT="10", LPF_RESISTOR="24", syn_instantiated=1, LSE_LINE_FILE_ID=28, LSE_LCOL=11, LSE_RCOL=14, LSE_LLINE=427, LSE_RLINE=427 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(427[11:14])
    defparam PLLInst_0.CLKI_DIV = 1;
    defparam PLLInst_0.CLKFB_DIV = 2;
    defparam PLLInst_0.CLKOP_DIV = 2;
    defparam PLLInst_0.CLKOS_DIV = 1;
    defparam PLLInst_0.CLKOS2_DIV = 1;
    defparam PLLInst_0.CLKOS3_DIV = 1;
    defparam PLLInst_0.CLKOP_ENABLE = "ENABLED";
    defparam PLLInst_0.CLKOS_ENABLE = "DISABLED";
    defparam PLLInst_0.CLKOS2_ENABLE = "DISABLED";
    defparam PLLInst_0.CLKOS3_ENABLE = "DISABLED";
    defparam PLLInst_0.VCO_BYPASS_A0 = "DISABLED";
    defparam PLLInst_0.VCO_BYPASS_B0 = "DISABLED";
    defparam PLLInst_0.VCO_BYPASS_C0 = "DISABLED";
    defparam PLLInst_0.VCO_BYPASS_D0 = "DISABLED";
    defparam PLLInst_0.CLKOP_CPHASE = 1;
    defparam PLLInst_0.CLKOS_CPHASE = 0;
    defparam PLLInst_0.CLKOS2_CPHASE = 0;
    defparam PLLInst_0.CLKOS3_CPHASE = 0;
    defparam PLLInst_0.CLKOP_FPHASE = 0;
    defparam PLLInst_0.CLKOS_FPHASE = 0;
    defparam PLLInst_0.CLKOS2_FPHASE = 0;
    defparam PLLInst_0.CLKOS3_FPHASE = 0;
    defparam PLLInst_0.FEEDBK_PATH = "CLKOP";
    defparam PLLInst_0.FRACN_ENABLE = "DISABLED";
    defparam PLLInst_0.FRACN_DIV = 0;
    defparam PLLInst_0.CLKOP_TRIM_POL = "RISING";
    defparam PLLInst_0.CLKOP_TRIM_DELAY = 0;
    defparam PLLInst_0.CLKOS_TRIM_POL = "FALLING";
    defparam PLLInst_0.CLKOS_TRIM_DELAY = 0;
    defparam PLLInst_0.PLL_USE_WB = "DISABLED";
    defparam PLLInst_0.PREDIVIDER_MUXA1 = 0;
    defparam PLLInst_0.PREDIVIDER_MUXB1 = 0;
    defparam PLLInst_0.PREDIVIDER_MUXC1 = 0;
    defparam PLLInst_0.PREDIVIDER_MUXD1 = 0;
    defparam PLLInst_0.OUTDIVIDER_MUXA2 = "DIVA";
    defparam PLLInst_0.OUTDIVIDER_MUXB2 = "DIVB";
    defparam PLLInst_0.OUTDIVIDER_MUXC2 = "DIVC";
    defparam PLLInst_0.OUTDIVIDER_MUXD2 = "DIVD";
    defparam PLLInst_0.PLL_LOCK_MODE = 0;
    defparam PLLInst_0.STDBY_ENABLE = "DISABLED";
    defparam PLLInst_0.DPHASE_SOURCE = "DISABLED";
    defparam PLLInst_0.PLLRST_ENA = "DISABLED";
    defparam PLLInst_0.MRST_ENA = "DISABLED";
    defparam PLLInst_0.DCRST_ENA = "DISABLED";
    defparam PLLInst_0.DDRST_ENA = "DISABLED";
    defparam PLLInst_0.INTFB_WAKE = "DISABLED";
    
endmodule
//
// Verilog Description of module LCD
//

module LCD (\led_timer[10] , i2c_done, regAddr, pin_lcd_d_0_c_0, data, 
            pin_lcd_rs_c, pin_lcd_strb_c, n4852, pin_lcd_d_7_c_7, pin_lcd_d_6_c_6, 
            pin_lcd_d_5_c_5, pin_lcd_d_4_c_4, pin_lcd_d_3_c_3, pin_lcd_d_2_c_2, 
            pin_lcd_d_1_c_1);
    input \led_timer[10] ;
    input i2c_done;
    input [7:0]regAddr;
    output pin_lcd_d_0_c_0;
    input [7:0]data;
    output pin_lcd_rs_c;
    output pin_lcd_strb_c;
    input n4852;
    output pin_lcd_d_7_c_7;
    output pin_lcd_d_6_c_6;
    output pin_lcd_d_5_c_5;
    output pin_lcd_d_4_c_4;
    output pin_lcd_d_3_c_3;
    output pin_lcd_d_2_c_2;
    output pin_lcd_d_1_c_1;
    
    wire \led_timer[10]  /* synthesis is_clock=1, SET_AS_NETWORK=led_timer[10] */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(208[9:18])
    wire i2c_done /* synthesis is_clock=1, SET_AS_NETWORK=i2c_done */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(219[9:17])
    wire [3:0]n409;
    
    wire n2127;
    wire [7:0]addr_int;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(32[9:17])
    
    wire led_timer_10_enable_3, led_timer_10_enable_1, n1, n2644, rst_send, 
        bufferd_send, n4729, n12, n2479;
    
    FD1S3JX state_FSM_i0 (.D(n2127), .CK(\led_timer[10] ), .PD(n409[3]), 
            .Q(n409[0]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(49[4] 78[13])
    defparam state_FSM_i0.GSR = "ENABLED";
    FD1S3AX addr_int_i0 (.D(regAddr[0]), .CK(i2c_done), .Q(addr_int[0])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=11, LSE_RCOL=14, LSE_LLINE=383, LSE_RLINE=383 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(41[3] 44[10])
    defparam addr_int_i0.GSR = "ENABLED";
    FD1S3AX LCD_Bus_int_i1 (.D(data[0]), .CK(i2c_done), .Q(pin_lcd_d_0_c_0)) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=11, LSE_RCOL=14, LSE_LLINE=383, LSE_RLINE=383 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(41[3] 44[10])
    defparam LCD_Bus_int_i1.GSR = "ENABLED";
    LUT4 i1_2_lut (.A(n409[1]), .B(n409[0]), .Z(led_timer_10_enable_3)) /* synthesis lut_function=(A+(B)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(49[4] 78[13])
    defparam i1_2_lut.init = 16'heeee;
    FD1P3AX RS_int_30 (.D(n1), .SP(led_timer_10_enable_1), .CK(\led_timer[10] ), 
            .Q(pin_lcd_rs_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(48[3] 79[10])
    defparam RS_int_30.GSR = "ENABLED";
    FD1P3IX E_int_31 (.D(n4852), .SP(n409[2]), .CD(n409[3]), .CK(\led_timer[10] ), 
            .Q(pin_lcd_strb_c));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(48[3] 79[10])
    defparam E_int_31.GSR = "ENABLED";
    LUT4 i1_2_lut_adj_16 (.A(n409[1]), .B(n2644), .Z(led_timer_10_enable_1)) /* synthesis lut_function=(A (B)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(49[4] 78[13])
    defparam i1_2_lut_adj_16.init = 16'h8888;
    FD1P3IX rst_send_29 (.D(bufferd_send), .SP(led_timer_10_enable_3), .CD(n409[1]), 
            .CK(\led_timer[10] ), .Q(rst_send));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(48[3] 79[10])
    defparam rst_send_29.GSR = "ENABLED";
    LUT4 i889_1_lut (.A(addr_int[0]), .Z(n1)) /* synthesis lut_function=(!(A)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(63[12:17])
    defparam i889_1_lut.init = 16'h5555;
    FD1S3AX LCD_Bus_int_i8 (.D(data[7]), .CK(i2c_done), .Q(pin_lcd_d_7_c_7)) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=11, LSE_RCOL=14, LSE_LLINE=383, LSE_RLINE=383 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(41[3] 44[10])
    defparam LCD_Bus_int_i8.GSR = "ENABLED";
    FD1S3AX LCD_Bus_int_i7 (.D(data[6]), .CK(i2c_done), .Q(pin_lcd_d_6_c_6)) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=11, LSE_RCOL=14, LSE_LLINE=383, LSE_RLINE=383 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(41[3] 44[10])
    defparam LCD_Bus_int_i7.GSR = "ENABLED";
    FD1S3AX LCD_Bus_int_i6 (.D(data[5]), .CK(i2c_done), .Q(pin_lcd_d_5_c_5)) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=11, LSE_RCOL=14, LSE_LLINE=383, LSE_RLINE=383 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(41[3] 44[10])
    defparam LCD_Bus_int_i6.GSR = "ENABLED";
    FD1S3AX LCD_Bus_int_i5 (.D(data[4]), .CK(i2c_done), .Q(pin_lcd_d_4_c_4)) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=11, LSE_RCOL=14, LSE_LLINE=383, LSE_RLINE=383 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(41[3] 44[10])
    defparam LCD_Bus_int_i5.GSR = "ENABLED";
    FD1S3AX LCD_Bus_int_i4 (.D(data[3]), .CK(i2c_done), .Q(pin_lcd_d_3_c_3)) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=11, LSE_RCOL=14, LSE_LLINE=383, LSE_RLINE=383 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(41[3] 44[10])
    defparam LCD_Bus_int_i4.GSR = "ENABLED";
    FD1S3AX LCD_Bus_int_i3 (.D(data[2]), .CK(i2c_done), .Q(pin_lcd_d_2_c_2)) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=11, LSE_RCOL=14, LSE_LLINE=383, LSE_RLINE=383 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(41[3] 44[10])
    defparam LCD_Bus_int_i3.GSR = "ENABLED";
    FD1S3AX LCD_Bus_int_i2 (.D(data[1]), .CK(i2c_done), .Q(pin_lcd_d_1_c_1)) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=11, LSE_RCOL=14, LSE_LLINE=383, LSE_RLINE=383 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(41[3] 44[10])
    defparam LCD_Bus_int_i2.GSR = "ENABLED";
    FD1S3AX addr_int_i7 (.D(regAddr[7]), .CK(i2c_done), .Q(addr_int[7])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=11, LSE_RCOL=14, LSE_LLINE=383, LSE_RLINE=383 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(41[3] 44[10])
    defparam addr_int_i7.GSR = "ENABLED";
    FD1S3AX addr_int_i6 (.D(regAddr[6]), .CK(i2c_done), .Q(addr_int[6])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=11, LSE_RCOL=14, LSE_LLINE=383, LSE_RLINE=383 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(41[3] 44[10])
    defparam addr_int_i6.GSR = "ENABLED";
    FD1S3AX addr_int_i5 (.D(regAddr[5]), .CK(i2c_done), .Q(addr_int[5])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=11, LSE_RCOL=14, LSE_LLINE=383, LSE_RLINE=383 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(41[3] 44[10])
    defparam addr_int_i5.GSR = "ENABLED";
    FD1S3AX addr_int_i4 (.D(regAddr[4]), .CK(i2c_done), .Q(addr_int[4])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=11, LSE_RCOL=14, LSE_LLINE=383, LSE_RLINE=383 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(41[3] 44[10])
    defparam addr_int_i4.GSR = "ENABLED";
    FD1S3AX addr_int_i3 (.D(regAddr[3]), .CK(i2c_done), .Q(addr_int[3])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=11, LSE_RCOL=14, LSE_LLINE=383, LSE_RLINE=383 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(41[3] 44[10])
    defparam addr_int_i3.GSR = "ENABLED";
    FD1S3AX addr_int_i2 (.D(regAddr[2]), .CK(i2c_done), .Q(addr_int[2])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=11, LSE_RCOL=14, LSE_LLINE=383, LSE_RLINE=383 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(41[3] 44[10])
    defparam addr_int_i2.GSR = "ENABLED";
    FD1S3AX addr_int_i1 (.D(regAddr[1]), .CK(i2c_done), .Q(addr_int[1])) /* synthesis LSE_LINE_FILE_ID=28, LSE_LCOL=11, LSE_RCOL=14, LSE_LLINE=383, LSE_RLINE=383 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(41[3] 44[10])
    defparam addr_int_i1.GSR = "ENABLED";
    FD1S3AX state_FSM_i3 (.D(n409[2]), .CK(\led_timer[10] ), .Q(n409[3]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(49[4] 78[13])
    defparam state_FSM_i3.GSR = "ENABLED";
    FD1S3IX state_FSM_i1 (.D(n409[0]), .CK(\led_timer[10] ), .CD(n4729), 
            .Q(n409[1]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(49[4] 78[13])
    defparam state_FSM_i1.GSR = "ENABLED";
    LUT4 i3611_4_lut (.A(addr_int[5]), .B(n12), .C(addr_int[2]), .D(addr_int[1]), 
         .Z(n2644)) /* synthesis lut_function=(!(A+(B+(C+(D))))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(41[3] 44[10])
    defparam i3611_4_lut.init = 16'h0001;
    LUT4 i5_4_lut (.A(addr_int[7]), .B(addr_int[3]), .C(addr_int[6]), 
         .D(addr_int[4]), .Z(n12)) /* synthesis lut_function=(A+(B+(C+!(D)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(41[3] 44[10])
    defparam i5_4_lut.init = 16'hfeff;
    LUT4 i1606_1_lut (.A(n409[1]), .Z(n2479)) /* synthesis lut_function=(!(A)) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(49[4] 78[13])
    defparam i1606_1_lut.init = 16'h5555;
    FD1S3IX state_FSM_i2 (.D(n2644), .CK(\led_timer[10] ), .CD(n2479), 
            .Q(n409[2]));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(49[4] 78[13])
    defparam state_FSM_i2.GSR = "ENABLED";
    special_flipflop special_flipflop_inst (.i2c_done(i2c_done), .bufferd_send(bufferd_send), 
            .n4729(n4729), .n412(n409[1]), .n2644(n2644), .n413(n409[0]), 
            .n2127(n2127), .rst_send(rst_send));   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(92[25:41])
    
endmodule
//
// Verilog Description of module special_flipflop
//

module special_flipflop (i2c_done, bufferd_send, n4729, n412, n2644, 
            n413, n2127, rst_send);
    input i2c_done;
    output bufferd_send;
    output n4729;
    input n412;
    input n2644;
    input n413;
    output n2127;
    input rst_send;
    
    wire i2c_done /* synthesis is_clock=1, SET_AS_NETWORK=i2c_done */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(219[9:17])
    
    wire R_int;
    
    LUT4 i952_3_lut_rep_49 (.A(R_int), .B(i2c_done), .C(bufferd_send), 
         .Z(n4729)) /* synthesis lut_function=(A+!(B+(C))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/special_flipflop.vhd(26[8:22])
    defparam i952_3_lut_rep_49.init = 16'habab;
    LUT4 i33_4_lut (.A(n412), .B(bufferd_send), .C(n2644), .D(n413), 
         .Z(n2127)) /* synthesis lut_function=(!(A (B (C)+!B !((D)+!C))+!A (B+!(D)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/lcd.vhd(49[4] 78[13])
    defparam i33_4_lut.init = 16'h3b0a;
    LUT4 i953_1_lut_3_lut (.A(R_int), .B(i2c_done), .C(bufferd_send), 
         .Z(bufferd_send)) /* synthesis lut_function=(!(A+!(B+(C)))) */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/special_flipflop.vhd(26[8:22])
    defparam i953_1_lut_3_lut.init = 16'h5454;
    LUT4 i2398_3_lut (.A(rst_send), .B(i2c_done), .C(R_int), .Z(R_int)) /* synthesis lut_function=(A+(B (C))) */ ;
    defparam i2398_3_lut.init = 16'heaea;
    
endmodule
//
// Verilog Description of module fifo
//

module fifo (pin_spi_0_mosi_c_0, pin_spi_0_sclk_c, pin_out_1_c, pin_gpio_05_c, 
            VCC_net, pin_ta_1_c_0, GND_net, pin_out_2_c_0, \led_output[7] , 
            \led_output[4] , pin_gpio_06_c_5, pin_gpio_26_c_6) /* synthesis NGD_DRC_MASK=1 */ ;
    input pin_spi_0_mosi_c_0;
    input pin_spi_0_sclk_c;
    input pin_out_1_c;
    input pin_gpio_05_c;
    input VCC_net;
    input pin_ta_1_c_0;
    input GND_net;
    output pin_out_2_c_0;
    output \led_output[7] ;
    output \led_output[4] ;
    output pin_gpio_06_c_5;
    output pin_gpio_26_c_6;
    
    wire pin_spi_0_sclk_c /* synthesis is_clock=1 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(26[3:17])
    
    FIFO8KB fifo_0_0 (.DI0(GND_net), .DI1(pin_spi_0_mosi_c_0), .DI2(GND_net), 
            .DI3(GND_net), .DI4(GND_net), .DI5(GND_net), .DI6(GND_net), 
            .DI7(GND_net), .DI8(GND_net), .DI9(GND_net), .DI10(GND_net), 
            .DI11(pin_spi_0_mosi_c_0), .DI12(GND_net), .DI13(GND_net), 
            .DI14(GND_net), .DI15(GND_net), .DI16(GND_net), .DI17(GND_net), 
            .FULLI(\led_output[4] ), .EMPTYI(\led_output[7] ), .CSW1(VCC_net), 
            .CSW0(VCC_net), .CSR1(VCC_net), .CSR0(VCC_net), .WE(pin_gpio_05_c), 
            .RE(VCC_net), .ORE(VCC_net), .CLKW(pin_spi_0_sclk_c), .CLKR(pin_out_1_c), 
            .RST(pin_ta_1_c_0), .RPRST(GND_net), .DO0(pin_out_2_c_0), 
            .EF(\led_output[7] ), .AEF(pin_gpio_06_c_5), .AFF(pin_gpio_26_c_6), 
            .FF(\led_output[4] )) /* synthesis syn_instantiated=1, LSE_LINE_FILE_ID=28, LSE_LCOL=12, LSE_RCOL=16, LSE_LLINE=394, LSE_RLINE=394 */ ;   // c:/users/nils/dropbox/thm/projektarbeit/fpga/template_a2_3-tqfp100_vhdl_2_4/tinyfpga_a2.vhd(394[12:16])
    defparam fifo_0_0.DATA_WIDTH_W = 1;
    defparam fifo_0_0.DATA_WIDTH_R = 1;
    defparam fifo_0_0.REGMODE = "NOREG";
    defparam fifo_0_0.RESETMODE = "ASYNC";
    defparam fifo_0_0.ASYNC_RESET_RELEASE = "SYNC";
    defparam fifo_0_0.CSDECODE_W = "0b11";
    defparam fifo_0_0.CSDECODE_R = "0b11";
    defparam fifo_0_0.AEPOINTER = "0b00100111000100";
    defparam fifo_0_0.AEPOINTER1 = "0b00100111000101";
    defparam fifo_0_0.AFPOINTER = "0b01011000111100";
    defparam fifo_0_0.AFPOINTER1 = "0b01011000111011";
    defparam fifo_0_0.FULLPOINTER = "0b10000000000000";
    defparam fifo_0_0.FULLPOINTER1 = "0b01111111111111";
    defparam fifo_0_0.GSR = "DISABLED";
    
endmodule
