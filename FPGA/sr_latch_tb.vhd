library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity S_R_latch_top_tb is
end entity S_R_latch_top_tb;

architecture behavioral of S_R_latch_top_tb is

	component S_R_latch_top is
		Port (
			S	: in	STD_LOGIC;
			R	: in	STD_LOGIC;
			Q	: out	STD_LOGIC
		);
	end component;

	constant TIME_DELTA : time := 100 ns;
--	constant TIME_DELTA_CLK : time := 10 ns;

--	signal	clk			: std_logic := '0';
	signal	S			: std_logic := '0';
	signal	R			: std_logic := '0';

	signal	Q			: std_logic;	

begin


--	clk   <= not clk  after TIME_DELTA_CLK;

	simulation: process
	begin

	S <= '0';
	R <= '0';

	wait for TIME_DELTA;

	S <= '1';
	R <= '0';

	wait for TIME_DELTA;

	S <= '0';
	R <= '0';

	wait for TIME_DELTA;

	S <= '0';
	R <= '1';

	wait for TIME_DELTA;

	S <= '0';
	R <= '0';

	wait for TIME_DELTA;

	S <= '0';
	R <= '0';

	wait for TIME_DELTA;

	S <= '1';
	R <= '1';

	wait for TIME_DELTA;

	S <= '0';
	R <= '0';

	wait for TIME_DELTA;

	S <= '1';
	R <= '0';

	wait for TIME_DELTA;

	S <= '1';
	R <= '1';

	wait for TIME_DELTA;

	S <= '1';
	R <= '0';

	wait for TIME_DELTA;

	S <= '0';
	R <= '0';

	wait for TIME_DELTA;

	S <= '0';
	R <= '0';

	wait for TIME_DELTA;

	S <= '0';
	R <= '1';

	wait for TIME_DELTA;

	S <= '1';
	R <= '1';

	wait for TIME_DELTA;

	S <= '0';
	R <= '1';

	wait for TIME_DELTA;

	S <= '0';
	R <= '0';

	wait for TIME_DELTA;
	end process simulation;
	
	S_R_latch_top_inst: S_R_latch_top
	port map(
--		clk	=> clk,
		S	=> S,
		R	=> R,
		Q	=> Q
	);
end behavioral;

