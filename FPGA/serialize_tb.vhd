library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity serialize_tb is
end entity serialize_tb;

architecture behavioral of serialize_tb is

	component serialize is
		Port (
		clk			: in	std_logic;
		reset		: in	std_logic;
		data		: in	std_logic_vector(7 downto 0);
		tmp_out		: out	std_logic_vector(7 downto 0);
		load		: out	std_logic;
		serial_out	: out	std_logic
		);
	end component;

	constant TIME_DELTA		: time := 10 ns;
	constant TIME_DELTA_CLK	: time := 10 ns;

	signal	clk				: std_logic := '0';
	signal	reset			: std_logic := '1';
	signal	data			: std_logic_vector (7 downto 0) := x"00";

	signal	tmp_out			: std_logic_vector(7 downto 0);
	signal	load			: std_logic;
	signal	serial_out		: std_logic;

begin


	clk   <= not clk  after TIME_DELTA_CLK;

	simulation: process
	begin


	wait for TIME_DELTA_CLK / 2;


	wait for TIME_DELTA;

	data		<= x"00";

	wait for TIME_DELTA;

	data		<= x"00";

	wait for TIME_DELTA;

	reset		<= '0';
	data		<= x"00";

	wait for TIME_DELTA;

	data		<= x"00";

	wait for TIME_DELTA;

	data		<= x"00";

	wait for TIME_DELTA;

	data		<= x"00";

	wait for TIME_DELTA;

	data		<= x"00";

	wait for TIME_DELTA;

	data		<= x"00";

	wait for TIME_DELTA;

	data		<= x"00";

	wait for TIME_DELTA;

	data		<= x"00";

	wait for TIME_DELTA;

	data		<= x"00";

	wait for TIME_DELTA;

	data		<= x"00";

	wait for TIME_DELTA;

	data		<= x"00";

	wait for TIME_DELTA;

	data		<= x"FF";

	wait for TIME_DELTA;

	data		<= x"FF";

	wait for TIME_DELTA;

	data		<= x"FF";

	wait for TIME_DELTA;

	data		<= x"FF";

	wait for TIME_DELTA;

	data		<= x"FF";

	wait for TIME_DELTA;

	data		<= x"FF";

	wait for TIME_DELTA;

	data		<= x"FF";

	wait for TIME_DELTA;

	data		<= x"FF";

	wait for TIME_DELTA;

	data		<= x"FF";

	wait for TIME_DELTA;

	data		<= x"FF";

	wait for TIME_DELTA;

	data		<= x"FF";

	wait for TIME_DELTA;

	data		<= x"FF";

	wait for TIME_DELTA;

	data		<= std_logic_vector(unsigned(data) + 1);

	wait for TIME_DELTA;

	data		<= std_logic_vector(unsigned(data) + 1);

	wait for TIME_DELTA;

	data		<= std_logic_vector(unsigned(data) + 1);

	wait for TIME_DELTA;

	data		<= std_logic_vector(unsigned(data) + 1);

	wait for TIME_DELTA;

	data		<= std_logic_vector(unsigned(data) + 1);

	wait for TIME_DELTA;

	data		<= std_logic_vector(unsigned(data) + 1);

	wait for TIME_DELTA;

	data		<= std_logic_vector(unsigned(data) + 1);

	wait for TIME_DELTA;

	data		<= std_logic_vector(unsigned(data) + 1);

	wait for TIME_DELTA;

	data		<= std_logic_vector(unsigned(data) + 1);

	wait for TIME_DELTA;

	data		<= std_logic_vector(unsigned(data) + 1);

	wait for TIME_DELTA;

	data		<= std_logic_vector(unsigned(data) + 1);

	wait for TIME_DELTA;

	end process simulation;
	


	serialize_inst: serialize
	port map(
		clk			=> clk,
		reset		=> reset,
		data		=> data,
		tmp_out		=> tmp_out,
		load		=> load,
		serial_out	=> serial_out
	);
end behavioral;

