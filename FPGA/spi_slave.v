////////////////////////////////////////////////////////////////////////////////
////                                                                        ////
//// Project Name: SPI (Verilog)                                            ////
////                                                                        ////
//// Module Name: spi_slave                                                ////
////                                                                        ////
////                                                                        ////
////  This file is part of the Ethernet IP core project                     ////
////  http://opencores.com/project,spi_verilog_master_slave                 ////
////                                                                        ////
////  Author(s):                                                            ////
////      Santhosh G (santhg@opencores.org)                                 ////
////                                                                        ////
////  Refer to Readme.txt for more information                              ////
////                                                                        ////
////////////////////////////////////////////////////////////////////////////////
////                                                                        ////
//// Copyright (C) 2014, 2015 Authors                                       ////
////                                                                        ////
//// This source file may be used and distributed without                   ////
//// restriction provided that this copyright statement is not              ////
//// removed from the file and that any derivative work contains            ////
//// the original copyright notice and the associated disclaimer.           ////
////                                                                        ////
//// This source file is free software; you can redistribute it             ////
//// and/or modify it under the terms of the GNU Lesser General             ////
//// Public License as published by the Free Software Foundation;           ////
//// either version 2.1 of the License, or (at your option) any             ////
//// later version.                                                         ////
////                                                                        ////
//// This source is distributed in the hope that it will be                 ////
//// useful, but WITHOUT ANY WARRANTY; without even the implied             ////
//// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR                ////
//// PURPOSE.  See the GNU Lesser General Public License for more           ////
//// details.                                                               ////
////                                                                        ////
//// You should have received a copy of the GNU Lesser General              ////
//// Public License along with this source; if not, download it             ////
//// from http://www.opencores.org/lgpl.shtml                               ////
////                                                                        ////
////////////////////////////////////////////////////////////////////////////////
/* SPI MODE 3
		CHANGE DATA (miso) @ NEGEDGE SCK
		read data (mosi) @posedge SCK
*/		
module spi_slave (resetbuffer,transmittenable,tdata,msbfirst,cs,sck,mosi, miso,done,rdata);
  input resetbuffer,cs,sck,mosi,transmittenable,msbfirst;
  input [7:0] tdata;
  output miso;           //slave out   master in 
  output reg done;
  output reg [7:0] rdata;

  reg [7:0] treg,rreg;
  reg [3:0] nb;
  reg done_int;
  wire sout;
  
  assign sout=msbfirst?treg[7]:treg[0];
  assign miso=( (!cs)&&transmittenable )?sout:1'bz; //if 1=> send data  else TRI-STATE miso


//read from  miso
always @(posedge sck or negedge resetbuffer)
  begin
    if (resetbuffer==0)
		begin rreg = 8'h00;  /*rdata = 8'h00;*/ done_int = 0; nb = 0; end   //
	else if (!cs) begin 
			if(msbfirst==0)  //LSB first, in@msb -> right shift
				begin rreg ={mosi,rreg[7:1]}; end
			else     //MSB first, in@lsb -> left shift
				begin rreg ={rreg[6:0],mosi}; end  
		//increment bit count
			nb=nb+1;
			if(nb!=8) done_int=0;
			else  begin rdata=rreg; done_int=1; nb=0; end
		end	 //if(!cs)_END  if(nb==8)
		done = done_int;
  end

//send to  miso
always @(negedge sck or negedge resetbuffer)
  begin
	if (resetbuffer==0)
		begin treg = 8'hFF; end
	else begin
		if(!cs) begin			
			if(nb==0) treg=tdata;
			else begin
			   if(msbfirst==0)  //LSB first, out=lsb -> right shift
					begin treg = {1'b1,treg[7:1]}; end
			   else     //MSB first, out=msb -> left shift
					begin treg = {treg[6:0],1'b1}; end			
			end
		end //!cs
	 end //resetbuffer	
  end //always

endmodule
      
/*
			if(msbfirst==0)  //LSB first, out=lsb -> right shift
					begin treg = {treg[7],treg[7:1]}; end
			else     //MSB first, out=msb -> left shift
					begin treg = {treg[6:0],treg[0]}; end	
*/


/*
force -freeze sim:/SPI_slave/sck 0 0, 1 {25 ns} -r 50 -can 410
run 405ns
noforce sim:/SPI_slave/sck
force -freeze sim:/SPI_slave/sck 1 0
*/
