library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity S_R_latch_top is
	Port (
		S : in    std_logic;
		R : in    std_logic;
		Q : out   std_logic
	);
end S_R_latch_top;

architecture Behavioral of S_R_latch_top is
signal Q2   : std_logic;
signal notQ : std_logic;
begin

Q    <= Q2;
Q2   <= R nor notQ;
notQ <= (S and not R) nor Q2;

end Behavioral;