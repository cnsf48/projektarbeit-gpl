library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity LCD is
	port(
		clk		: in	std_logic;
		send_n	: in	std_logic;
		addr	: in	std_logic_vector(7 downto 0);
		data	: in	std_logic_vector(7 downto 0);
		LCD_Bus	: out	std_logic_vector(7 downto 0);
		E		: out	std_logic;
		RS		: out	std_logic
	);
end LCD;

architecture behavioral of LCD is

	component special_flipflop is
		Port (
			S	: in	STD_LOGIC;
			R	: in	STD_LOGIC;
			Q	: out	STD_LOGIC
		);
	end component;

	type State_type is (idle, init, enable, disable);

	signal	state			: State_type;
	signal	LCD_Bus_int		: std_logic_vector (7 downto 0);
	signal	addr_int		: std_logic_vector (7 downto 0);
	signal	E_int			: std_logic;
	signal	RS_int			: std_logic;	
	signal	bufferd_send	: std_logic;
	signal	rst_send		: std_logic;

begin
	process (send_n)
	begin
		if rising_edge(send_n) then			
			LCD_Bus_int <= data;
			addr_int <= addr;
		end if;
	end process;
	process (clk)
	begin
		if rising_edge(clk) then
			case state is
				when idle =>
					if (bufferd_send = '1') then
						state <= init;
						rst_send <= '1';
					else 
						rst_send <= '0';
					end if;
				when init =>
					rst_send <= '0';
					case addr_int is
						when x"10" =>
							RS_int <= '1';
							state <= enable;
						when x"11" =>
							RS_int <= '0';
							state <= enable;
						when others =>
							state <= idle;
					end case;
				when enable =>
					E_int <= '1';
					state <= disable;
				when disable =>
					E_int <= '0';
					state <= idle;
				when others =>
					state <= idle;
					rst_send <= '0';
			end case;
		end if;
	end process;

	LCD_Bus <= LCD_Bus_int;
	E <= E_int;
	RS <= RS_int;

	special_flipflop_inst: special_flipflop
	port map(
		S	=> send_n,
		R	=> rst_send,
		Q	=> bufferd_send
	);
end behavioral;

